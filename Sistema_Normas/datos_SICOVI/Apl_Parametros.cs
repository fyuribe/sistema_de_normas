//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace datos_cambios_procesos
{
    using System;
    using System.Collections.Generic;
    
    public partial class Apl_Parametros
    {
        public int Parametro_ID { get; set; }
        public int Empresa_ID { get; set; }
        public string No_Intentos_Acceso { get; set; }
        public string Prefijo { get; set; }
        public string Tipo_Usuario { get; set; }
        public Nullable<int> Menu_ID { get; set; }
    
        public virtual Apl_Empresas Apl_Empresas { get; set; }
        public virtual Apl_Menus Apl_Menus { get; set; }
    }
}
