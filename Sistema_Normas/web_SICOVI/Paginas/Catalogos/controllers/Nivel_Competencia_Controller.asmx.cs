﻿using admin_cambios_procesos.Models.Ayudante;
using datos_cambios_procesos;
using LitJson;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using web_cambios_procesos.Models.Negocio;

namespace web_cambios_procesos.Paginas.Catalogos.controllers
{
    /// <summary>
    /// Summary description for Nivel_Competencia_Controller
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class Nivel_Competencia_Controller : System.Web.Services.WebService
    {
        /// <summary>
        /// Método que realiza el alta del un nuevo registro
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Alta(string jsonObject)
        {
            //parametros para la consulta
            Cls_Cat_Nivel_Competencia_Negocio Obj_Nivel = null;
            //resultado de la funcion
            string Json_Resultado = string.Empty;
            //mensaje que se regresa
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Insertar";
                Obj_Nivel = JsonMapper.ToObject<Cls_Cat_Nivel_Competencia_Negocio>(jsonObject);

                using (var dbContext = new Sistema_NormasContainer())
                {
                    //consulta a la base de datos
                    var _nivel_competencia = new Cat_Nivel_Competencia();
                    _nivel_competencia.Nombre = Obj_Nivel.Nombre;
                    _nivel_competencia.Descripcion = Obj_Nivel.Descripcion;
                    _nivel_competencia.Estatus = Obj_Nivel.Estatus;
                    _nivel_competencia.Usuario_Creo = Cls_Sesiones.Usuario;
                    _nivel_competencia.Fecha_Creo = new DateTime?(DateTime.Now).Value;

                    dbContext.Cat_Nivel_Competencia.Add(_nivel_competencia);
                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "La opercion de completo sin problemas.";
                }
            }
            //Mostrar mensajes de error
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Technical report";
                Mensaje.Estatus = "error";
                //validar el mensaje 
                if (Ex.InnerException.Message.Contains("String or binary data would be truncated"))
                    Mensaje.Mensaje =
                        "Any of the fields you are trying to insert have a size larger than that set in the database. <br /><br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; String or binary data would be truncated";
                //validar el mensaje 
                else if (Ex.InnerException.InnerException.Message.Contains("Cannot insert duplicate key row in object"))
                    Mensaje.Mensaje =
                        "There are defined fields as keys that can not be duplicated. <br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Please check that you are not entering duplicate data.";
                //validar el mensaje 
                else
                    Mensaje.Mensaje = "Technical report: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }

        /// <summary>
        /// Método que realiza la actualización de los datos del registro seleccionado.
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Actualizar(string jsonObject)
        {
            //parametros del consulta
            Cls_Cat_Nivel_Competencia_Negocio Obj_Nivel = null;
            //resultado de la funcion
            string Json_Resultado = string.Empty;
            //mensaje que se regresa
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Actualizar registro";
                Obj_Nivel = JsonMapper.ToObject<Cls_Cat_Nivel_Competencia_Negocio>(jsonObject);

                using (var dbContext = new Sistema_NormasContainer())
                {
                    //consulta a la base de datos
                    var _nivel = dbContext.Cat_Nivel_Competencia.Where(u => u.Nivel_Competencia_ID == Obj_Nivel.Nivel_Competencia_ID).First();
                    _nivel.Nombre = Obj_Nivel.Nombre;
                    _nivel.Descripcion = Obj_Nivel.Descripcion;
                    _nivel.Estatus = Obj_Nivel.Estatus;
                    _nivel.Usuario_Modifico = Cls_Sesiones.Usuario;
                    _nivel.Fecha_Modifico = new DateTime?(DateTime.Now).Value;

                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "La operacion de completo sin problemas.";
                }
            }
            //Mostrar mensajes de error
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Technical report";
                Mensaje.Estatus = "error";
                //validar mensaje
                if (Ex.InnerException.Message.Contains("String or binary data would be truncated"))
                    Mensaje.Mensaje =
                        "Any of the fields you are trying to insert have a size larger than that set in the database. <br /><br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; String or binary data would be truncated";
                //validar mensaje
                else if (Ex.InnerException.InnerException.Message.Contains("Cannot insert duplicate key row in object"))
                    Mensaje.Mensaje =
                        "There are defined fields as keys that can not be duplicated. <br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp;Please check that you are not entering duplicate data.";
                //mostrar mensaje
                else
                    Mensaje.Mensaje = "Technical report: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }

        /// <summary>
        /// Método que elimina el registro seleccionado.
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Eliminar(string jsonObject)
        {
            //parametros del consulta
            Cls_Cat_Nivel_Competencia_Negocio Obj_Nivel = null;
            //resultado de la funcion
            string Json_Resultado = string.Empty;
            //mensaje que se regresa
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Eliminar registro";
                Obj_Nivel = JsonMapper.ToObject<Cls_Cat_Nivel_Competencia_Negocio>(jsonObject);

                using (var dbContext = new Sistema_NormasContainer())
                {
                    //consulta a la base de datos
                    var _nivel_competencia = dbContext.Cat_Nivel_Competencia.Where(u => u.Nivel_Competencia_ID == Obj_Nivel.Nivel_Competencia_ID).First();
                    dbContext.Cat_Nivel_Competencia.Remove(_nivel_competencia);

                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "La operacion de completo sin ploblemas.";
                }
            }
            // Mostrar mensajes de error
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Informe Técnico";
                Mensaje.Estatus = "error";
                //validar mensaje 
                if (Ex.InnerException.InnerException.Message.Contains("The DELETE statement conflicted with the REFERENCE constraint"))
                    Mensaje.Mensaje =
                        "The delete record operation was revoked. <br /><br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; The record you are trying to delete is already in use.";
                //validar mensaje
                else
                    Mensaje.Mensaje = "Technical report: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }

        /// <summary>
        /// Método que realiza la búsqueda de registros.
        /// </summary>
        /// <returns>Listado serializado según los filtros aplícados</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Nivel_Competencia_Por_Filtros(string jsonObject)
        {
            //parametros de la consulta 
            Cls_Cat_Nivel_Competencia_Negocio Obj_Nivel = null;
            //resultado de la funcion
            string Json_Resultado = string.Empty;
            //lista de resultados 
            List<Cls_Cat_Nivel_Competencia_Negocio> Lista_Aspectos = new List<Cls_Cat_Nivel_Competencia_Negocio>();
            //mensaje que se regresa
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                Obj_Nivel = JsonMapper.ToObject<Cls_Cat_Nivel_Competencia_Negocio>(jsonObject);

                using (var dbContext = new Sistema_NormasContainer())
                {
                    //consulta a la base de datos
                    var nivel = (from _nivel in dbContext.Cat_Nivel_Competencia
                                    where
                                   ((Obj_Nivel.Nivel_Competencia_ID != 0) ? _nivel.Nivel_Competencia_ID.Equals(Obj_Nivel.Nivel_Competencia_ID) : true)
                                    select new Cls_Cat_Nivel_Competencia_Negocio
                                    {
                                        Nivel_Competencia_ID = _nivel.Nivel_Competencia_ID,
                                        Nombre = _nivel.Nombre,
                                        Descripcion = _nivel.Descripcion,
                                        Estatus = _nivel.Estatus
                                    }).OrderByDescending(u => u.Nivel_Competencia_ID);
                    //recorrer la lista de datos 
                    foreach (var p in nivel)
                        Lista_Aspectos.Add((Cls_Cat_Nivel_Competencia_Negocio)p);
                    Json_Resultado = JsonMapper.ToJson(Lista_Aspectos);
                }
            }
            //Mostrar mensaje de error
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            return Json_Resultado;
        }

        /// <summary>
        /// Método que realiza la búsqueda de registros.
        /// </summary>
        /// <returns>Listado</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Nivel_Competencia_Por_Nombre(string jsonObject)
        {
            //parametros de la consulta
            Cls_Cat_Nivel_Competencia_Negocio Obj_Nivel_Competencia = null;
            //resultado de la funcion
            string Json_Resultado = string.Empty;
            //lista de registros
            List<Cls_Cat_Nivel_Competencia_Negocio> Lista_Unidades = new List<Cls_Cat_Nivel_Competencia_Negocio>();
            //mensaje que se regresa
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Validaciones";
                Obj_Nivel_Competencia = JsonMapper.ToObject<Cls_Cat_Nivel_Competencia_Negocio>(jsonObject);

                using (var dbContext = new Sistema_NormasContainer())
                {
                    var _nivel_competencia = (from _nivel in dbContext.Cat_Nivel_Competencia
                                                where _nivel.Nombre.Equals(Obj_Nivel_Competencia.Nombre)
                                                select new Cls_Cat_Nivel_Competencia_Negocio
                                                {
                                                    Nivel_Competencia_ID = _nivel.Nivel_Competencia_ID,
                                                    Estatus = _nivel.Estatus
                                                }).OrderByDescending(u => u.Nivel_Competencia_ID);
                    //valida si la lista tiene algun registro
                    if (_nivel_competencia.Any())
                    {
                        //valida si el id es igual a 0
                        if (Obj_Nivel_Competencia.Nivel_Competencia_ID == 0)
                        {
                            Mensaje.Estatus = "error";
                            //valida el nombre
                            if (!string.IsNullOrEmpty(Obj_Nivel_Competencia.Nombre))
                                Mensaje.Mensaje = "El nombre ingresado ya se encuentra registrado.";
                        }
                        //valida si el id existe en la bd
                        else
                        {
                            //consulta a la BD
                            var item_edit = _nivel_competencia.Where(u => u.Nivel_Competencia_ID == Obj_Nivel_Competencia.Nivel_Competencia_ID);
                            //valida el id del registro
                            if (item_edit.Count() == 1)
                                Mensaje.Estatus = "success";
                            //mostrar mensaje de error
                            else
                            {
                                Mensaje.Estatus = "error";
                                //validar el nombre del registro
                                if (!string.IsNullOrEmpty(Obj_Nivel_Competencia.Nombre))
                                    Mensaje.Mensaje = "El nombre ingresado ya se encuentra registrado.";
                            }
                        }
                    }
                    //mostrar mensaje de exito
                    else
                        Mensaje.Estatus = "success";

                    Json_Resultado = JsonMapper.ToJson(Mensaje);
                }
            }
            //mostrar mensaje de error
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            return Json_Resultado;
        }


        /// <summary>
        /// Método que realiza la consulta de aspectos normativos
        /// </summary>
        /// <returns>Listado</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void Consultar_Niveles()
        {
            //resultado que se regresa
            string Json_Resultado = string.Empty;
            //lista de registros
            List<Cls_Select2> Lst_Combo = new List<Cls_Select2>();
            //mensaje que se regresa
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                string q = string.Empty;
                NameValueCollection nvc = Context.Request.Form;

                if (!String.IsNullOrEmpty(nvc["q"]))
                    q = nvc["q"].ToString().Trim();

                using (var dbContext = new Sistema_NormasContainer())
                {
                    var Niveles = (from _nivel in dbContext.Cat_Nivel_Competencia
                                    where
                                     ((string.IsNullOrEmpty(q)) ? true : _nivel.Nombre.Contains(q))
                                    select new Cls_Select2
                                    {
                                        id = _nivel.Nivel_Competencia_ID.ToString(),
                                        text = _nivel.Nombre,

                                    });
                    //recorre la lista de resultados
                    foreach (var p in Niveles)
                        Lst_Combo.Add(p);

                    Json_Resultado = JsonMapper.ToJson(Lst_Combo);
                }
            }
            //mensaje de error
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            finally
            {
                Context.Response.Write(Json_Resultado);
            }
        }
    }
}
