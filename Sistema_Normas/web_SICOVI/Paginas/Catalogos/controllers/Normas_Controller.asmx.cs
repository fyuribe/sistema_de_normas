﻿using admin_cambios_procesos.Models.Ayudante;
using datos_cambios_procesos;
using LitJson;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using web_cambios_procesos.Models.Negocio;

namespace web_cambios_procesos.Paginas.Catalogos.controllers
{
    /// <summary>
    /// Summary description for Normas_Controller
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class Normas_Controller : System.Web.Services.WebService
    {
        /// <summary>
        /// Método que realiza el alta del un nuevo registro
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Alta(string jsonObject)
        {
            //parametros para la consulta
            Cls_Cat_Normas_Negocio Obj_Normas = null;
            //resultado de la funcion
            string Json_Resultado = string.Empty;
            //mensaje que se regresa
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Insertar";
                Obj_Normas = JsonMapper.ToObject<Cls_Cat_Normas_Negocio>(jsonObject);

                using (var dbContext = new Sistema_NormasContainer())
                {
                    //consulta a la base de datos
                    var _normas = new Cat_Normas();
                    _normas.Nombre = Obj_Normas.Nombre;
                    _normas.Descripcion = Obj_Normas.Descripcion;
                    _normas.Estatus = Obj_Normas.Estatus;
                    _normas.Nivel_Competencia_ID = Obj_Normas.Nivel_Competencia_ID;
                    _normas.Aspecto_Normativo_ID = Obj_Normas.Aspecto_Normativo_ID;
                    _normas.Usuario_Creo = Cls_Sesiones.Usuario;
                    _normas.Fecha_Creo = new DateTime?(DateTime.Now).Value;

                    dbContext.Cat_Normas.Add(_normas);
                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "La opercion de completo sin problemas.";
                }
            }
            //Mostrar mensajes de error
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Technical report";
                Mensaje.Estatus = "error";
                //validar el mensaje 
                if (Ex.InnerException.Message.Contains("String or binary data would be truncated"))
                    Mensaje.Mensaje =
                        "Any of the fields you are trying to insert have a size larger than that set in the database. <br /><br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; String or binary data would be truncated";
                //validar el mensaje 
                else if (Ex.InnerException.InnerException.Message.Contains("Cannot insert duplicate key row in object"))
                    Mensaje.Mensaje =
                        "There are defined fields as keys that can not be duplicated. <br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Please check that you are not entering duplicate data.";
                //validar el mensaje 
                else
                    Mensaje.Mensaje = "Technical report: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }

        /// <summary>
        /// Método que realiza la actualización de los datos del registro seleccionado.
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Actualizar(string jsonObject)
        {
            //parametros del consulta
            Cls_Cat_Normas_Negocio Obj_Normas = null;
            //resultado de la funcion
            string Json_Resultado = string.Empty;
            //mensaje que se regresa
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Actualizar registro";
                Obj_Normas = JsonMapper.ToObject<Cls_Cat_Normas_Negocio>(jsonObject);

                using (var dbContext = new Sistema_NormasContainer())
                {
                    //consulta a la base de datos
                    var _normas = dbContext.Cat_Normas.Where(u => u.Norma_ID == Obj_Normas.Norma_ID).First();
                    _normas.Nombre = Obj_Normas.Nombre;
                    _normas.Descripcion = Obj_Normas.Descripcion;
                    _normas.Estatus = Obj_Normas.Estatus;
                    _normas.Nivel_Competencia_ID = Obj_Normas.Nivel_Competencia_ID;
                    _normas.Aspecto_Normativo_ID = Obj_Normas.Aspecto_Normativo_ID;
                    _normas.Usuario_Modifico = Cls_Sesiones.Usuario;
                    _normas.Fecha_Modifico = new DateTime?(DateTime.Now).Value;

                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "La operacion de completo sin problemas.";
                }
            }
            //Mostrar mensajes de error
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Technical report";
                Mensaje.Estatus = "error";
                //validar mensaje
                if (Ex.InnerException.Message.Contains("String or binary data would be truncated"))
                    Mensaje.Mensaje =
                        "Any of the fields you are trying to insert have a size larger than that set in the database. <br /><br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; String or binary data would be truncated";
                //validar mensaje
                else if (Ex.InnerException.InnerException.Message.Contains("Cannot insert duplicate key row in object"))
                    Mensaje.Mensaje =
                        "There are defined fields as keys that can not be duplicated. <br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp;Please check that you are not entering duplicate data.";
                //mostrar mensaje
                else
                    Mensaje.Mensaje = "Technical report: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }

        /// <summary>
        /// Método que elimina el registro seleccionado.
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Eliminar(string jsonObject)
        {
            //parametros del consulta
            Cls_Cat_Normas_Negocio Obj_Normas = null;
            //resultado de la funcion
            string Json_Resultado = string.Empty;
            //mensaje que se regresa
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Eliminar registro";
                Obj_Normas = JsonMapper.ToObject<Cls_Cat_Normas_Negocio>(jsonObject);

                using (var dbContext = new Sistema_NormasContainer())
                {
                    //consulta a la base de datos
                    var _normas = dbContext.Cat_Normas.Where(u => u.Norma_ID == Obj_Normas.Norma_ID).First();
                    dbContext.Cat_Normas.Remove(_normas);

                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "La operacion de completo sin ploblemas.";
                }
            }
            // Mostrar mensajes de error
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Informe Técnico";
                Mensaje.Estatus = "error";
                //validar mensaje 
                if (Ex.InnerException.InnerException.Message.Contains("The DELETE statement conflicted with the REFERENCE constraint"))
                    Mensaje.Mensaje =
                        "The delete record operation was revoked. <br /><br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; The record you are trying to delete is already in use.";
                //validar mensaje
                else
                    Mensaje.Mensaje = "Technical report: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }

        /// <summary>
        /// Método que realiza la búsqueda de registros.
        /// </summary>
        /// <returns>Listado serializado según los filtros aplícados</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Normas_Por_Filtros(string jsonObject)
        {
            //parametros de la consulta 
            Cls_Cat_Normas_Negocio Obj_Normas = null;
            //resultado de la funcion
            string Json_Resultado = string.Empty;
            //lista de resultados 
            List<Cls_Cat_Normas_Negocio> Lista_Normas = new List<Cls_Cat_Normas_Negocio>();
            //mensaje que se regresa
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                Obj_Normas = JsonMapper.ToObject<Cls_Cat_Normas_Negocio>(jsonObject);

                using (var dbContext = new Sistema_NormasContainer())
                {
                    //consulta a la base de datos
                    var Normas = (from _norma in dbContext.Cat_Normas
                                  join _aspectos in dbContext.Cat_Aspectos_Normativos on _norma.Aspecto_Normativo_ID equals _aspectos.Aspecto_Normativo_ID
                                  join _nivel in dbContext.Cat_Nivel_Competencia on _norma.Nivel_Competencia_ID equals _nivel.Nivel_Competencia_ID
                                  where
                                 ((Obj_Normas.Norma_ID != 0) ? _norma.Norma_ID.Equals(Obj_Normas.Norma_ID) : true)
                                 select new Cls_Cat_Normas_Negocio
                                 {
                                     Norma_ID = _norma.Norma_ID,
                                     Nombre = _norma.Nombre,
                                     Descripcion = _norma.Descripcion,
                                     Estatus = _norma.Estatus,
                                     Aspecto_Normativo_ID = _norma.Aspecto_Normativo_ID,
                                     Nivel_Competencia_ID = _norma.Nivel_Competencia_ID,
                                     Aspecto_Normativo = _aspectos.Nombre,
                                     Nivel_Competencia = _nivel.Nombre
                                 }).OrderByDescending(u => u.Norma_ID);
                    //recorrer la lista de datos 
                    foreach (var p in Normas)
                        Lista_Normas.Add((Cls_Cat_Normas_Negocio)p);
                    Json_Resultado = JsonMapper.ToJson(Lista_Normas);
                }
            }
            //Mostrar mensaje de error
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            return Json_Resultado;
        }

        /// <summary>
        /// Método que realiza la búsqueda de registros.
        /// </summary>
        /// <returns>Listado</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Normas_Por_Nombre(string jsonObject)
        {
            //parametros de la consulta
            Cls_Cat_Normas_Negocio Obj_Normas = null;
            //resultado de la funcion
            string Json_Resultado = string.Empty;
            //lista de registros
            List<Cls_Cat_Normas_Negocio> Lista_Normas = new List<Cls_Cat_Normas_Negocio>();
            //mensaje que se regresa
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Validaciones";
                Obj_Normas = JsonMapper.ToObject<Cls_Cat_Normas_Negocio>(jsonObject);

                using (var dbContext = new Sistema_NormasContainer())
                {
                    var _normas = (from _nivel in dbContext.Cat_Normas
                                              where _nivel.Nombre.Equals(Obj_Normas.Nombre)
                                              select new Cls_Cat_Normas_Negocio
                                              {
                                                  Norma_ID = _nivel.Norma_ID,
                                                  Estatus = _nivel.Estatus
                                              }).OrderByDescending(u => u.Norma_ID);
                    //valida si la lista tiene algun registro
                    if (_normas.Any())
                    {
                        //valida si el id es igual a 0
                        if (Obj_Normas.Norma_ID == 0)
                        {
                            Mensaje.Estatus = "error";
                            //valida el nombre
                            if (!string.IsNullOrEmpty(Obj_Normas.Nombre))
                                Mensaje.Mensaje = "El nombre ingresado ya se encuentra registrado.";
                        }
                        //valida si el id existe en la bd
                        else
                        {
                            //consulta a la BD
                            var item_edit = _normas.Where(u => u.Norma_ID == Obj_Normas.Norma_ID);
                            //valida el id del registro
                            if (item_edit.Count() == 1)
                                Mensaje.Estatus = "success";
                            //mostrar mensaje de error
                            else
                            {
                                Mensaje.Estatus = "error";
                                //validar el nombre del registro
                                if (!string.IsNullOrEmpty(Obj_Normas.Nombre))
                                    Mensaje.Mensaje = "El nombre ingresado ya se encuentra registrado.";
                            }
                        }
                    }
                    //mostrar mensaje de exito
                    else
                        Mensaje.Estatus = "success";

                    Json_Resultado = JsonMapper.ToJson(Mensaje);
                }
            }
            //mostrar mensaje de error
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            return Json_Resultado;
        }


        /// <summary>
        /// Método que realiza la consulta de niveles de competencia
        /// </summary>
        /// <returns>Listado</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void Consultar_Niveles()
        {
            //resultado que se regresa
            string Json_Resultado = string.Empty;
            //lista de registros
            List<Cls_Select2> Lst_Combo = new List<Cls_Select2>();
            //mensaje que se regresa
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                string q = string.Empty;
                NameValueCollection nvc = Context.Request.Form;

                if (!String.IsNullOrEmpty(nvc["q"]))
                    q = nvc["q"].ToString().Trim();

                using (var dbContext = new Sistema_NormasContainer())
                {
                    var Niveles = (from _nivel in dbContext.Cat_Nivel_Competencia
                                   where
                                    ((string.IsNullOrEmpty(q)) ? true : _nivel.Nombre.Contains(q))
                                   select new Cls_Select2
                                   {
                                       id = _nivel.Nivel_Competencia_ID.ToString(),
                                       text = _nivel.Nombre,

                                   });
                    //recorre la lista de resultados
                    foreach (var p in Niveles)
                        Lst_Combo.Add(p);

                    Json_Resultado = JsonMapper.ToJson(Lst_Combo);
                }
            }
            //mensaje de error
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            finally
            {
                Context.Response.Write(Json_Resultado);
            }
        }

        /// <summary>
        /// Método que realiza la consulta de aspectos normativos
        /// </summary>
        /// <returns>Listado</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void Consultar_Aspecto_Normativo()
        {
            //resultado que se regresa
            string Json_Resultado = string.Empty;
            //lista de registros
            List<Cls_Select2> Lst_Combo = new List<Cls_Select2>();
            //mensaje que se regresa
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                string q = string.Empty;
                NameValueCollection nvc = Context.Request.Form;

                if (!String.IsNullOrEmpty(nvc["q"]))
                    q = nvc["q"].ToString().Trim();

                using (var dbContext = new Sistema_NormasContainer())
                {
                    var Aspectos = (from _aspecto in dbContext.Cat_Aspectos_Normativos
                                    where
                                     ((string.IsNullOrEmpty(q)) ? true : _aspecto.Nombre.Contains(q))
                                    select new Cls_Select2
                                    {
                                        id = _aspecto.Aspecto_Normativo_ID.ToString(),
                                        text = _aspecto.Nombre,

                                    });
                    //recorre la lista de resultados
                    foreach (var p in Aspectos)
                        Lst_Combo.Add(p);

                    Json_Resultado = JsonMapper.ToJson(Lst_Combo);
                }
            }
            //mensaje de error
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            finally
            {
                Context.Response.Write(Json_Resultado);
            }
        }

        /// <summary>
        /// Método que realiza la consulta de normas
        /// </summary>
        /// <returns>Listado</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void Consultar_Normas()
        {
            //resultado que se regresa
            string Json_Resultado = string.Empty;
            //lista de registros
            List<Cls_Select2> Lst_Combo = new List<Cls_Select2>();
            //mensaje que se regresa
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                string q = string.Empty;
                NameValueCollection nvc = Context.Request.Form;

                if (!String.IsNullOrEmpty(nvc["q"]))
                    q = nvc["q"].ToString().Trim();

                using (var dbContext = new Sistema_NormasContainer())
                {
                    var Normas = (from _normas in dbContext.Cat_Normas
                                    where
                                     ((string.IsNullOrEmpty(q)) ? true : _normas.Nombre.Contains(q))
                                    select new Cls_Select2
                                    {
                                        id = _normas.Norma_ID.ToString(),
                                        text = _normas.Nombre,

                                    });
                    //recorre la lista de resultados
                    foreach (var p in Normas)
                        Lst_Combo.Add(p);

                    Json_Resultado = JsonMapper.ToJson(Lst_Combo);
                }
            }
            //mensaje de error
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            finally
            {
                Context.Response.Write(Json_Resultado);
            }
        }
    }
}
