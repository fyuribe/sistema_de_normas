﻿using datos_cambios_procesos;
using LitJson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using web_cambios_procesos.Models.Ayudante;
using web_cambios_procesos.Models.Negocio;

namespace web_cambios_procesos.Paginas.Catalogos.controllers
{
    /// <summary>
    /// Summary description for Puestos_Controller
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class Puestos_Controller : System.Web.Services.WebService
    {
        #region (Métodos)
        /// <summary>
        /// Método que realiza el alta de la area.
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Alta(string jsonObject)
        {
            Cls_Cat_Puestos_Negocio Obj_Puestos = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Alta registro";
                Obj_Puestos = JsonMapper.ToObject<Cls_Cat_Puestos_Negocio>(jsonObject);

                using (var dbContext = new Sistema_NormasContainer())
                {
                    var _puestos = new Cat_Puestos();
                    _puestos.Estatus_ID = Obj_Puestos.Estatus_ID;
                    _puestos.Departamento_ID = Obj_Puestos.Departamento_ID;
                    _puestos.Nombre = Obj_Puestos.Nombre;
                    _puestos.Descripcion = Obj_Puestos.Descripcion;
                    _puestos.Usuario_Creo = Cls_Sesiones.Usuario;
                    _puestos.Fecha_Creo = new DateTime?(DateTime.Now).Value;

                    dbContext.Cat_Puestos.Add(_puestos);
                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "La operación se completó sin problemas.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Informe Técnico";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.Message.Contains("Los datos de cadena o binarios se truncarían"))
                    Mensaje.Mensaje =
                        "Alguno de los campos que intenta insertar tiene un tamaño mayor al establecido en la base de datos. <br /><br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Los datos de cadena o binarios se truncarían";
                else if (Ex.InnerException.InnerException.Message.Contains("Cannot insert duplicate key row in object"))
                    Mensaje.Mensaje =
                        "Existen campos definidos como claves que no pueden duplicarse. <br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Por favor revisar que no este ingresando datos duplicados.";
                else
                    Mensaje.Mensaje = "Informe técnico: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que realiza la actualización de los datos de la area seleccionada.
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Actualizar(string jsonObject)
        {
            Cls_Cat_Puestos_Negocio Obj_Puestos = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Actualizar registro";
                Obj_Puestos = JsonMapper.ToObject<Cls_Cat_Puestos_Negocio>(jsonObject);

                using (var dbContext = new Sistema_NormasContainer())
                {
                    var _puestos = dbContext.Cat_Puestos.Where(u => u.Puesto_ID == Obj_Puestos.Puesto_ID).First();

                    _puestos.Estatus_ID = Obj_Puestos.Estatus_ID;
                    _puestos.Departamento_ID = Obj_Puestos.Departamento_ID;
                    _puestos.Nombre = Obj_Puestos.Nombre;
                    _puestos.Descripcion = Obj_Puestos.Descripcion;
                    _puestos.Usuario_Modifico = Cls_Sesiones.Usuario;
                    _puestos.Fecha_Modifico = new DateTime?(DateTime.Now);

                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "La operación se completó sin problemas.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Informe Técnico";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.Message.Contains("Los datos de cadena o binarios se truncarían"))
                    Mensaje.Mensaje =
                        "Alguno de los campos que intenta insertar tiene un tamaño mayor al establecido en la base de datos. <br /><br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Los datos de cadena o binarios se truncarían";
                else if (Ex.InnerException.InnerException.Message.Contains("Cannot insert duplicate key row in object"))
                    Mensaje.Mensaje =
                        "Existen campos definidos como claves que no pueden duplicarse. <br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Por favor revisar que no este ingresando datos duplicados.";
                else
                    Mensaje.Mensaje = "Informe técnico: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que elimina el registro seleccionado.
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Eliminar(string jsonObject)
        {
            Cls_Cat_Puestos_Negocio Obj_Puestos = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Eliminar registro";
                Obj_Puestos = JsonMapper.ToObject<Cls_Cat_Puestos_Negocio>(jsonObject);

                using (var dbContext = new Sistema_NormasContainer())
                {
                    var _puestos = dbContext.Cat_Puestos.Where(u => u.Puesto_ID == Obj_Puestos.Puesto_ID).First();
                    dbContext.Cat_Puestos.Remove(_puestos);
                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "La operación se completó sin problemas.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Informe Técnico";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.InnerException.Message.Contains("The DELETE statement conflicted with the REFERENCE constraint"))
                {
                    try
                    {
                        using (var dbContext = new Sistema_NormasContainer())
                        {
                            var _puesto = dbContext.Cat_Puestos.Where(u => u.Puesto_ID == Obj_Puestos.Puesto_ID).First();
                            var _estatus = dbContext.Apl_Estatus.Where(u => u.Estatus == "ELIMINADO").First();

                            _puesto.Estatus_ID = _estatus.Estatus_ID;
                            _puesto.Usuario_Modifico = Cls_Sesiones.Usuario;
                            _puesto.Fecha_Modifico = new DateTime?(DateTime.Now);

                            dbContext.SaveChanges();
                            Mensaje.Estatus = "success";
                            Mensaje.Mensaje = "La operación se completó sin problemas.";
                        }
                    }
                    catch { }
                }
                //Mensaje.Mensaje =
                //        "La operación de eliminar el registro fue revocada. <br /><br />" +
                //        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; El registro que intenta eliminar ya se encuentra en uso.";
                else
                    Mensaje.Mensaje = "Informe técnico: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que realiza la búsqueda de la area.
        /// <returns>Listado de las areas  filtradas por nombre</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Puestos_Por_Nombre(string jsonObject)
        {
            Cls_Cat_Puestos_Negocio Obj_Puestos = null;
            string Json_Resultado = string.Empty;
            List<Cls_Cat_Puestos_Negocio> Lista_Empaques = new List<Cls_Cat_Puestos_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Validaciones";
                Obj_Puestos = JsonMapper.ToObject<Cls_Cat_Puestos_Negocio>(jsonObject);

                using (var dbContext = new Sistema_NormasContainer())
                {
                    var _puestos = (from _puesto in dbContext.Cat_Puestos
                                    join _estatus in dbContext.Apl_Estatus on _puesto.Estatus_ID equals _estatus.Estatus_ID
                                    from _departamentos in dbContext.Cat_Departamentos.Where(x => x.Departamento_ID == _puesto.Departamento_ID).DefaultIfEmpty()

                                    where _puesto.Puesto_ID.Equals(Obj_Puestos.Puesto_ID) &&
                                    _puesto.Nombre.Equals(Obj_Puestos.Nombre)

                                    select new Cls_Cat_Puestos_Negocio
                                    {
                                        Puesto_ID = _puesto.Puesto_ID,
                                        Nombre = _puesto.Nombre,
                                        Estatus = _estatus.Estatus,
                                        Departamento= _departamentos.Nombre,
                                        Departamento_ID=_departamentos.Departamento_ID
                                      
                                    }).OrderByDescending(u => u.Nombre);

                    //if (Obj_Puestos.Area_ID != 0)
                    //{
                    //    _puestos = (from _puesto in dbContext.Cat_Puestos
                    //                join _estatus in dbContext.Apl_Estatus on _puesto.Estatus_ID equals _estatus.Estatus_ID
                    //                join _areas in dbContext.Cat_Areas on _puesto.Area_ID equals _areas.Area_ID

                    //                where (_puesto.Puesto_ID.Equals(Obj_Puestos.Puesto_ID) &&
                    //                _puesto.Area_ID.Equals(Obj_Puestos.Area_ID)) ||
                    //                (_puesto.Nombre.Equals(Obj_Puestos.Nombre) &&
                    //                _puesto.Area_ID.Equals(Obj_Puestos.Area_ID))

                    //                select new Cls_Cat_Puestos_Negocio
                    //                {
                    //                    Puesto_ID = _puesto.Puesto_ID,
                    //                    Nombre = _puesto.Nombre,
                    //                    Estatus = _estatus.Estatus,
                    //                    Area = _areas.Nombre,
                    //                    Area_ID = _puesto.Area_ID
                    //                }).OrderByDescending(u => u.Nombre);
                    //}

                    if (_puestos.Any())
                    {
                        if (Obj_Puestos.Puesto_ID == 0)
                        {
                            Mensaje.Estatus = "error";
                            if (!string.IsNullOrEmpty(Obj_Puestos.Nombre))
                                Mensaje.Mensaje = "El nombre y la área ingresados ya se encuentran registrados.";
                        }
                        else
                        {
                            var item_edit = _puestos.Where(u => u.Puesto_ID == Obj_Puestos.Puesto_ID);

                            if (item_edit.Count() == 1)
                                Mensaje.Estatus = "success";
                            else
                            {
                                Mensaje.Estatus = "error";
                                if (!string.IsNullOrEmpty(Obj_Puestos.Nombre))
                                    Mensaje.Mensaje = "El nombre y la área ingresados ya se encuentran registrados.";
                            }
                        }
                    }
                    else
                        Mensaje.Estatus = "success";

                    Json_Resultado = JsonMapper.ToJson(Mensaje);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que realiza la búsqueda de las areas.
        /// </summary>
        /// <returns>Listado serializado con las areas según los filtros aplícados</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Puestos_Por_Filtros(string jsonObject)
        {
            Cls_Cat_Puestos_Negocio Obj_Puestos = null;
            string Json_Resultado = string.Empty;
            List<Cls_Cat_Puestos_Negocio> Lista_puestos = new List<Cls_Cat_Puestos_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                Obj_Puestos = JsonMapper.ToObject<Cls_Cat_Puestos_Negocio>(jsonObject);

                using (var dbContext = new Sistema_NormasContainer())
                {
                    int empresa_id = string.IsNullOrEmpty(Cls_Sesiones.Empresa_ID) ? -1 : Convert.ToInt32(Cls_Sesiones.Empresa_ID);

                    var Puestos = (from _puestos in dbContext.Cat_Puestos
                                 join _estatus in dbContext.Apl_Estatus on _puestos.Estatus_ID equals _estatus.Estatus_ID
                                   from _departamentos in dbContext.Cat_Departamentos.Where(x => x.Departamento_ID == _puestos.Departamento_ID).DefaultIfEmpty()

                                   where //_puestos.Empresa_ID.Equals(empresa_id) &&
                                 _estatus.Estatus != "ELIMINADO" &&
                                 (!string.IsNullOrEmpty(Obj_Puestos.Nombre) ? _puestos.Nombre.ToLower().Contains(Obj_Puestos.Nombre.ToLower()) : true)

                                 select new Cls_Cat_Puestos_Negocio
                                 {
                                     Puesto_ID = _puestos.Puesto_ID,
                                     Nombre = _puestos.Nombre,
                                     Estatus = _estatus.Estatus,
                                     Estatus_ID = _puestos.Estatus_ID,
                                     Departamento= _departamentos.Nombre,
                                     //Departamento_ID = _puestos.Departamento_ID,
                                     Descripcion = _puestos.Descripcion,
                                 }).OrderByDescending(u => u.Nombre);
                    
                    foreach (var p in Puestos)
                        Lista_puestos.Add((Cls_Cat_Puestos_Negocio)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_puestos);


                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método para consultar los estatus.
        /// </summary>
        /// <returns>Listado serializado de los estatus</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Estatus()
        {
            string Json_Resultado = string.Empty;
            List<Apl_Estatus> Lista_estatus = new List<Apl_Estatus>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                using (var dbContext = new Sistema_NormasContainer())
                {
                    var Estatus = from _empresas in dbContext.Apl_Estatus
                                  select new { _empresas.Estatus, _empresas.Estatus_ID };
                    
                    Json_Resultado = JsonMapper.ToJson(Estatus.ToList());
                    
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método para consultar las areas.
        /// </summary>
        /// <returns>Listado serializado de los estatus</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Areas()
        {
            string Json_Resultado = string.Empty;
            List<Cat_Areas> Lista_areas = new List<Cat_Areas>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                using (var dbContext = new Sistema_NormasContainer())
                {
                    var Areas = from _areas in dbContext.Cat_Areas
                                  select new { _areas.Nombre, _areas.Area_ID };

                    Json_Resultado = JsonMapper.ToJson(Areas.ToList());

                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Departamentos()
        {
            string Json_Resultado = string.Empty;
            List<Cat_Departamentos> Lista_departamentos = new List<Cat_Departamentos>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                using (var dbContext = new Sistema_NormasContainer())
                {
                    var Departamento = from _departamento in dbContext.Cat_Departamentos
                                select new { _departamento.Nombre, _departamento.Departamento_ID };

                    Json_Resultado = JsonMapper.ToJson(Departamento.ToList());

                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }

        #endregion
    }
}
