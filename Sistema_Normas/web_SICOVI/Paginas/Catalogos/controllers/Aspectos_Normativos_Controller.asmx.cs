﻿using admin_cambios_procesos.Models.Ayudante;
using datos_cambios_procesos;
using LitJson;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using web_cambios_procesos.Models.Negocio;

namespace web_cambios_procesos.Paginas.Catalogos.controllers
{
    /// <summary>
    /// Summary description for Aspectos_Normativos_Controller
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class Aspectos_Normativos_Controller : System.Web.Services.WebService
    {
        /// <summary>
        /// Método que realiza el alta del un nuevo aspecto normativo
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Alta(string jsonObject)
        {
            //parametros para la consulta
            Cls_Cat_Aspectos_Normativos_Negocio Obj_Aspecto = null;
            //resultado de la funcion
            string Json_Resultado = string.Empty;
            //mensaje que se regresa
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Insertar";
                Obj_Aspecto = JsonMapper.ToObject<Cls_Cat_Aspectos_Normativos_Negocio>(jsonObject);

                using (var dbContext = new Sistema_NormasContainer())
                {
                    //consulta a la base de datos
                    var _aspecto_normativo = new Cat_Aspectos_Normativos();
                    _aspecto_normativo.Nombre = Obj_Aspecto.Nombre;
                    _aspecto_normativo.Descripcion = Obj_Aspecto.Descripcion;
                    _aspecto_normativo.Estatus = Obj_Aspecto.Estatus;
                    _aspecto_normativo.Usuario_Creo = Cls_Sesiones.Usuario;
                    _aspecto_normativo.Fecha_Creo = new DateTime?(DateTime.Now).Value;

                    dbContext.Cat_Aspectos_Normativos.Add(_aspecto_normativo);
                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "La opercion de completo sin problemas.";
                }
            }
            //Mostrar mensajes de error
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Technical report";
                Mensaje.Estatus = "error";
                //validar el mensaje 
                if (Ex.InnerException.Message.Contains("String or binary data would be truncated"))
                    Mensaje.Mensaje =
                        "Any of the fields you are trying to insert have a size larger than that set in the database. <br /><br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; String or binary data would be truncated";
                //validar el mensaje 
                else if (Ex.InnerException.InnerException.Message.Contains("Cannot insert duplicate key row in object"))
                    Mensaje.Mensaje =
                        "There are defined fields as keys that can not be duplicated. <br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Please check that you are not entering duplicate data.";
                //validar el mensaje 
                else
                    Mensaje.Mensaje = "Technical report: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }

        /// <summary>
        /// Método que realiza la actualización de los datos del aspecto normativo seleccionado.
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Actualizar(string jsonObject)
        {
            //parametros del consulta
            Cls_Cat_Aspectos_Normativos_Negocio Obj_Aspecto = null;
            //resultado de la funcion
            string Json_Resultado = string.Empty;
            //mensaje que se regresa
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Actualizar registro";
                Obj_Aspecto = JsonMapper.ToObject<Cls_Cat_Aspectos_Normativos_Negocio>(jsonObject);

                using (var dbContext = new Sistema_NormasContainer())
                {
                    //consulta a la base de datos
                    var _aspecto = dbContext.Cat_Aspectos_Normativos.Where(u => u.Aspecto_Normativo_ID == Obj_Aspecto.Aspecto_Normativo_ID).First();
                    _aspecto.Nombre = Obj_Aspecto.Nombre;
                    _aspecto.Descripcion = Obj_Aspecto.Descripcion;
                    _aspecto.Estatus = Obj_Aspecto.Estatus;
                    _aspecto.Usuario_Modifico = Cls_Sesiones.Usuario;
                    _aspecto.Fecha_Modifico = new DateTime?(DateTime.Now).Value;

                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "La operacion de completo sin problemas.";
                }
            }
            //Mostrar mensajes de error
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Technical report";
                Mensaje.Estatus = "error";
                //validar mensaje
                if (Ex.InnerException.Message.Contains("String or binary data would be truncated"))
                    Mensaje.Mensaje =
                        "Any of the fields you are trying to insert have a size larger than that set in the database. <br /><br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; String or binary data would be truncated";
                //validar mensaje
                else if (Ex.InnerException.InnerException.Message.Contains("Cannot insert duplicate key row in object"))
                    Mensaje.Mensaje =
                        "There are defined fields as keys that can not be duplicated. <br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp;Please check that you are not entering duplicate data.";
                //mostrar mensaje
                else
                    Mensaje.Mensaje = "Technical report: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }

        /// <summary>
        /// Método que elimina el registro seleccionado.
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Eliminar(string jsonObject)
        {
            //parametros del consulta
            Cls_Cat_Aspectos_Normativos_Negocio Obj_Aspecto = null;
            //resultado de la funcion
            string Json_Resultado = string.Empty;
            //mensaje que se regresa
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Eliminar registro";
                Obj_Aspecto = JsonMapper.ToObject<Cls_Cat_Aspectos_Normativos_Negocio>(jsonObject);

                using (var dbContext = new Sistema_NormasContainer())
                {
                    //consulta a la base de datos
                    var _aspecto = dbContext.Cat_Aspectos_Normativos.Where(u => u.Aspecto_Normativo_ID == Obj_Aspecto.Aspecto_Normativo_ID).First();
                    dbContext.Cat_Aspectos_Normativos.Remove(_aspecto);

                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "La operacion de completo sin ploblemas.";
                }
            }
            // Mostrar mensajes de error
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Informe Técnico";
                Mensaje.Estatus = "error";
                //validar mensaje 
                if (Ex.InnerException.InnerException.Message.Contains("The DELETE statement conflicted with the REFERENCE constraint"))
                    Mensaje.Mensaje =
                        "The delete record operation was revoked. <br /><br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; The record you are trying to delete is already in use.";
                //validar mensaje
                else
                    Mensaje.Mensaje = "Technical report: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }

        /// <summary>
        /// Método que realiza la búsqueda de los aspectos normativos.
        /// </summary>
        /// <returns>Listado serializado según los filtros aplícados</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Aspectos_Normarivos_Por_Filtros(string jsonObject)
        {
            //parametros de la consulta 
            Cls_Cat_Aspectos_Normativos_Negocio Obj_Aspectos = null;
            //resultado de la funcion
            string Json_Resultado = string.Empty;
            //lista de resultados 
            List<Cls_Cat_Aspectos_Normativos_Negocio> Lista_Aspectos = new List<Cls_Cat_Aspectos_Normativos_Negocio>();
            //mensaje que se regresa
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                Obj_Aspectos = JsonMapper.ToObject<Cls_Cat_Aspectos_Normativos_Negocio>(jsonObject);

                using (var dbContext = new Sistema_NormasContainer())
                {
                    //consulta a la base de datos
                    var Aspectos = (from _aspectos in dbContext.Cat_Aspectos_Normativos
                                    where
                                   ((Obj_Aspectos.Aspecto_Normativo_ID != 0) ? _aspectos.Aspecto_Normativo_ID.Equals(Obj_Aspectos.Aspecto_Normativo_ID) : true)
                                    select new Cls_Cat_Aspectos_Normativos_Negocio
                                    {
                                        Aspecto_Normativo_ID = _aspectos.Aspecto_Normativo_ID,
                                        Nombre = _aspectos.Nombre,
                                        Descripcion = _aspectos.Descripcion,
                                        Estatus = _aspectos.Estatus
                                    }).OrderByDescending(u => u.Aspecto_Normativo_ID);
                    //recorrer la lista de datos 
                    foreach (var p in Aspectos)
                        Lista_Aspectos.Add((Cls_Cat_Aspectos_Normativos_Negocio)p);
                    Json_Resultado = JsonMapper.ToJson(Lista_Aspectos);
                }
            }
            //Mostrar mensaje de error
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            return Json_Resultado;
        }

        /// <summary>
        /// Método que realiza la búsqueda de aspectos normativos.
        /// </summary>
        /// <returns>Listado</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Aspectos_Normativos_Por_Nombre(string jsonObject)
        {
            //parametros de la consulta
            Cls_Cat_Aspectos_Normativos_Negocio Obj_Aspecto = null;
            //resultado de la funcion
            string Json_Resultado = string.Empty;
            //lista de registros
            List<Cls_Cat_Aspectos_Normativos_Negocio> Lista_Unidades = new List<Cls_Cat_Aspectos_Normativos_Negocio>();
            //mensaje que se regresa
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Validaciones";
                Obj_Aspecto = JsonMapper.ToObject<Cls_Cat_Aspectos_Normativos_Negocio>(jsonObject);

                using (var dbContext = new Sistema_NormasContainer())
                {
                    var _aspectos_normativos = (from _aspecto in dbContext.Cat_Aspectos_Normativos
                                                where _aspecto.Nombre.Equals(Obj_Aspecto.Nombre)
                                                select new Cls_Cat_Aspectos_Normativos_Negocio
                                                {
                                                    Aspecto_Normativo_ID = _aspecto.Aspecto_Normativo_ID,
                                                    Estatus = _aspecto.Estatus
                                                }).OrderByDescending(u => u.Aspecto_Normativo_ID);
                    //valida si la lista tiene algun registro
                    if (_aspectos_normativos.Any())
                    {
                        //valida si el id es igual a 0
                        if (Obj_Aspecto.Aspecto_Normativo_ID == 0)
                        {
                            Mensaje.Estatus = "error";
                            //valida el nombre
                            if (!string.IsNullOrEmpty(Obj_Aspecto.Nombre))
                                Mensaje.Mensaje = "El nombre ingresado ya se encuentra registrado.";
                        }
                        //valida si el id existe en la bd
                        else
                        {
                            //consulta a la BD
                            var item_edit = _aspectos_normativos.Where(u => u.Aspecto_Normativo_ID == Obj_Aspecto.Aspecto_Normativo_ID);
                            //valida el id del registro
                            if (item_edit.Count() == 1)
                                Mensaje.Estatus = "success";
                            //mostrar mensaje de error
                            else
                            {
                                Mensaje.Estatus = "error";
                                //validar el nombre del registro
                                if (!string.IsNullOrEmpty(Obj_Aspecto.Nombre))
                                    Mensaje.Mensaje = "El nombre ingresado ya se encuentra registrado.";
                            }
                        }
                    }
                    //mostrar mensaje de exito
                    else
                        Mensaje.Estatus = "success";

                    Json_Resultado = JsonMapper.ToJson(Mensaje);
                }
            }
            //mostrar mensaje de error
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            return Json_Resultado;
        }


        /// <summary>
        /// Método que realiza la consulta de aspectos normativosm
        /// </summary>
        /// <returns>Listado</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void Consultar_Aspectos()
        {
            //resultado que se regresa
            string Json_Resultado = string.Empty;
            //lista de registros
            List<Cls_Select2> Lst_Combo = new List<Cls_Select2>();
            //mensaje que se regresa
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                string q = string.Empty;
                NameValueCollection nvc = Context.Request.Form;

                if (!String.IsNullOrEmpty(nvc["q"]))
                    q = nvc["q"].ToString().Trim();

                using (var dbContext = new Sistema_NormasContainer())
                {
                    var Aspectos = (from _corporativo in dbContext.Cat_Aspectos_Normativos
                                    where
                                     ((string.IsNullOrEmpty(q)) ? true : _corporativo.Nombre.Contains(q))
                                    select new Cls_Select2
                                    {
                                        id = _corporativo.Aspecto_Normativo_ID.ToString(),
                                        text = _corporativo.Nombre,

                                    });
                    //recorre la lista de resultados
                    foreach (var p in Aspectos)
                        Lst_Combo.Add(p);

                    Json_Resultado = JsonMapper.ToJson(Lst_Combo);
                }
            }
            //mensaje de error
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            finally
            {
                Context.Response.Write(Json_Resultado);
            }
        }
    }
}
