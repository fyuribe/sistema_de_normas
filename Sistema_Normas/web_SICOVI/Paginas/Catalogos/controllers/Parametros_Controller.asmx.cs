﻿using datos_cambios_procesos;
using LitJson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using web_cambios_procesos.Models.Ayudante;
using web_cambios_procesos.Models.Negocio;

namespace web_cambios_procesos.Paginas.Catalogos.controllers
{
    /// <summary>
    /// Summary description for Parametros_Controller
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class Parametros_Controller : System.Web.Services.WebService
    {

        #region Metodos
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Alta(string jsonObject)
        {
            Cls_Tra_Cat_Parametros_Negocio ObjParametro = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Alta registro";
                ObjParametro = JsonMapper.ToObject<Cls_Tra_Cat_Parametros_Negocio>(jsonObject);

                using (var dbContext = new Sistema_NormasContainer())
                {
                    var _parametros= new Apl_Parametros();
                    _parametros.Empresa_ID = Convert.ToInt32(Cls_Sesiones.Empresa_ID);
                    _parametros.Prefijo = ObjParametro.Prefijo;
                    _parametros.No_Intentos_Acceso = "3";
                    _parametros.Tipo_Usuario = ObjParametro.Tipo_Usuario;
                    _parametros.Menu_ID = ObjParametro.Menu_ID == 0 ? null : ObjParametro.Menu_ID;

                    dbContext.Apl_Parametros.Add(_parametros);
                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "La operación se completó sin problemas.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Informe Técnico";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.Message.Contains("Los datos de cadena o binarios se truncarían"))
                    Mensaje.Mensaje =
                        "Alguno de los campos que intenta insertar tiene un tamaño mayor al establecido en la base de datos. <br /><br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Los datos de cadena o binarios se truncarían";
                else if (Ex.InnerException.InnerException.Message.Contains("Cannot insert duplicate key row in object"))
                    Mensaje.Mensaje =
                        "Existen campos definidos como claves que no pueden duplicarse. <br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Por favor revisar que no esté ingresando datos duplicados.";
                else
                    Mensaje.Mensaje = "Informe técnico: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }

        /// <summary>
        /// Método que realiza la actualización de los datos de la unidad seleccionada.
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Actualizar(string jsonObject)
        {
            Cls_Tra_Cat_Parametros_Negocio ObjParametro = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Actualizar registro";
                ObjParametro = JsonMapper.ToObject<Cls_Tra_Cat_Parametros_Negocio>(jsonObject);

                using (var dbContext = new Sistema_NormasContainer())
                {
                    var _parametros = dbContext.Apl_Parametros.Where(u => u.Parametro_ID == ObjParametro.Parametro_ID).First();

                    _parametros.Prefijo = ObjParametro.Prefijo;
                    _parametros.Tipo_Usuario = ObjParametro.Tipo_Usuario;
                    _parametros.Menu_ID = ObjParametro.Menu_ID == 0 ? null : ObjParametro.Menu_ID;

                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "La operación se completó sin problemas.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Informe Técnico";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.Message.Contains("Los datos de cadena o binarios se truncarían"))
                    Mensaje.Mensaje =
                        "Alguno de los campos que intenta insertar tiene un tamaño mayor al establecido en la base de datos. <br /><br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Los datos de cadena o binarios se truncarían";
                else if (Ex.InnerException.InnerException.Message.Contains("Cannot insert duplicate key row in object"))
                    Mensaje.Mensaje =
                        "Existen campos definidos como claves que no pueden duplicarse. <br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Por favor revisar que no esté ingresando datos duplicados.";
                else
                    Mensaje.Mensaje = "Informe técnico: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }

        /// <summary>
        /// Método que elimina el registro seleccionado.
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Eliminar(string jsonObject)
        {
            Cls_Tra_Cat_Parametros_Negocio ObjParametro = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Eliminar registro";
                ObjParametro = JsonMapper.ToObject<Cls_Tra_Cat_Parametros_Negocio>(jsonObject);

                using (var dbContext = new Sistema_NormasContainer())
                {
                    var _parametro = dbContext.Apl_Parametros.Where(u => u.Parametro_ID == ObjParametro.Parametro_ID).First();
                    dbContext.Apl_Parametros.Remove(_parametro);
                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "La operación se completó sin problemas.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Informe Técnico";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.InnerException.Message.Contains("The DELETE statement conflicted with the REFERENCE constraint"))
                    Mensaje.Mensaje =
                        "La operación de eliminar el registro fue revocada. <br /><br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; El registro que intenta eliminar ya se encuentra en uso.";
                else
                    Mensaje.Mensaje = "Informe técnico: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }

        /// <summary>
        /// Método que realiza la búsqueda de Procesos.
        /// </summary>
        /// <returns>Listado de procesos filtradas por nombre</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Parametro_Por_prefijo(string jsonObject)
        {
            Cls_Tra_Cat_Parametros_Negocio ObjParametro = null;
            string Json_Resultado = string.Empty;
            List<Cls_Tra_Cat_Parametros_Negocio> Lista_parametros = new List<Cls_Tra_Cat_Parametros_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Validaciones";
                ObjParametro = JsonMapper.ToObject<Cls_Tra_Cat_Parametros_Negocio>(jsonObject);

                using (var dbContext = new Sistema_NormasContainer())
                {
                    var _parametro = (from _select in dbContext.Apl_Parametros
                                      where _select.Parametro_ID.Equals(ObjParametro.Parametro_ID) ||
                                     _select.Prefijo.Equals(ObjParametro.Prefijo)
                                     select new Cls_Tra_Cat_Parametros_Negocio
                                     {
                                         Parametro_ID = _select.Parametro_ID,
                                         Prefijo = _select.Prefijo
                                     }).OrderByDescending(u => u.Parametro_ID);

                    if (_parametro.Any())
                    {
                        if (ObjParametro.Parametro_ID == 0)
                        {
                            Mensaje.Estatus = "error";
                            if (!string.IsNullOrEmpty(ObjParametro.Prefijo))
                                Mensaje.Mensaje = "El prefijo ingresado ya se encuentra registrado.";
                          
                        }
                        else
                        {
                            var item_edit = _parametro.Where(u => u.Parametro_ID == ObjParametro.Parametro_ID);

                            if (item_edit.Count() == 1)
                                Mensaje.Estatus = "success";
                            else
                            {
                                Mensaje.Estatus = "error";
                                if (!string.IsNullOrEmpty(ObjParametro.Prefijo))
                                    Mensaje.Mensaje = "El prefijo ingresado ya se encuentra registrado.";
                              
                            }
                        }
                    }
                    else
                        Mensaje.Estatus = "success";

                    Json_Resultado = JsonMapper.ToJson(Mensaje);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }

        /// <summary>
        /// Método que realiza la búsqueda de procesos.
        /// </summary>
        /// <returns>Listado serializado con las fases según los filtros aplícados</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Parametro_Por_Filtros(string jsonObject)
        {
            Cls_Tra_Cat_Parametros_Negocio objParametro = null;
            string Json_Resultado = string.Empty;
            List<Cls_Tra_Cat_Parametros_Negocio> Lista_parametros = new List<Cls_Tra_Cat_Parametros_Negocio>();
            int empresa = string.IsNullOrEmpty(Cls_Sesiones.Empresa_ID) ? -1 : Convert.ToInt32(Cls_Sesiones.Empresa_ID);
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                objParametro = JsonMapper.ToObject<Cls_Tra_Cat_Parametros_Negocio>(jsonObject);

                using (var dbContext = new Sistema_NormasContainer())
                {
                    var parametro = (from _select in dbContext.Apl_Parametros
                                     where _select.Empresa_ID.Equals(empresa)&&(
                                        ((_select.Prefijo.ToLower().Contains(objParametro.Prefijo.ToLower()) && !string.IsNullOrEmpty(objParametro.Prefijo))) ||
                                        (string.IsNullOrEmpty(objParametro.Prefijo)))
                                     
                                    select new
                                    {
                                        Parametro_ID = _select.Parametro_ID,
                                        Prefijo = _select.Prefijo,
                                        Tipo_Usuario = _select.Tipo_Usuario,
                                        Empresa_ID = _select.Empresa_ID,
                                        Menu_ID=_select.Menu_ID,
                                        Nombre_Mostrar = (_select.Menu_ID == null) ? null : (dbContext.Apl_Menus.Where(e => e.Menu_ID == _select.Menu_ID).Select(e => e.Nombre_Mostrar).FirstOrDefault())

                                    }).OrderByDescending(u => u.Parametro_ID);

                    Json_Resultado = JsonMapper.ToJson(parametro.ToList());
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string ConsultarTipoUsuario()
        {
            string Json_Resultado = string.Empty;
            List<Cls_Apl_Tipos_Usuarios_Negocio> Lista_fase = new List<Cls_Apl_Tipos_Usuarios_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {

                using (var dbContext = new Sistema_NormasContainer())
                {
                    var tipos_usuarios = from _select in dbContext.Apl_Tipos_Usuarios
                             
                                select new { _select.Tipo_Usuario_ID, _select.Nombre };


                    Json_Resultado = JsonMapper.ToJson(tipos_usuarios.ToList());


                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Menus()
        {
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                using (var dbContext = new Sistema_NormasContainer())
                {
                    var menus = from _menu in dbContext.Apl_Menus
                                  select new { _menu.Menu_ID, _menu.Nombre_Mostrar };
                    Json_Resultado = JsonMapper.ToJson(menus.ToList());
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }
        #endregion
    }
}
