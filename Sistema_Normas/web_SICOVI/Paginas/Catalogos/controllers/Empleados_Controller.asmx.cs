﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using datos_cambios_procesos;
using LitJson;
using System.Web.Script.Services;

using web_cambios_procesos.Models.Negocio;
using web_cambios_procesos.Models.Ayudante;
using System.Collections.Specialized;

namespace web_cambios_procesos.Paginas.Catalogos.controllers
{
    /// <summary>
    /// Summary description for Empleados_Controller
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class Empleados_Controller : System.Web.Services.WebService
    {
        #region (Métodos)
        /// <summary>
        /// Método que realiza el alta de la area.
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Alta(string jsonObject)
        {
            Cls_Cat_Empleados_Negocio Obj_Empleados = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Alta registro";
                Obj_Empleados = JsonMapper.ToObject<Cls_Cat_Empleados_Negocio>(jsonObject);

                using (var dbContext = new Sistema_NormasContainer())
                {
                    var _empleados = new Cat_Empleados();
                    _empleados.Empresa_ID = Convert.ToInt32(Cls_Sesiones.Empresa_ID);
                    _empleados.No_Empleado = Obj_Empleados.No_Empleado;
                    _empleados.Nombre = Obj_Empleados.Nombre;
                    _empleados.Apellidos = Obj_Empleados.Apellidos;
                    _empleados.Nivel = Obj_Empleados.Nivel;
                    _empleados.Email = Obj_Empleados.Email;
                    _empleados.Planta = Obj_Empleados.Planta;
                    _empleados.Estatus = Obj_Empleados.Estatus;
                  //  _empleados.Departamento_ID = Obj_Empleados.Departamento_ID;
                    _empleados.Observaciones = Obj_Empleados.Observaciones;
                    _empleados.Usuario_Creo = Cls_Sesiones.Usuario;
                    _empleados.Fecha_Creo = new DateTime?(DateTime.Now).Value;

                    dbContext.Cat_Empleados.Add(_empleados);
                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "La operación se completó sin problemas.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Informe Técnico";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.Message.Contains("Los datos de cadena o binarios se truncarían"))
                    Mensaje.Mensaje =
                        "Alguno de los campos que intenta insertar tiene un tamaño mayor al establecido en la base de datos. <br /><br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Los datos de cadena o binarios se truncarían";
                else if (Ex.InnerException.InnerException.Message.Contains("Cannot insert duplicate key row in object"))
                    Mensaje.Mensaje =
                        "Existen campos definidos como claves que no pueden duplicarse. <br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Por favor revisar que no esté ingresando datos duplicados.";
                else
                    Mensaje.Mensaje = "Informe técnico: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que realiza la actualización de los datos de la area seleccionada.
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Actualizar(string jsonObject)
        {
            Cls_Cat_Empleados_Negocio Obj_Empleados = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Actualizar registro";
                Obj_Empleados = JsonMapper.ToObject<Cls_Cat_Empleados_Negocio>(jsonObject);

                using (var dbContext = new Sistema_NormasContainer())
                {
                    var _empleado = dbContext.Cat_Empleados.Where(u => u.Empleado_ID == Obj_Empleados.Empleado_ID).First();

                    _empleado.No_Empleado = Obj_Empleados.No_Empleado;
                    _empleado.Nombre = Obj_Empleados.Nombre;
                    _empleado.Apellidos = Obj_Empleados.Apellidos;
                    _empleado.Nivel = Obj_Empleados.Nivel;
                    _empleado.Email = Obj_Empleados.Email;
                    _empleado.Planta = Obj_Empleados.Planta;
                    _empleado.Estatus = Obj_Empleados.Estatus;
                  //  _empleado.Departamento_ID = Obj_Empleados.Departamento_ID;
                    _empleado.Observaciones = Obj_Empleados.Observaciones;
                    _empleado.Usuario_Modifico = Cls_Sesiones.Usuario;
                    _empleado.Fecha_Modifico = new DateTime?(DateTime.Now);

                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "La operación se completó sin problemas.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Informe Técnico";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.Message.Contains("Los datos de cadena o binarios se truncarían"))
                    Mensaje.Mensaje =
                        "Alguno de los campos que intenta insertar tiene un tamaño mayor al establecido en la base de datos. <br /><br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Los datos de cadena o binarios se truncarían";
                else if (Ex.InnerException.InnerException.Message.Contains("Cannot insert duplicate key row in object"))
                    Mensaje.Mensaje =
                        "Existen campos definidos como claves que no pueden duplicarse. <br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Por favor revisar que no esté ingresando datos duplicados.";
                else
                    Mensaje.Mensaje = "Informe técnico: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que elimina el registro seleccionado.
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Eliminar(string jsonObject)
        {
            Cls_Cat_Empleados_Negocio Obj_Empleados = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Eliminar registro";
                Obj_Empleados = JsonMapper.ToObject<Cls_Cat_Empleados_Negocio>(jsonObject);

                using (var dbContext = new Sistema_NormasContainer())
                {
                    var _empleado = dbContext.Cat_Empleados.Where(u => u.Empleado_ID == Obj_Empleados.Empleado_ID).First();
                    dbContext.Cat_Empleados.Remove(_empleado);
                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "La operación se completo sin problemas.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Informe Técnico";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.InnerException.Message.Contains("The DELETE statement conflicted with the REFERENCE constraint"))
                {
                    try
                    {
                        using (var dbContext = new Sistema_NormasContainer())
                        {
                            var _empleado = dbContext.Cat_Empleados.Where(u => u.Empleado_ID == Obj_Empleados.Empleado_ID).First();
                            var _estatus = dbContext.Apl_Estatus.Where(u => u.Estatus == "ELIMINADO").First();

                            _empleado.Estatus = _estatus.Estatus;
                            _empleado.Usuario_Modifico = Cls_Sesiones.Usuario;
                            _empleado.Fecha_Modifico = new DateTime?(DateTime.Now);

                            dbContext.SaveChanges();
                            Mensaje.Estatus = "success";
                            Mensaje.Mensaje = "La operación se completó sin problemas.";
                        }
                    }
                    catch { }
                }
                //Mensaje.Mensaje =
                //        "La operación de eliminar el registro fue revocada. <br /><br />" +
                //        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; El registro que intenta eliminar ya se encuentra en uso.";
                else
                    Mensaje.Mensaje = "Informe técnico: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que realiza la búsqueda de la area.
        /// <returns>Listado de las areas  filtradas por nombre</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Empleados_Por_Nombre(string jsonObject)
        {
            Cls_Cat_Empleados_Negocio Obj_Empleados = null;
            string Json_Resultado = string.Empty;
            List<Cls_Cat_Empleados_Negocio> Lista_Empleados = new List<Cls_Cat_Empleados_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Validaciones";
                Obj_Empleados = JsonMapper.ToObject<Cls_Cat_Empleados_Negocio>(jsonObject);

                using (var dbContext = new Sistema_NormasContainer())
                {
                    var _empleado = (from _empleados in dbContext.Cat_Empleados
                                     where _empleados.Empleado_ID.Equals(Obj_Empleados.Empleado_ID) ||
                                  (_empleados.Nombre.Equals(Obj_Empleados.Nombre) && _empleados.Apellidos.Equals(Obj_Empleados.Apellidos) ) ||
                                  _empleados.No_Empleado.Equals(Obj_Empleados.No_Empleado)

                                  select new Cls_Cat_Empleados_Negocio
                                  {
                                      Empleado_ID = _empleados.Empleado_ID,
                                      No_Empleado= _empleados.No_Empleado,
                                      Nombre = _empleados.Nombre + " " + _empleados.Apellidos,
                                      Apellidos=_empleados.Apellidos,
                                      Nivel= _empleados.Nivel,
                                      Email= _empleados.Email,
                                      Planta= _empleados.Planta,
                                      Estatus = _empleados.Estatus
                                  }).OrderByDescending(u => u.Nombre);

                    if (_empleado.Any())
                    {
                        if (Obj_Empleados.Empleado_ID == 0)
                        {
                            Mensaje.Estatus = "error";
                            if (!string.IsNullOrEmpty(Obj_Empleados.Nombre))
                                Mensaje.Mensaje = "El nombre y apellidos ingresados ya se encuentran registrados.";
                            else if (!string.IsNullOrEmpty(Obj_Empleados.No_Empleado))
                                Mensaje.Mensaje = "El número de empleado ya se encuentra registrado.";
                        }
                        else
                        {
                            var item_edit = _empleado.Where(u => u.Empleado_ID == Obj_Empleados.Empleado_ID);

                            if (item_edit.Count() == 1)
                                Mensaje.Estatus = "success";
                            else
                            {
                                Mensaje.Estatus = "error";
                                if (!string.IsNullOrEmpty(Obj_Empleados.Nombre))
                                    Mensaje.Mensaje = "El nombre y apellidos ingresados ya se encuentran registrados.";
                                else if (!string.IsNullOrEmpty(Obj_Empleados.No_Empleado))
                                    Mensaje.Mensaje = "El número de empleado ya se encuentra registrado.";
                            }
                        }
                    }
                    else
                        Mensaje.Estatus = "success";

                    Json_Resultado = JsonMapper.ToJson(Mensaje);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que realiza la búsqueda de las areas.
        /// </summary>
        /// <returns>Listado serializado con las areas según los filtros aplícados</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Empleados_Por_Filtros(string jsonObject)
        {
            Cls_Cat_Empleados_Negocio Obj_Empleados = null;
            string Json_Resultado = string.Empty;
            List<Cls_Cat_Empleados_Negocio> Lista_empleados = new List<Cls_Cat_Empleados_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Obj_Empleados = JsonMapper.ToObject<Cls_Cat_Empleados_Negocio>(jsonObject);

                using (var dbContext = new Sistema_NormasContainer())
                {
                    int empresa_id = string.IsNullOrEmpty(Cls_Sesiones.Empresa_ID) ? -1 : Convert.ToInt32(Cls_Sesiones.Empresa_ID);

                    var Empleados = (from _empleados in dbContext.Cat_Empleados             
                                     //from _departamentos in dbContext.Cat_Departamentos.Where(x => x.Departamento_ID == _empleados.Departamento_ID).DefaultIfEmpty()
                                     where _empleados.Empresa_ID.Equals(empresa_id) &&
                                     _empleados.Estatus != "ELIMINADO" &&
                                 (!string.IsNullOrEmpty(Obj_Empleados.Nombre) ? _empleados.Nombre.ToLower().Contains(Obj_Empleados.Nombre.ToLower()) : true)

                                 select new Cls_Cat_Empleados_Negocio
                                 {
                                     Empleado_ID = _empleados.Empleado_ID,
                                     No_Empleado = _empleados.No_Empleado,
                                     Nombre = _empleados.Nombre,
                                     Empresa_ID = _empleados.Empresa_ID,
                                     Observaciones = _empleados.Observaciones,
                                     Apellidos = _empleados.Apellidos,
                                     Nivel = _empleados.Nivel,
                                     Email = _empleados.Email,
                                     Planta = _empleados.Planta,
                                     Empleado= _empleados.Nombre + " " + _empleados.Apellidos,
                                     Estatus = _empleados.Estatus,
                                    // Departamento=_departamentos.Nombre
                                     
                                 }).OrderByDescending(u => u.Nombre);



                    foreach (var p in Empleados)
                        Lista_empleados.Add((Cls_Cat_Empleados_Negocio)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_empleados);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void Consultar_Departamentos()
        {
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            List<Cls_Select2> Lista_Departamentos = new List<Cls_Select2>();

            try
            {
                string q = string.Empty;
                NameValueCollection nvc = Context.Request.Form;

                if (!String.IsNullOrEmpty(nvc["q"]))
                    q = nvc["q"].ToString().Trim();

                using (var dbContext = new Sistema_NormasContainer())
                {
                    var _estatus = dbContext.Apl_Estatus.Where(p => p.Estatus == "ACTIVO").First();

                    var _lst_departamentos = (from _departamentos in dbContext.Cat_Departamentos
                                              where _departamentos.Empresa_ID.ToString() == Cls_Sesiones.Empresa_ID
                                               && _departamentos.Estatus_ID == _estatus.Estatus_ID
                                              && _departamentos.Nombre.Contains(q)
                                              select new Cls_Select2
                                              {
                                                  id = _departamentos.Departamento_ID.ToString(),
                                                  text = _departamentos.Nombre,
                                                  tag = String.Empty
                                              }).OrderBy(u => u.text);

                    if (_lst_departamentos.Any())
                        foreach (var p in _lst_departamentos)
                            Lista_Departamentos.Add((Cls_Select2)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Departamentos);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color: #FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            finally
            {
                Context.Response.Write(Json_Resultado);
            }
        }
    

        #endregion
    }
}
