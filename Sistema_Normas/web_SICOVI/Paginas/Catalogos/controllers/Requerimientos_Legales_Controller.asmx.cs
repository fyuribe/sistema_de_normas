﻿using admin_cambios_procesos.Models.Ayudante;
using datos_cambios_procesos;
using LitJson;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using web_cambios_procesos.Models.Negocio;

namespace web_cambios_procesos.Paginas.Catalogos.controllers
{
    /// <summary>
    /// Summary description for Requerimientos_Legales_Controller
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class Requerimientos_Legales_Controller : System.Web.Services.WebService
    {

        /// <summary>
        /// Método que realiza el alta del un nuevo registro
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Alta(string jsonObject)
        {
            //parametros para la consulta
            Cls_Cat_Requerimientos_Legales_Negocio Obj_Requerimientos = null;
            //resultado de la funcion
            string Json_Resultado = string.Empty;
            //mensaje que se regresa
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Insertar";
                Obj_Requerimientos = JsonConvert.DeserializeObject<Cls_Cat_Requerimientos_Legales_Negocio>(jsonObject);

                using (var dbContext = new Sistema_NormasContainer())
                {
                    //consulta a la base de datos
                    var _requerimiento_legal = new Cat_Requerimientos_Legales();
                    _requerimiento_legal.Requerimiento = Obj_Requerimientos.Requerimiento;
                    _requerimiento_legal.Norma_ID = Obj_Requerimientos.Norma_ID;
                    _requerimiento_legal.Estatus = Obj_Requerimientos.Estatus;
                    _requerimiento_legal.Vigencia = Obj_Requerimientos.Vigencia;
                    _requerimiento_legal.Descripcion = Obj_Requerimientos.Descripcion;
                    _requerimiento_legal.Prioridad = Obj_Requerimientos.Prioridad;
                    _requerimiento_legal.Vigencia_Dias = Obj_Requerimientos.Vigencia_Dias;
                    _requerimiento_legal.Costo_Aproximado = Obj_Requerimientos.Costo_Aproximado;
                    _requerimiento_legal.Fecha_Actualizacion = Obj_Requerimientos.Fecha_Actualizacion;
                    _requerimiento_legal.Fecha_Entrada_Vigor = Obj_Requerimientos.Fecha_Entrada_Vigor;
                    _requerimiento_legal.Usuario_Creo = Cls_Sesiones.Usuario;
                    _requerimiento_legal.Fecha_Creo = new DateTime?(DateTime.Now).Value;

                    dbContext.Cat_Requerimientos_Legales.Add(_requerimiento_legal);
                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "La opercion de completo sin problemas.";
                }
            }
            //Mostrar mensajes de error
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Technical report";
                Mensaje.Estatus = "error";
                //validar el mensaje 
                if (Ex.InnerException.Message.Contains("String or binary data would be truncated"))
                    Mensaje.Mensaje =
                        "Any of the fields you are trying to insert have a size larger than that set in the database. <br /><br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; String or binary data would be truncated";
                //validar el mensaje 
                else if (Ex.InnerException.InnerException.Message.Contains("Cannot insert duplicate key row in object"))
                    Mensaje.Mensaje =
                        "There are defined fields as keys that can not be duplicated. <br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Please check that you are not entering duplicate data.";
                //validar el mensaje 
                else
                    Mensaje.Mensaje = "Technical report: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }

        /// <summary>
        /// Método que realiza la actualización de los datos del registro seleccionado.
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Actualizar(string jsonObject)
        {
            //parametros del consulta
            Cls_Cat_Requerimientos_Legales_Negocio Obj_Requerimientos = null;
            //resultado de la funcion
            string Json_Resultado = string.Empty;
            //mensaje que se regresa
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Actualizar registro";
                Obj_Requerimientos = JsonConvert.DeserializeObject<Cls_Cat_Requerimientos_Legales_Negocio>(jsonObject);


                using (var dbContext = new Sistema_NormasContainer())
                {
                    //consulta a la base de datos
                    var _requerimiento_legal = dbContext.Cat_Requerimientos_Legales.Where(u => u.Requerimiento_Legal_ID == Obj_Requerimientos.Requerimiento_Legal_ID).First();
                    _requerimiento_legal.Requerimiento = Obj_Requerimientos.Requerimiento;
                    _requerimiento_legal.Norma_ID = Obj_Requerimientos.Norma_ID;
                    _requerimiento_legal.Estatus = Obj_Requerimientos.Estatus;
                    _requerimiento_legal.Vigencia = Obj_Requerimientos.Vigencia;
                    _requerimiento_legal.Descripcion = Obj_Requerimientos.Descripcion;
                    _requerimiento_legal.Prioridad = Obj_Requerimientos.Prioridad;
                    _requerimiento_legal.Vigencia_Dias = Obj_Requerimientos.Vigencia_Dias;
                    _requerimiento_legal.Costo_Aproximado = Obj_Requerimientos.Costo_Aproximado;
                    _requerimiento_legal.Fecha_Actualizacion = Obj_Requerimientos.Fecha_Actualizacion;
                    _requerimiento_legal.Fecha_Entrada_Vigor = Obj_Requerimientos.Fecha_Entrada_Vigor;
                    _requerimiento_legal.Usuario_Modifico = Cls_Sesiones.Usuario;
                    _requerimiento_legal.Fecha_Modifico = new DateTime?(DateTime.Now).Value;

                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "La operacion de completo sin problemas.";
                }
            }
            //Mostrar mensajes de error
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Technical report";
                Mensaje.Estatus = "error";
                //validar mensaje
                if (Ex.InnerException.Message.Contains("String or binary data would be truncated"))
                    Mensaje.Mensaje =
                        "Any of the fields you are trying to insert have a size larger than that set in the database. <br /><br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; String or binary data would be truncated";
                //validar mensaje
                else if (Ex.InnerException.InnerException.Message.Contains("Cannot insert duplicate key row in object"))
                    Mensaje.Mensaje =
                        "There are defined fields as keys that can not be duplicated. <br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp;Please check that you are not entering duplicate data.";
                //mostrar mensaje
                else
                    Mensaje.Mensaje = "Technical report: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }

        /// <summary>
        /// Método que elimina el registro seleccionado.
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Eliminar(string jsonObject)
        {
            //parametros del consulta
            Cls_Cat_Requerimientos_Legales_Negocio Obj_Requerimientos = null;
            //resultado de la funcion
            string Json_Resultado = string.Empty;
            //mensaje que se regresa
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Eliminar registro";
                Obj_Requerimientos = JsonMapper.ToObject<Cls_Cat_Requerimientos_Legales_Negocio>(jsonObject);

                using (var dbContext = new Sistema_NormasContainer())
                {
                    //consulta a la base de datos
                    var _requerimientos_legales = dbContext.Cat_Requerimientos_Legales.Where(u => u.Requerimiento_Legal_ID == Obj_Requerimientos.Requerimiento_Legal_ID).First();
                    dbContext.Cat_Requerimientos_Legales.Remove(_requerimientos_legales);

                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "La operacion de completo sin ploblemas.";
                }
            }
            // Mostrar mensajes de error
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Informe Técnico";
                Mensaje.Estatus = "error";
                //validar mensaje 
                if (Ex.InnerException.InnerException.Message.Contains("The DELETE statement conflicted with the REFERENCE constraint"))
                    Mensaje.Mensaje =
                        "The delete record operation was revoked. <br /><br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; The record you are trying to delete is already in use.";
                //validar mensaje
                else
                    Mensaje.Mensaje = "Technical report: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }

        /// <summary>
        /// Método que realiza la búsqueda de registros.
        /// </summary>
        /// <returns>Listado serializado según los filtros aplícados</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Requerimientos_Legales_Por_Filtros(string jsonObject)
        {
            //parametros de la consulta 
            Cls_Cat_Requerimientos_Legales_Negocio Obj_Requerimientos = null;
            //resultado de la funcion
            string Json_Resultado = string.Empty;
            //lista de resultados 
            List<Cls_Cat_Requerimientos_Legales_Negocio> Lista_Requerimientos = new List<Cls_Cat_Requerimientos_Legales_Negocio>();
            //mensaje que se regresa
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                Obj_Requerimientos = JsonMapper.ToObject<Cls_Cat_Requerimientos_Legales_Negocio>(jsonObject);

                using (var dbContext = new Sistema_NormasContainer())
                {
                    //consulta a la base de datos
                    var requerimientos = (from _requerimientos in dbContext.Cat_Requerimientos_Legales
                                          join _normas in dbContext.Cat_Normas on _requerimientos.Norma_ID equals _normas.Norma_ID
                                 where
                                ((Obj_Requerimientos.Requerimiento_Legal_ID != 0) ? _requerimientos.Requerimiento_Legal_ID.Equals(Obj_Requerimientos.Requerimiento_Legal_ID) : true)
                                 select new Cls_Cat_Requerimientos_Legales_Negocio
                                 {
                                     Requerimiento_Legal_ID = _requerimientos.Requerimiento_Legal_ID,
                                     Requerimiento = _requerimientos.Requerimiento,                             
                                     Estatus = _requerimientos.Estatus,
                                     Vigencia = _requerimientos.Vigencia,
                                     Descripcion = _requerimientos.Descripcion,
                                     Prioridad = _requerimientos.Prioridad,
                                     Vigencia_Dias = _requerimientos.Vigencia_Dias,
                                     Costo_Aproximado = _requerimientos.Costo_Aproximado,
                                     Fecha_Actualizacion = _requerimientos.Fecha_Actualizacion,
                                     Fecha_Entrada_Vigor = _requerimientos.Fecha_Entrada_Vigor,
                                     Norma_ID = _requerimientos.Norma_ID,
                                     Norma = _normas.Nombre

                                 }).OrderByDescending(u => u.Requerimiento_Legal_ID);
                    //recorrer la lista de datos 
                    foreach (var p in requerimientos)
                        Lista_Requerimientos.Add((Cls_Cat_Requerimientos_Legales_Negocio)p);
                    Json_Resultado = JsonMapper.ToJson(Lista_Requerimientos);
                }
            }
            //Mostrar mensaje de error
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            return Json_Resultado;
        }

        /// <summary>
        /// Método que realiza la búsqueda de registros.
        /// </summary>
        /// <returns>Listado</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Requerimientos_Legales_Por_Nombre(string jsonObject)
        {
            //parametros de la consulta
            Cls_Cat_Requerimientos_Legales_Negocio Obj_Requerimientos = null;
            //resultado de la funcion
            string Json_Resultado = string.Empty;
            //lista de registros
            List<Cls_Cat_Requerimientos_Legales_Negocio> Lista_Requerimientos = new List<Cls_Cat_Requerimientos_Legales_Negocio>();
            //mensaje que se regresa
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Validaciones";
                Obj_Requerimientos = JsonMapper.ToObject<Cls_Cat_Requerimientos_Legales_Negocio>(jsonObject);

                using (var dbContext = new Sistema_NormasContainer())
                {
                    var _requerimientos_legales = (from _requerimiento in dbContext.Cat_Requerimientos_Legales
                                              where _requerimiento.Requerimiento.Equals(Obj_Requerimientos.Requerimiento)
                                              select new Cls_Cat_Requerimientos_Legales_Negocio
                                              {
                                                  Requerimiento_Legal_ID = _requerimiento.Requerimiento_Legal_ID,
                                                  Estatus = _requerimiento.Estatus
                                              }).OrderByDescending(u => u.Requerimiento_Legal_ID);
                    //valida si la lista tiene algun registro
                    if (_requerimientos_legales.Any())
                    {
                        //valida si el id es igual a 0
                        if (Obj_Requerimientos.Requerimiento_Legal_ID == 0)
                        {
                            Mensaje.Estatus = "error";
                            //valida el nombre
                            if (!string.IsNullOrEmpty(Obj_Requerimientos.Requerimiento))
                                Mensaje.Mensaje = "El nombre ingresado ya se encuentra registrado.";
                        }
                        //valida si el id existe en la bd
                        else
                        {
                            //consulta a la BD
                            var item_edit = _requerimientos_legales.Where(u => u.Requerimiento_Legal_ID == Obj_Requerimientos.Requerimiento_Legal_ID);
                            //valida el id del registro
                            if (item_edit.Count() == 1)
                                Mensaje.Estatus = "success";
                            //mostrar mensaje de error
                            else
                            {
                                Mensaje.Estatus = "error";
                                //validar el nombre del registro
                                if (!string.IsNullOrEmpty(Obj_Requerimientos.Requerimiento))
                                    Mensaje.Mensaje = "El nombre ingresado ya se encuentra registrado.";
                            }
                        }
                    }
                    //mostrar mensaje de exito
                    else
                        Mensaje.Estatus = "success";

                    Json_Resultado = JsonMapper.ToJson(Mensaje);
                }
            }
            //mostrar mensaje de error
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            return Json_Resultado;
        }

        /// <summary>
        /// Método que realiza la consulta de normas
        /// </summary>
        /// <returns>Listado</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void Consultar_Normas()
        {
            //resultado que se regresa
            string Json_Resultado = string.Empty;
            //lista de registros
            List<Cls_Select2> Lst_Combo = new List<Cls_Select2>();
            //mensaje que se regresa
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                string q = string.Empty;
                NameValueCollection nvc = Context.Request.Form;

                if (!String.IsNullOrEmpty(nvc["q"]))
                    q = nvc["q"].ToString().Trim();

                using (var dbContext = new Sistema_NormasContainer())
                {
                    var Normas = (from _normas in dbContext.Cat_Normas
                                  where
                                   ((string.IsNullOrEmpty(q)) ? true : _normas.Nombre.Contains(q))
                                  select new Cls_Select2
                                  {
                                      id = _normas.Norma_ID.ToString(),
                                      text = _normas.Nombre,

                                  });
                    //recorre la lista de resultados
                    foreach (var p in Normas)
                        Lst_Combo.Add(p);

                    Json_Resultado = JsonMapper.ToJson(Lst_Combo);
                }
            }
            //mensaje de error
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            finally
            {
                Context.Response.Write(Json_Resultado);
            }
        }
       
        /// <summary>
        /// Método que realiza la consulta de requerimientos legales
        /// </summary>
        /// <returns>Listado</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void Consultar_Requerimientos_Legales()
        {
            //resultado que se regresa
            string Json_Resultado = string.Empty;
            //lista de registros
            List<Cls_Select2> Lst_Combo = new List<Cls_Select2>();
            //mensaje que se regresa
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                string q = string.Empty;
                NameValueCollection nvc = Context.Request.Form;

                if (!String.IsNullOrEmpty(nvc["q"]))
                    q = nvc["q"].ToString().Trim();

                using (var dbContext = new Sistema_NormasContainer())
                {
                    var Requerimientos = (from _requerimientos in dbContext.Cat_Requerimientos_Legales
                                  where
                                   ((string.IsNullOrEmpty(q)) ? true : _requerimientos.Requerimiento.Contains(q))
                                  select new Cls_Select2
                                  {
                                      id = _requerimientos.Norma_ID.ToString(),
                                      text = _requerimientos.Requerimiento,

                                  });
                    //recorre la lista de resultados
                    foreach (var p in Requerimientos)
                        Lst_Combo.Add(p);

                    Json_Resultado = JsonMapper.ToJson(Lst_Combo);
                }
            }
            //mensaje de error
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            finally
            {
                Context.Response.Write(Json_Resultado);
            }
        }
    }
}
