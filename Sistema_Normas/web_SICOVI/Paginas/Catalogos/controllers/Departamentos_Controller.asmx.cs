﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using datos_cambios_procesos;
using LitJson;
using System.Web.Script.Services;

using web_cambios_procesos.Models.Negocio;
using web_cambios_procesos.Models.Ayudante;
using System.Collections.Specialized;

namespace web_cambios_procesos.Paginas.Catalogos.controllers
{
    /// <summary>
    /// Summary description for Departamentos_Controller
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class Departamentos_Controller : System.Web.Services.WebService
    {
        #region (Métodos)
        /// <summary>
        /// Método que realiza el alta del departamento.
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Alta(string jsonObject)
        {
            Cls_Cat_Departamentos_Negocio Obj_Departamentos = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Alta registro";
                Obj_Departamentos = JsonMapper.ToObject<Cls_Cat_Departamentos_Negocio>(jsonObject);

                using (var dbContext = new Sistema_NormasContainer())
                {
                    var _departamentos = new Cat_Departamentos();
                    _departamentos.Empresa_ID = Convert.ToInt32(Cls_Sesiones.Empresa_ID);
                    _departamentos.Estatus_ID = Obj_Departamentos.Estatus_ID;
                    _departamentos.Nombre = Obj_Departamentos.Nombre;
                    _departamentos.Observaciones = Obj_Departamentos.Observaciones;
                    _departamentos.Usuario_Creo = Cls_Sesiones.Usuario;
                    _departamentos.Fecha_Creo = new DateTime?(DateTime.Now).Value;

                    dbContext.Cat_Departamentos.Add(_departamentos);
                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "La operación se completó sin problemas.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Informe Técnico";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.Message.Contains("Los datos de cadena o binarios se truncarían"))
                    Mensaje.Mensaje =
                        "Alguno de los campos que intenta insertar tiene un tamaño mayor al establecido en la base de datos. <br /><br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Los datos de cadena o binarios se truncarían";
                else if (Ex.InnerException.InnerException.Message.Contains("Cannot insert duplicate key row in object"))
                    Mensaje.Mensaje =
                        "Existen campos definidos como claves que no pueden duplicarse. <br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Por favor revisar que no esté ingresando datos duplicados.";
                else
                    Mensaje.Mensaje = "Informe técnico: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que realiza la actualización de los datos del departamento seleccionado.
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Actualizar(string jsonObject)
        {
            Cls_Cat_Departamentos_Negocio Obj_Departamentos = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Actualizar registro";
                Obj_Departamentos = JsonMapper.ToObject<Cls_Cat_Departamentos_Negocio>(jsonObject);

                using (var dbContext = new Sistema_NormasContainer())
                {
                    var _departamentos = dbContext.Cat_Departamentos.Where(u => u.Departamento_ID == Obj_Departamentos.Departamento_ID).First();

                    _departamentos.Estatus_ID = Obj_Departamentos.Estatus_ID;
                    _departamentos.Nombre = Obj_Departamentos.Nombre;
                    _departamentos.Observaciones = Obj_Departamentos.Observaciones;
                    _departamentos.Usuario_Modifico = Cls_Sesiones.Usuario;
                    _departamentos.Fecha_Modifico = new DateTime?(DateTime.Now);

                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "La operación se completó sin problemas.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Informe Técnico";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.Message.Contains("Los datos de cadena o binarios se truncarían"))
                    Mensaje.Mensaje =
                        "Alguno de los campos que intenta insertar tiene un tamaño mayor al establecido en la base de datos. <br /><br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Los datos de cadena o binarios se truncarían";
                else if (Ex.InnerException.InnerException.Message.Contains("Cannot insert duplicate key row in object"))
                    Mensaje.Mensaje =
                        "Existen campos definidos como claves que no pueden duplicarse. <br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Por favor revisar que no esté ingresando datos duplicados.";
                else
                    Mensaje.Mensaje = "Informe técnico: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que elimina el registro seleccionado.
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Eliminar(string jsonObject)
        {
            Cls_Cat_Departamentos_Negocio Obj_Departamentos = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Eliminar registro";
                Obj_Departamentos = JsonMapper.ToObject<Cls_Cat_Departamentos_Negocio>(jsonObject);

                using (var dbContext = new Sistema_NormasContainer())
                {
                    var _departamentos = dbContext.Cat_Departamentos.Where(u => u.Departamento_ID == Obj_Departamentos.Departamento_ID).First();
                    dbContext.Cat_Departamentos.Remove(_departamentos);
                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "La operación se completó sin problemas.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Informe Técnico";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.InnerException.Message.Contains("The DELETE statement conflicted with the REFERENCE constraint"))
                {
                    try
                    {
                        using (var dbContext = new Sistema_NormasContainer())
                        {
                            var _departamento = dbContext.Cat_Departamentos.Where(u => u.Departamento_ID == Obj_Departamentos.Departamento_ID).First();
                            var _estatus = dbContext.Apl_Estatus.Where(u => u.Estatus == "ELIMINADO").First();

                            _departamento.Estatus_ID = _estatus.Estatus_ID;
                            _departamento.Usuario_Modifico = Cls_Sesiones.Usuario;
                            _departamento.Fecha_Modifico = new DateTime?(DateTime.Now);

                            dbContext.SaveChanges();
                            Mensaje.Estatus = "success";
                            Mensaje.Mensaje = "La operación se completó sin problemas.";
                        }
                    }
                    catch { }
                }
                //Mensaje.Mensaje =
                //        "La operación de eliminar el registro fue revocada. <br /><br />" +
                //        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; El registro que intenta eliminar ya se encuentra en uso.";
                else
                    Mensaje.Mensaje = "Informe técnico: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que realiza la búsqueda del departamento.
        /// <returns>Listado de los departamentos filtrados por nombre</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Departamentos_Por_Nombre(string jsonObject)
        {
            Cls_Cat_Departamentos_Negocio Obj_Departamentos = null;
            string Json_Resultado = string.Empty;
            List<Cls_Cat_Departamentos_Negocio> Lista_Departamentos = new List<Cls_Cat_Departamentos_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Validaciones";
                Obj_Departamentos = JsonMapper.ToObject<Cls_Cat_Departamentos_Negocio>(jsonObject);

                using (var dbContext = new Sistema_NormasContainer())
                {
                    var departamentos = (from _departamentos in dbContext.Cat_Departamentos
                                  join _estatus in dbContext.Apl_Estatus on _departamentos.Estatus_ID equals _estatus.Estatus_ID

                                  where _departamentos.Departamento_ID.Equals(Obj_Departamentos.Departamento_ID) ||
                                  _departamentos.Nombre.Equals(Obj_Departamentos.Nombre)

                                  select new Cls_Cat_Departamentos_Negocio
                                  {
                                      Departamento_ID = _departamentos.Departamento_ID,
                                      Nombre = _departamentos.Nombre,
                                      Estatus = _estatus.Estatus
                                  }).OrderByDescending(u => u.Nombre);

                    if (departamentos.Any())
                    {
                        if (Obj_Departamentos.Departamento_ID == 0)
                        {
                            Mensaje.Estatus = "error";
                            if (!string.IsNullOrEmpty(Obj_Departamentos.Nombre))
                                Mensaje.Mensaje = "El nombre ingresado ya se encuentra registrado.";
                        }
                        else
                        {
                            var item_edit = departamentos.Where(u => u.Departamento_ID == Obj_Departamentos.Departamento_ID);

                            if (item_edit.Count() == 1)
                                Mensaje.Estatus = "success";
                            else
                            {
                                Mensaje.Estatus = "error";
                                if (!string.IsNullOrEmpty(Obj_Departamentos.Nombre))
                                    Mensaje.Mensaje = "El nombre ingresado ya se encuentra registrado.";
                            }
                        }
                    }
                    else
                        Mensaje.Estatus = "success";

                    Json_Resultado = JsonMapper.ToJson(Mensaje);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que realiza la búsqueda del departamento.
        /// </summary>
        /// <returns>Listado serializado con los departamentos según los filtros aplícados</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Departamentos_Por_Filtros(string jsonObject)
        {
            Cls_Cat_Departamentos_Negocio Obj_Departamentos = null;
            string Json_Resultado = string.Empty;
            List<Cls_Cat_Departamentos_Negocio> Lista_Departamentos = new List<Cls_Cat_Departamentos_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Obj_Departamentos = JsonMapper.ToObject<Cls_Cat_Departamentos_Negocio>(jsonObject);

                using (var dbContext = new Sistema_NormasContainer())
                {
                    int empresa_id = string.IsNullOrEmpty(Cls_Sesiones.Empresa_ID) ? -1 : Convert.ToInt32(Cls_Sesiones.Empresa_ID);

                    var Departamentos = (from _departamentos in dbContext.Cat_Departamentos
                                 join _estatus in dbContext.Apl_Estatus on _departamentos.Estatus_ID equals _estatus.Estatus_ID

                                 where _departamentos.Empresa_ID.Equals(empresa_id) &&
                                 _estatus.Estatus != "ELIMINADO" &&
                                 (!string.IsNullOrEmpty(Obj_Departamentos.Nombre) ? _departamentos.Nombre.ToLower().Contains(Obj_Departamentos.Nombre.ToLower()) : true)

                                 select new Cls_Cat_Departamentos_Negocio
                                 {
                                     Departamento_ID = _departamentos.Departamento_ID,
                                     Nombre = _departamentos.Nombre,
                                     Estatus = _estatus.Estatus,
                                     Estatus_ID = _departamentos.Estatus_ID,
                                     Empresa_ID = _departamentos.Empresa_ID,
                                     Observaciones = _departamentos.Observaciones,
                                 }).OrderByDescending(u => u.Nombre);



                    foreach (var p in Departamentos)
                        Lista_Departamentos.Add((Cls_Cat_Departamentos_Negocio)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Departamentos);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método para consultar los estatus.
        /// </summary>
        /// <returns>Listado serializado de los estatus</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Estatus()
        {
            string Json_Resultado = string.Empty;
            List<Apl_Estatus> Lista_estatus = new List<Apl_Estatus>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {

                using (var dbContext = new Sistema_NormasContainer())
                {
                    var Estatus = from _empresas in dbContext.Apl_Estatus
                                  select new { _empresas.Estatus, _empresas.Estatus_ID };


                    Json_Resultado = JsonMapper.ToJson(Estatus.ToList());


                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }


        #endregion
    }
}
