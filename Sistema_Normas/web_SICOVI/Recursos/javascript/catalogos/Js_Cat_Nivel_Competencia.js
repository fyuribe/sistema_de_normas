﻿var closeModal = true; // cerrar modal 

$(document).on('ready', function () {
    _inicializar_pagina();
});
/* =============================================
--NOMBRE_FUNCIÓN: _inicializar_pagina
--DESCRIPCIÓN: funcion para iniciar a pagina 
--PARÁMETROS: NA
--CREO: Flor Yanet Uribe Ortega
--FECHA_CREO: 03 de septiembre de 2019
--MODIFICÓ:
--FECHA_MODIFICÓ:
--CAUSA_MODIFICACIÓN:
=============================================*/
function _inicializar_pagina() {
    try {
        _habilitar_controles('Inicio');
        _limpiar_controles();
        _cargar_tabla();
        _search();
        _modal();
        //_eventos_textbox();
        _eventos();
        _enter_keypress_modal();
        _set_location_toolbar();
        _cargar_estatus();
        _load_niveles();
    } catch (e) {
        _mostrar_mensaje('Technical report', e);
    }
}
/* =============================================
--NOMBRE_FUNCIÓN: _estado_inicial
--DESCRIPCIÓN: estado inicial de la pantalla
--PARÁMETROS: NA
--CREO: Flor Yanet Uribe Ortega
--FECHA_CREO: 03 de septiembre de 2019
--MODIFICÓ:
--FECHA_MODIFICÓ:
--CAUSA_MODIFICACIÓN:
=============================================*/
function _estado_inicial() {
    try {
        _habilitar_controles('Inicio');
        _limpiar_controles();
        _set_location_toolbar();
    } catch (e) {
        _mostrar_mensaje('Technical report', e);
    }
}

/* =============================================
--NOMBRE_FUNCIÓN: _habilitar_controles
--DESCRIPCIÓN: funcion para habilitar los controles 
--PARÁMETROS: opcion -- vista de la pantalla
--CREO: Flor Yanet Uribe Ortega
--FECHA_CREO: 03 de septiembre de 2019
--MODIFICÓ:
--FECHA_MODIFICÓ:
--CAUSA_MODIFICACIÓN:
=============================================*/
function _habilitar_controles(opcion) {
    //variable que regresa la funcion
    var Estatus = false;

    switch (opcion) {
        case "Nuevo":
            Estatus = true;
            //   $('#cmb_estatus').attr({ disabled: Estatus });
            break;
        case "Modificar":
            Estatus = true;
            $('#cmb_estatus').attr({ disabled: !Estatus });
            break;
        case "Inicio":
            break;
    }
    $('#txt_nombre').attr({ disabled: !Estatus });
    $('#txt_observaciones').attr({ disabled: !Estatus });
}

/* =============================================
--NOMBRE_FUNCIÓN: _eventos
--DESCRIPCIÓN: eventos de los controles de la pantalla
--PARÁMETROS: NA
--CREO: Flor Yanet Uribe Ortega
--FECHA_CREO: 03 de septiembre de 2019
--MODIFICÓ:
--FECHA_MODIFICÓ:
--CAUSA_MODIFICACIÓN:
=============================================*/
function _eventos() {
    try {
        $('#modal_datos').on('hidden.bs.modal', function () {
            //cerrar el modal
            if (!closeModal)
                $(this).modal('show');
        });
        $('#btn_nuevo').click(function (e) {
            _limpiar_controles();
            _habilitar_controles('Nuevo');
            _launch_modal('<i class="fa fa-floppy-o" style="font-size: 25px;"></i>&nbsp;&nbsp;Registrar Nivel Competencia');
            $('#cmb_estatus').attr('disabled', true);
            $("#cmb_estatus").select2("trigger", "select", {
                data: {
                    id: 'ACTIVO',
                    text: 'ACTIVO',
                }
            });
        });

        $('#btn_limpiar').click(function (e) {
            $("#cmb_busqueda_por_nombre").select2("trigger", "select", {
                data: {
                    id: '',
                    text: '',
                }
            });
        });

        $('.cancelar').each(function (index, element) {
            $(this).on('click', function (e) {
                e.preventDefault();
                _estado_inicial();
            });
        });
        $('#btn_salir').on('click', function (e) { e.preventDefault(); window.location.href = '../Paginas_Generales/Frm_Apl_Principal.aspx'; });
        $('#btn_busqueda').on('click', function (e) {
            e.preventDefault();
            _search();
        });
        $('#modal_datos input[type=text]').each(function (index, element) {
            $(this).on('focus', function () {
                _remove_class_error('#' + $(this).attr('id'));
            });
        });
    } catch (e) {
        _mostrar_mensaje('Technical report', e);
    }
}
/* =============================================
--NOMBRE_FUNCIÓN: _limpiar_controles
--DESCRIPCIÓN: limpiar los controles de la pantalla
--PARÁMETROS: NA
--CREO: Flor Yanet Uribe Ortega
--FECHA_CREO: 03 de septiembre de 2019
--MODIFICÓ:
--FECHA_MODIFICÓ:
--CAUSA_MODIFICACIÓN:
=============================================*/
function _limpiar_controles() {
    $('input[type=text]').each(function () { $(this).val(''); });
    $('#cmb_estatus').val('');
    $('textarea').each(function () { $(this).val(''); });
    $('#txt_nivel_id').val('');
    _validation_sumary(null);
    _clear_all_class_error();
}

/* =============================================
--NOMBRE_FUNCIÓN: _cargar_tabla
--DESCRIPCIÓN: crear columnas del listado de la pantalla
--PARÁMETROS: NA
--CREO: Flor Yanet Uribe Ortega
--FECHA_CREO: 03 de septiembre de 2019
--MODIFICÓ:
--FECHA_MODIFICÓ:
--CAUSA_MODIFICACIÓN:
=============================================*/
function _cargar_tabla() {

    try {
        $('#tbl_nivel_competencia').bootstrapTable('destroy');
        $('#tbl_nivel_competencia').bootstrapTable({
            cache: false,
            width: 900,
            height: 400,
            striped: true,
            pagination: true,
            pageSize: 10,
            pageList: [10, 25, 50, 100, 200],
            smartDysplay: false,
            search: true,
            showColumns: false,
            showRefresh: false,
            minimumCountColumns: 2,
            clickToSelect: true,
            columns: [
                { field: 'Nivel_Competencia_ID', title: '', width: 0, align: 'center', valign: 'bottom', sortable: true, visible: false },
                { field: 'Nombre', title: 'Nombre', width: 200, align: 'left', valign: 'bottom', sortable: true, clickToSelect: false },
                { field: 'Estatus', title: 'Estatus', width: 100, align: 'left', valign: 'bottom', sortable: true, clickToSelect: false },
                { field: 'Descripcion', title: 'Descripción', width: 200, align: 'left', valign: 'bottom', sortable: true, clickToSelect: false },

                {
                    field: 'Nivel_Competencia_ID',
                    title: '',
                    align: 'center',
                    valign: 'bottom',
                    width: 60,
                    clickToSelect: false,
                    formatter: function (value, row) {
                        return '<div> ' +
                            '<a class="remove ml10 edit" id="' + row.Nivel_Competencia_ID + '" href="javascript:void(0)" data-nivel=\'' + JSON.stringify(row) + '\' onclick="btn_editar_click(this);" title="Edit"><i class="glyphicon glyphicon-edit"></i></button>' +
                            '&nbsp;&nbsp;<a class="remove ml10 delete" id="' + row.Nivel_Competencia_ID + '" href="javascript:void(0)" data-nivel=\'' + JSON.stringify(row) + '\' onclick="btn_eliminar_click(this);" title="Remove"><i class="glyphicon glyphicon-trash"></i></a>' +
                            '</div>';
                    }
                }
            ]
        });
    } catch (e) {
        _mostrar_mensaje('Technical report', e);
    }
}

/* =============================================
--NOMBRE_FUNCIÓN: _search
--DESCRIPCIÓN: consultar los registros del catalogo y mostrarlos en la lista
--PARÁMETROS: NA
--CREO: Flor Yanet Uribe Ortega
--FECHA_CREO: 03 de septiembre de 2019
--MODIFICÓ:
--FECHA_MODIFICÓ:
--CAUSA_MODIFICACIÓN:
=============================================*/
function _search() {
    //datos que se enviaran a la consulta 
    var filtros = null;
    try {
        show_loading_bar({
            pct: 78,
            wait: .5,
            delay: .5,
            finish: function (pct) {
                filtros = new Object();

                filtros.Nivel_Competencia_ID = $('#cmb_busqueda_por_nombre').val() === '' || $('#cmb_busqueda_por_nombre').val() === null ? 0 : parseInt($('#cmb_busqueda_por_nombre').val());
                //convertir los datos para enviarlos al metodo
                var $data = JSON.stringify({ 'jsonObject': JSON.stringify(filtros) });

                jQuery.ajax({
                    type: 'POST',
                    url: 'controllers/Nivel_Competencia_Controller.asmx/Consultar_Nivel_Competencia_Por_Filtros',
                    data: $data,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true,
                    cache: false,
                    success: function (datos) {
                        //valida si datos no es null
                        if (datos !== null) {
                            $('#tbl_nivel_competencia').bootstrapTable('load', JSON.parse(datos.d));
                            hide_loading_bar();
                        }
                    }
                });
            }
        });
    } catch (e) {

    }
}

/* =============================================
--NOMBRE_FUNCIÓN: _modal
--DESCRIPCIÓN: Crear el cuerpo del modal 
--PARÁMETROS: NA
--CREO: Flor Yanet Uribe Ortega
--FECHA_CREO: 03 de septiembre de 2019
--MODIFICÓ:
--FECHA_MODIFICÓ:
--CAUSA_MODIFICACIÓN:
=============================================*/
function _modal() {
    //construir en cuerpo del modal
    var tags = '';
    try {
        tags += '<div class="modal fade" id="modal_datos" name="modal_datos" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">';
        tags += '<div class="modal-dialog">';
        tags += '<div class="modal-content">';

        tags += '<div class="modal-header">';
        tags += '<button type="button" class="close cancelar" data-dismiss="modal" aria-label="Close" onclick="_set_close_modal(true);"><i class="fa fa-times"></i></button>';
        tags += '<h4 class="modal-title" id="myModalLabel">';
        tags += '<label id="lbl_titulo"></label>';
        tags += '</h4>';
        tags += '</div>';

        tags += '<div class="modal-body">';

        tags += '<div class="row">' +
            ' <div class="col-md-6">' +
            '            <label class="fuente_lbl_controles">(*) Estatus</label>' +
            '        <select id="cmb_estatus" name="cmb_estatus" class="form-control input-sm"  data-parsley-required="true" required  style="width:100%" ></select> ' +
            '        <input type="hidden" id="txt_nivel_id"/>' +
            '       </div> ' +

            ' <div class="col-md-6">' +
            '            <label class="fuente_lbl_controles">(*) Nombre</label>' +
            '        <input type="text" id="txt_nombre" name="txt_nombre" class="form-control input-sm" disabled="disabled" placeholder="(*) Nombre" data-parsley-required="true" maxlength="100" required /> ' +
            '    </div>' +
            '</div>' +

            '<div class="row">' +
            ' <div class="col-md-12">' +
            '            <label class="fuente_lbl_controles">Descripción</label>' +
            '        <textarea  id="txt_descripcion" name="txt_observaciones" class="form-control input-sm" placeholder="Descripción"  style="min-height: 50px !important;" data-parsley-required="false" maxlength="250"> </textarea>' +
            '    </div>' +
            '</div>';

        tags += '</div>';

        tags += '<div class="modal-footer">';
        tags += '<div class="row">';

        tags += '<div class="col-md-7">';
        tags += '<div id="sumary_error" class="alert alert-danger text-left" style="width: 277.78px !important; display:none;">';
        tags += '<label id="lbl_msg_error"/>';
        tags += '</div>';
        tags += '</div>';

        tags += '<div class="col-md-5">';
        tags += '<div class="form-inline">';
        tags += '<button type="submit" class="btn btn-info btn-icon btn-icon-standalone btn-xs" id="btn_guardar_datos" title="Guardad"><i class="fa fa-check"></i><span>Guardar</span></button>';
        tags += '<button type="button" class="btn btn-danger btn-icon btn-icon-standalone btn-xs cancelar" data-dismiss="modal" id="btn_cancelar" aria-label="Cancelar" onclick="_set_close_modal(true);" title="Cancelar"><i class="fa fa-remove"></i><span>Cancelar</span></button>';
        tags += '</div>';
        tags += '</div>';

        tags += '</div>';
        tags += '</div>';

        tags += '</div>';
        tags += '</div>';
        tags += '</div>';

        $(tags).appendTo('body');

        $('#btn_guardar_datos').bind('click', function (e) {
            e.preventDefault();
            //validar el id del registro 
            if ($('#txt_nivel_id').val() != null && $('#txt_nivel_id').val() != undefined && $('#txt_nivel_id').val() != '') {
                //resultado de la funcion
                var _output = _validation('editar');
                //validar el estatus que regresa la funcion 
                if (_output.Estatus) {
                    //ejecutar la funcion actualizar
                    if (_actualizar()) {
                        _estado_inicial();
                        _set_close_modal(true);
                        jQuery('#modal_datos').modal('hide');
                    }
                }
                    //cerrar modal
                else {
                    _set_close_modal(false);
                }
            }
                //valida si se dara de alta un nuevo registro 
            else {
                //resultado de la funcion
                var _output = _validation('alta');
                //validar el estatus que regresa la funcion
                if (_output.Estatus) {
                    //ejecutar la funcion alta
                    if (_alta()) {
                        _estado_inicial();
                        _set_close_modal(true);
                        jQuery('#modal_datos').modal('hide');
                    }
                }
                    //cerrar el modal 
                else {
                    _set_close_modal(false);
                }
            }
        });
    } catch (e) {
        _mostrar_mensaje('Technical report', e);
    }
}
/* =============================================
--NOMBRE_FUNCIÓN: _set_title_modal
--DESCRIPCIÓN: Colocar el titulo en el modal
--PARÁMETROS:Titulo --Titulo del modal
--CREO: Flor Yanet Uribe Ortega
--FECHA_CREO: 03 de septiembre de 2019
--MODIFICÓ:
--FECHA_MODIFICÓ:
--CAUSA_MODIFICACIÓN:
=============================================*/
function _set_title_modal(Titulo) {
    $("#lbl_titulo").html(Titulo);
}
/* =============================================
--NOMBRE_FUNCIÓN: _launch_modal
--DESCRIPCIÓN: Mostrar el modal 
--PARÁMETROS:title_window --titulo a mostrar
--CREO: Flor Yanet Uribe Ortega
--FECHA_CREO: 03 de septiembre de 2019
--MODIFICÓ:
--FECHA_MODIFICÓ:
--CAUSA_MODIFICACIÓN:
=============================================*/
function _launch_modal(title_window) {
    _set_title_modal(title_window);
    jQuery('#modal_datos').modal('show', { backdrop: 'static', keyboard: false });
    $('#txt_nombre').focus();

}
/* =============================================
--NOMBRE_FUNCIÓN: _set_close_modal
--DESCRIPCIÓN: Cerrar el modal  
--PARÁMETROS:state -- estatus del modal
--CREO: Flor Yanet Uribe Ortega
--FECHA_CREO: 03 de septiembre de 2019
--MODIFICÓ:
--FECHA_MODIFICÓ:
--CAUSA_MODIFICACIÓN:
=============================================*/
function _set_close_modal(state) {
    closeModal = state;
}
/* =============================================
--NOMBRE_FUNCIÓN: _eventos_textbox
--DESCRIPCIÓN: eventos de las cajas de texto
--PARÁMETROS:NA
--CREO: Flor Yanet Uribe Ortega
--FECHA_CREO: 03 de septiembre de 2019
--MODIFICÓ:
--FECHA_MODIFICÓ:
--CAUSA_MODIFICACIÓN:
=============================================*/
function _eventos_textbox() {
    $('#txt_nombre').on('blur', function () {
        $(this).val($(this).val().match(/^[0-9a-zA-Z\u0020]+$/) ? $(this).val() : $(this).val().replace(/\W+/g, ''));
    });

    $('#txt_descripcion').on('blur', function () {
        $(this).val($(this).val().match(/^[0-9a-zA-Z\u0020]+$/) ? $(this).val() : $(this).val().replace(/\W+/g, ''));
    });
}
/* =============================================
--NOMBRE_FUNCIÓN: _validation_sumary
--DESCRIPCIÓN: validar los datos requeridos
--PARÁMETROS:validation-- accion a realizar, alta o editar
--CREO: Flor Yanet Uribe Ortega
--FECHA_CREO: 03 de septiembre de 2019
--MODIFICÓ:
--FECHA_MODIFICÓ:
--CAUSA_MODIFICACIÓN:
=============================================*/
function _validation_sumary(validation) {
    var header_message = '<i class="fa fa-exclamation-triangle fa-2x"></i><span>Observaciones</span><br />';
    //validar si es igual a null
    if (validation == null) {
        $('#lbl_msg_error').html('');
        $('#sumary_error').css('display', 'none');
    }
        //mostrar mensaje de error en los controles
    else {
        $('#lbl_msg_error').html(header_message + validation.Mensaje);
        $('#sumary_error').css('display', 'block');
    }
}
/* =============================================
--NOMBRE_FUNCIÓN: _validation
--DESCRIPCIÓN: validar los datos requeridos
--PARÁMETROS:opcion-- accion a realizar, alta o editar
--CREO: Flor Yanet Uribe Ortega
--FECHA_CREO: 03 de septiembre de 2019
--MODIFICÓ:
--FECHA_MODIFICÓ:
--CAUSA_MODIFICACIÓN:
=============================================*/
function _validation(opcion) {
    //variable que regresa la funcion
    var _output = new Object();

    _output.Estatus = true;
    _output.Mensaje = '';
    //validar si el control esta vacio
    if (!$('#txt_nombre').parsley().isValid()) {
        _add_class_error('#txt_nombre');
        _output.Estatus = false;
        _output.Mensaje += '&nbsp;<i class="fa fa-angle-double-right"></i>&nbsp;El nombre es un dato requerido.<br />';
    }
        //validar si se registra un nuevo elemento 
    else {
        //opcion de alta
        var _Resultado = (opcion === 'alta') ?
            _validate_fields($('#txt_nombre').val(), null, 'nombre') :
            _validate_fields($('#txt_nombre').val(), $('#txt_nivel_id').val(), 'nombre');
        //validar el estatu que regresa la funcion
        if (_Resultado.Estatus === 'error') {
            _add_class_error('#txt_nombre');
            _output.Estatus = false;
            _output.Mensaje += '&nbsp;<i class="fa fa-angle-double-right"></i>&nbsp;' + _Resultado.Mensaje + '<br />';
        }
    }
    //validar si el control esta vacio
    if (!$('#cmb_estatus').parsley().isValid()) {
        _add_class_error('#cmb_estatus');
        _output.Estatus = false;
        _output.Mensaje += '&nbsp;<i class="fa fa-angle-double-right"></i>&nbsp;El estatus es un dato requerido.<br />';
    }
    //regresar el resultado
    if (!_output.Estatus) _validation_sumary(_output);

    return _output;
}

/* =============================================
--NOMBRE_FUNCIÓN: _validate_fields
--DESCRIPCIÓN: validar que no se repita el mismo nombre
--PARÁMETROS:
value -- valor a buscar
id -- id del registro,
field -- campo a validar
--CREO: Flor Yanet Uribe Ortega
--FECHA_CREO: 03 de septiembre de 2019
--MODIFICÓ:
--FECHA_MODIFICÓ:
--CAUSA_MODIFICACIÓN:
=============================================*/
function _validate_fields(value, id, field) {
    var Nivel = null;//datos que se enviaran a la consulta
    var Resultado = null;//resultado que regresa la funcion

    try {
        Nivel = new Object();
        //valida que id sea dirente de null
        if (id !== null)
            Nivel.Nivel_Competencia_ID = parseInt(id);

        switch (field) {
            case 'nombre':
                Nivel.Nombre = value;
                break;
            case 'clave':
                Nivel.Clave = value;
                break;
            default:
        }
        //convertir los datos para enviarlos al metodo
        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(Nivel) });
        $.ajax({
            type: 'POST',
            url: 'controllers/Nivel_Competencia_Controller.asmx/Consultar_Nivel_Competencia_Por_Nombre',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function (result) {
                //valida que result sea diferente a null 
                if (result !== null)
                    Resultado = JSON.parse(result.d);
            }
        });
    } catch (e) {
        Resultado = new Object();
        Resultado.Estatus = 'error';
        Resultado.Mensaje = 'No fue posible realizar la validación del ' + field + ' en la base de datos.';
        _mostrar_mensaje('Informe Técnico', e);
    }
    return Resultado;
}
/* =============================================
--NOMBRE_FUNCIÓN: _add_class_error
--DESCRIPCIÓN: mostrar el mensaje en los controles
--PARÁMETROS:selector-- accion a realizar, alta o editar
--CREO: Flor Yanet Uribe Ortega
--FECHA_CREO: 03 de septiembre de 2019
--MODIFICÓ:
--FECHA_MODIFICÓ:
--CAUSA_MODIFICACIÓN:
=============================================*/
function _add_class_error(selector) {
    $(selector).addClass('alert-danger');
}
/* =============================================
--NOMBRE_FUNCIÓN: _remove_class_error
--DESCRIPCIÓN: quitar el mensaje en los controles
--PARÁMETROS:selector-- accion a realizar, alta o editar
--CREO: Flor Yanet Uribe Ortega
--FECHA_CREO: 03 de septiembre de 2019
--MODIFICÓ:
--FECHA_MODIFICÓ:
--CAUSA_MODIFICACIÓN:
=============================================*/
function _remove_class_error(selector) {
    $(selector).removeClass('alert-danger');
}
/* =============================================
--NOMBRE_FUNCIÓN: _clear_all_class_error
--DESCRIPCIÓN: limpiar mensajes en los controles
--PARÁMETROS:NA
--CREO: Flor Yanet Uribe Ortega
--FECHA_CREO: 03 de septiembre de 2019
--MODIFICÓ:
--FECHA_MODIFICÓ:
--CAUSA_MODIFICACIÓN:
=============================================*/
function _clear_all_class_error() {
    $('#modal_datos input[type=text]').each(function (index, element) {
        _remove_class_error('#' + $(this).attr('id'));
    });
    $('#modal_datos select').each(function () {
        _remove_class_error('#' + $(this).attr('id'));
    });
}

/* =============================================
--NOMBRE_FUNCIÓN: _enter_keypress_modal
--DESCRIPCIÓN: guardar datos al dar enter
--PARÁMETROS:NA
--CREO: Flor Yanet Uribe Ortega
--FECHA_CREO: 03 de septiembre de 2019
--MODIFICÓ:
--FECHA_MODIFICÓ:
--CAUSA_MODIFICACIÓN:
=============================================*/
function _enter_keypress_modal() {
    var $btn = $('[id$=btn_guardar_datos]').get(0);
    $(window).keypress(function (e) {
        //validar datos en la caja de texto 
        if (e.which === 13 && e.target.type !== 'textarea') {
            //validar el valor 
            if ($btn != undefined && $btn != null) {
                if ($btn.type === 'submit')
                    $btn.click();
                    //mostrar el valor
                else
                    eval($btn.href);
                return false;
            }
        }
    });
}

/* =============================================
--NOMBRE_FUNCIÓN: _set_location_toolbar
--DESCRIPCIÓN: alinear los botones inicio y nuevo
--PARÁMETROS:NA
--CREO: Flor Yanet Uribe Ortega
--FECHA_CREO: 03 de septiembre de 2019
--MODIFICÓ:
--FECHA_MODIFICÓ:
--CAUSA_MODIFICACIÓN:
=============================================*/
function _set_location_toolbar() {
    $('#toolbar').parent().removeClass("pull-left");
    $('#toolbar').parent().addClass("pull-right");
}

/* =============================================
--NOMBRE_FUNCIÓN: _load_niveles
--DESCRIPCIÓN: cargar los registros del catalogo
--PARÁMETROS:NA
--CREO: Flor Yanet Uribe Ortega
--FECHA_CREO: 03 de septiembre de 2019
--MODIFICÓ:
--FECHA_MODIFICÓ:
--CAUSA_MODIFICACIÓN:
=============================================*/
function _load_niveles() {
    try {
        $('#cmb_busqueda_por_nombre').select2({
            language: "es",
            theme: "classic",
            placeholder: 'SELECCIONE',
            allowClear: true,
            ajax: {
                url: 'controllers/Nivel_Competencia_Controller.asmx/Consultar_Niveles',
                cache: true,
                dataType: 'json',
                type: "POST",
                delay: 250,
                params: {
                    contentType: 'application/json; charset=utf-8'
                },
                quietMillis: 100,
                results: function (data) {
                    return { results: data };
                },
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page,
                    };
                },
                processResults: function (data, page) {
                    return {
                        results: data
                    };
                },
            }
        });

    } catch (e) {
        _mostrar_mensaje('Informe técnico', e);
    }

}
/* =============================================
--NOMBRE_FUNCIÓN: _cargar_estatus
--DESCRIPCIÓN: cargar estatus en el control
--PARÁMETROS:NA
--CREO: Flor Yanet Uribe Ortega
--FECHA_CREO: 03 de septiembre de 2019
--MODIFICÓ:
--FECHA_MODIFICÓ:
--CAUSA_MODIFICACIÓN:
=============================================*/
function _cargar_estatus() {
    try {
        $('#cmb_estatus').select2({
            language: "es",
            theme: "classic",
            placeholder: 'SELECCIONE',
            allowClear: true,
            data: [
                {
                    id: 'ACTIVO',
                    text: 'ACTIVO',
                }, {
                    id: 'INACTIVO',
                    text: 'INACTIVO',
                }],
        });

    } catch (e) {
        _mostrar_mensaje('Informe técnico', e);
    }
}
/* =============================================
METODOS
=============================================*/
/* =============================================
--NOMBRE_FUNCIÓN: _alta
--DESCRIPCIÓN: registrar datos a la base de datos
--PARÁMETROS: NA
--CREO: Flor Yanet Uribe Ortega
--FECHA_CREO: 03 de septiembre de 2019
--MODIFICÓ:
--FECHA_MODIFICÓ:
--CAUSA_MODIFICACIÓN:
=============================================*/
function _alta() {
    var Nivel = null;// datos que se enviaran al metodo
    var isComplete = false;//variable que regresa la funcion

    try {

        Nivel = new Object();
        Nivel.Nombre = $('#txt_nombre').val();
        Nivel.Descripcion = $('#txt_descripcion').val();
        Nivel.Estatus = $('#cmb_estatus').val();

        //convertir los datos para enviarlos al metodo
        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(Nivel) });

        $.ajax({
            type: 'POST',
            url: 'controllers/Nivel_Competencia_Controller.asmx/Alta',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function (result) {
                //convertir el resultado del metodo
                var Resultado = JSON.parse(result.d);
                //validar que Resultado se diferente de null, undefined o vacio
                if (Resultado != null && Resultado != undefined && Resultado != '') {
                    //Validar el que el estatus del resultado sea exitoso
                    if (Resultado.Estatus == 'success') {
                        isComplete = true;
                        _mostrar_mensaje('Información', 'El registro se completo correctamente');
                        _search();
                    }
                        //Validar si el resultado es igual a error
                    else if (Resultado.Estatus == 'error') {
                        _validation_sumary(Resultado);
                    }
                }
                    //mostrar resultado
                else {
                    _validation_sumary(Resultado);
                }
            }
        });
    } catch (e) {
        _mostrar_mensaje('Technical report', e);
    }
    return isComplete;
}
/* =============================================
--NOMBRE_FUNCIÓN: _actualizar
--DESCRIPCIÓN: actualizar datos del registro
--PARÁMETROS:NA
--CREO: Flor Yanet Uribe Ortega
--FECHA_CREO: 03 de septiembre de 2019
--MODIFICÓ:
--FECHA_MODIFICÓ:
--CAUSA_MODIFICACIÓN:
=============================================*/
function _actualizar() {
    var Nivel = null;//datos que se enviaran al metodo
    var isComplete = false;//variable que regresa la funcion
    try {
        Nivel = new Object();
        Nivel.Nivel_Competencia_ID = parseInt($('#txt_nivel_id').val());
        Nivel.Nombre = $('#txt_nombre').val();
        Nivel.Descripcion = $('#txt_descripcion').val();
        Nivel.Estatus = $('#cmb_estatus').val();
        //convertir los datos para enviarlos al metodo
        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(Nivel) });

        $.ajax({
            type: 'POST',
            url: 'controllers/Nivel_Competencia_Controller.asmx/Actualizar',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function (result) {
                //convertir el resultado de la consulta
                var Resultado = JSON.parse(result.d);
                //validar que Resultado se diferente de null, undefined o vacio
                if (Resultado != null && Resultado != undefined && Resultado != '') {
                    //Validar el que el estatus del resultado sea exitoso
                    if (Resultado.Estatus == 'success') {
                        _search();
                        isComplete = true;
                    }
                        //Validar si el resultado es igual a error
                    else if (Resultado.Estatus == 'error') {
                        _validation_sumary(Resultado);
                    }
                }
                    //mostrar el resultado 
                else {
                    _validation_sumary(Resultado);
                }
            }
        });
    } catch (e) {
        _mostrar_mensaje('Informe Tecnico', e);
    }
    return isComplete;
}

/* =============================================
--NOMBRE_FUNCIÓN: btn_editar_click
--DESCRIPCIÓN: llenar los controles con los datos del registro
--PARÁMETROS: aspecto -- datos del row 
--CREO: Flor Yanet Uribe Ortega
--FECHA_CREO: 03 de septiembre de 2019
--MODIFICÓ:
--FECHA_MODIFICÓ:
--CAUSA_MODIFICACIÓN:
=============================================*/
function btn_editar_click(aspecto) {
    //datos del registro
    var row = $(aspecto).data('nivel');

    $('#txt_nivel_id').val(row.Nivel_Competencia_ID);
    $('#txt_nombre').val(row.Nombre);
    $("#cmb_estatus").select2("trigger", "select", {
        data: {
            id: row.Estatus,
            text: row.Estatus,
        }
    });
    $('#txt_descripcion').val(row.Descripcion);

    _habilitar_controles('Modificar');
    _launch_modal('<i class="glyphicon glyphicon-edit" style="font-size: 25px;"></i>&nbsp;&nbsp;Actualizar registro');
}

/* =============================================
--NOMBRE_FUNCIÓN: btn_eliminar_click
--DESCRIPCIÓN: mostrar mensaje para eliminar el registro
--PARÁMETROS: nivel -- datos del row 
--CREO: Flor Yanet Uribe Ortega
--FECHA_CREO: 03 de septiembre de 2019
--MODIFICÓ:
--FECHA_MODIFICÓ:
--CAUSA_MODIFICACIÓN:
=============================================*/
function btn_eliminar_click(nivel) {
    //datos del row
    var row = $(nivel).data('nivel');

    bootbox.confirm({
        title: 'Eliminar Registro',
        message: '¿Está seguro de eliminar el registro seleccionado?',
        callback: function (result) {
            //validar el resultado
            if (result) {
                _eliminar(row.Nivel_Competencia_ID);
            }
            _estado_inicial();
        }
    });
}

/* =============================================
--NOMBRE_FUNCIÓN: _eliminar
--DESCRIPCIÓN: eliminar el registro
--PARÁMETROS: id --id del registro
--CREO: Flor Yanet Uribe Ortega
--FECHA_CREO: 03 de septiembre de 2019
--MODIFICÓ:
--FECHA_MODIFICÓ:
--CAUSA_MODIFICACIÓN:
=============================================*/
function _eliminar(id) {
    //datos que se enviaran al metodo
    var Nivel = null;

    try {
        Nivel = new Object();
        Nivel.Nivel_Competencia_ID = parseInt(id);
        //convertir los datos para enviarlos al metodo
        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(Nivel) });

        $.ajax({
            type: 'POST',
            url: 'controllers/Nivel_Competencia_Controller.asmx/Eliminar',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function (result) {
                //convertir el resultado 
                var Resultado = JSON.parse(result.d);
                //validar que Resultado se diferente de null, undefined o vacio
                if (Resultado != null && Resultado != undefined && Resultado != '') {
                    //Validar el que el estatus del resultado sea exitoso
                    if (Resultado.Estatus == 'success') {
                        _search();
                        _mostrar_mensaje('Información', 'El registro se elimino correctamente.')
                    }
                        //Validar si el resultado es igual a error
                    else if (Resultado.Estatus == 'error') {
                        _mostrar_mensaje(Resultado.Titulo, Resultado.Mensaje);
                    }
                }
                    //mostrar mensaje 
                else { _mostrar_mensaje(Resultado.Titulo, Resultado.Mensaje); }
            }
        });

    } catch (e) {
        _mostrar_mensaje('Informe Técnico', e);
    }
}