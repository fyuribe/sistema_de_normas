﻿var closeModal = true;
var estatusActivo = '';
$(document).on('ready', function () {
    _inicializar_pagina();
});
function _inicializar_pagina() {
    try {
        _habilitar_controles('Inicio');
        _limpiar_controles();
        _load_estatus();
        _cargar_tabla();
        _search();
        _modal();
        _eventos_textbox();
        _eventos();
        _enter_keypress_modal();
        _set_location_toolbar();
    } catch (e) {
        _mostrar_mensaje('Informe Técnico', e);
    }
}
function _estado_inicial() {
    try {
        _habilitar_controles('Inicio');
        _limpiar_controles();
        $('#tbl_parametros_movil').bootstrapTable('refresh', 'controllers/Parametros_Movil_Controller.asmx/Consultar_Parametros_Por_Filtros');
        _set_location_toolbar();
    } catch (e) {
        _mostrar_mensaje('Informe Técnico', e);
    }
}
function _habilitar_controles(opcion) {
    var Estatus = false;

    switch (opcion) {
        case "Nuevo":
            Estatus = true;
            $('#cmb_estatus').attr({ disabled: Estatus });
            break;
        case "Modificar":
            Estatus = true;
            $('#cmb_estatus').attr({ disabled: !Estatus });
            break;
        case "Inicio":
            break;
    }
    $('#txt_nombre').attr({ disabled: !Estatus });
    $('#txt_observaciones').attr({ disabled: !Estatus });
}
function _limpiar_controles() {
    $('input[type=text]').each(function () { $(this).val(''); });
    $('select').each(function () { $(this).val(estatusActivo); });
    $('#cmb_estatus').val('');
    $('textarea').each(function () { $(this).val(''); });
    $('#txt_parametro_id').val('');
    _validation_sumary(null);
    _clear_all_class_error();
}
function _eventos() {
    try {
        $('#modal_datos').on('hidden.bs.modal', function () {
            if (!closeModal)
                $(this).modal('show');
        });
        $('#btn_nuevo').click(function (e) {
            _limpiar_controles();
            _habilitar_controles('Nuevo');
            $('#cmb_estatus').val(estatusActivo);
            _launch_modal('<i class="fa fa-floppy-o" style="font-size: 25px;"></i>&nbsp;&nbsp;Alta de registro');
        });
        $('.cancelar').each(function (index, element) {
            $(this).on('click', function (e) {
                e.preventDefault();
                _estado_inicial();
            });
        });
        $('#btn_salir').on('click', function (e) { e.preventDefault(); window.location.href = '../Paginas_Generales/Frm_Apl_Principal.aspx'; });
        $('#btn_busqueda').on('click', function (e) {
            e.preventDefault();
            _search();
        });
        $('#modal_datos input[type=text]').each(function (index, element) {
            $(this).on('focus', function () {
                _remove_class_error('#' + $(this).attr('id'));
            });
        });
    } catch (e) {
        _mostrar_mensaje('Informe Técnico', e);
    }
}
function _eventos_textbox() {
    $('#txt_nombre').on('blur', function () {
        $(this).val($(this).val().match(/^[0-9a-zA-Z\u0020]+$/) ? $(this).val() : $(this).val().replace(/\W+/g, ''));
    });

    $('#txt_observaciones').on('blur', function () {
        $(this).val($(this).val().match(/^[^'#&\\]*$/) ? $(this).val() : $(this).val().replace(/'*#*&*\\*/gi, ''));
    });
}
function _mostrar_mensaje(Titulo, Mensaje) {
    bootbox.dialog({
        message: Mensaje,
        title: Titulo,
        locale: 'es',
        closeButton: true,
        buttons: [{
            label: 'Cerrar',
            className: 'btn-default',
            callback: function () { }
        }]
    });
}
function _cargar_tabla() {

    try {
        $('#tbl_parametros_movil').bootstrapTable('destroy');
        $('#tbl_parametros_movil').bootstrapTable({
            cache: false,
            width: 900,
            height: 400,
            striped: true,
            pagination: true,
            pageSize: 10,
            pageList: [10, 25, 50, 100, 200],
            smartDysplay: false,
            search: true,
            showColumns: false,
            showRefresh: false,
            minimumCountColumns: 2,
            clickToSelect: true,
            columns: [
                { field: 'Parametro_ID', title: '', width: 0, align: 'center', valign: 'bottom', sortable: true, visible: false },
                 {
                     field: 'Estatus', title: 'Estatus', width: '10%', align: 'left', valign: '', halign: 'left', sortable: true, clickToSelect: false, formatter: function (value, row) {

                         var color = '';
                         switch (value) {
                            
                             case 'VALIDADO':
                                 color = "orange";
                                 break;
                             case 'ACTIVO':
                                 color = "green";
                                 break;
                             default:
                                 color = 'rgba(85, 171, 237, 1)';
                         }

                         return '<i class="fa fa-circle" style="color: ' + color + '"></i>&nbsp;' + value;
                     }
                 },
                { field: 'Ruta_Servicios_Web', title: 'Ruta Servicios Web', width: '20%', align: 'left', valign: 'bottom', sortable: true, clickToSelect: false },
                { field: 'Latitud_Ubicacion_Empresa', title: 'Lat. Empresa', width: '8%', align: 'left', valign: 'bottom', sortable: true, clickToSelect: false },
                { field: 'Longitud_Ubicacion_Empresa', title: 'Long. Empresa', width: '8%', align: 'left', valign: 'bottom', sortable: true, clickToSelect: false },
                { field: 'Latitud_Ubicacion_Vigilancia', title: 'Lat. Vigilancia', width: '8%', align: 'left', valign: 'bottom', sortable: true, clickToSelect: false },
                { field: 'Longitud_Ubicacion_Vigilancia', title: 'Long. Vigilancia', width: '8%', align: 'left', valign: 'bottom', sortable: true, clickToSelect: false },
                { field: 'Distancia_Vigilancia', title: 'Dist. Vigilancia', width: '8%', align: 'left', valign: 'bottom', sortable: true, clickToSelect: false },
                { field: 'Tiempo_Peticiones_GPS', title: 'Tiempo PeticionesGPS', width: '8%', align: 'left', valign: 'bottom', sortable: true, clickToSelect: false },
                { field: 'Aplicacion_ID', title: 'Aplicacion_ID', width: '8%', align: 'left', valign: 'bottom', sortable: true, clickToSelect: false },

                {
                    field: 'Area_ID',
                    title: '',
                    align: 'center',
                    valign: 'bottom',
                    width: 60,
                    clickToSelect: false,
                    formatter: function (value, row) {
                        return '<div> ' +
                            '<a class="remove ml10 edit" id="' + row.Parametro_ID + '" href="javascript:void(0)" data-parametros=\'' + JSON.stringify(row) + '\' onclick="btn_editar_click(this);" title="Editar"><i class="glyphicon glyphicon-edit"></i></button>' +
                            '&nbsp;&nbsp;<a class="remove ml10 delete" id="' + row.Parametro_ID + '" href="javascript:void(0)" data-parametros=\'' + JSON.stringify(row) + '\' onclick="btn_eliminar_click(this);" title="Eliminar"><i class="glyphicon glyphicon-trash"></i></a>' +
                            '</div>';
                    }
                }
            ]
        });
    } catch (e) {
        _mostrar_mensaje('Informe Técnico', e);
    }
}
function _alta_parametros() {
    var Parametros = null;
    var isComplete = false;

    try {

        Parametros = new Object();
     
         Parametros.Ruta_Servicios_Web = $('#txt_ruta_servicios').val();
         Parametros.Latitud_Ubicacion_Empresa = parseFloat($('#txt_latitud_empresa').val());
         Parametros.Longitud_Ubicacion_Empresa =parseFloat( $('#txt_longitud_empresa').val());
         Parametros.Latitud_Ubicacion_Vigilancia =parseFloat( $('#txt_latitud_vigilancia').val());
         Parametros.Longitud_Ubicacion_Vigilancia = parseFloat($('#txt_longitud_vigilancia').val());
         Parametros.Distancia_Vigilancia = parseInt($('#txt_distancia_vigilancia').val());
         Parametros.Tiempo_Peticiones_GPS =parseInt($('#txt_tiempo').val());
         Parametros.Aplicacion_ID = $('#txt_aplicacion_id').val();
         Parametros.Remitente_ID = $('#txt_remitente_id').val();
         Parametros.Api_Token = $('#txt_api_token').val();
         Parametros.Perfil_Seguridad = $('#txt_perfil_seguridad').val();
         Parametros.Estatus_ID = parseInt($('#cmb_estatus').val());


        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(Parametros) });

        $.ajax({
            type: 'POST',
            url: 'controllers/Parametros_Movil_Controller.asmx/Alta',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function (result) {
                var Resultado = JSON.parse(result.d);
                if (Resultado != null && Resultado != undefined && Resultado != '') {
                    if (Resultado.Estatus == 'success') {
                        _search();
                        isComplete = true;
                    } else if (Resultado.Estatus == 'error') {
                        _validation_sumary(Resultado);
                    }
                } else {
                    _validation_sumary(Resultado);
                }
            }
        });
    } catch (e) {
        _mostrar_mensaje('Informe Técnico', e);
    }
    return isComplete;
}
function _modificar_parametros() {
    var Parametros = null;
    var isComplete = false;

    try {
         Parametros = new Object();
         Parametros.Parametro_ID = parseInt($('#txt_parametro_id').val());
         Parametros.Ruta_Servicios_Web = $('#txt_ruta_servicios').val();
         Parametros.Latitud_Ubicacion_Empresa = parseFloat($('#txt_latitud_empresa').val());
         Parametros.Longitud_Ubicacion_Empresa =parseFloat( $('#txt_longitud_empresa').val());
         Parametros.Latitud_Ubicacion_Vigilancia =parseFloat( $('#txt_latitud_vigilancia').val());
         Parametros.Longitud_Ubicacion_Vigilancia = parseFloat($('#txt_longitud_vigilancia').val());
         Parametros.Distancia_Vigilancia = parseInt($('#txt_distancia_vigilancia').val());
         Parametros.Tiempo_Peticiones_GPS =parseInt($('#txt_tiempo').val());
         Parametros.Aplicacion_ID = $('#txt_aplicacion_id').val();
         Parametros.Remitente_ID = $('#txt_remitente_id').val();
         Parametros.Api_Token = $('#txt_api_token').val();
         Parametros.Perfil_Seguridad = $('#txt_perfil_seguridad').val();
         Parametros.Estatus_ID = parseInt($('#cmb_estatus').val());

        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(Parametros) });

        $.ajax({
            type: 'POST',
            url: 'controllers/Parametros_Movil_Controller.asmx/Actualizar',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function (result) {
                var Resultado = JSON.parse(result.d);
                if (Resultado != null && Resultado != undefined && Resultado != '') {
                    if (Resultado.Estatus == 'success') {
                        _search();
                        isComplete = true;
                    } else if (Resultado.Estatus == 'error') {
                        _validation_sumary(Resultado);
                    }
                } else {
                    _validation_sumary(Resultado);
                }
            }
        });

    } catch (e) {
        _mostrar_mensaje('Informe Técnico', e);
    }
    return isComplete;
}
function _eliminar_parametro(parametro_id) {
    var Parametro = null;

    try {
        Parametro = new Object();
        Parametro.Parametro_ID= parseInt(parametro_id);

        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(Parametro) });

        $.ajax({
            type: 'POST',
            url: 'controllers/Parametros_Movil_Controller.asmx/Eliminar',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function (result) {
                var Resultado = JSON.parse(result.d);
                if (Resultado != null && Resultado != undefined && Resultado != '') {
                    if (Resultado.Estatus == 'success') {
                        _search();
                    } else if (Resultado.Estatus == 'error') {
                        _mostrar_mensaje(Resultado.Titulo, Resultado.Mensaje);
                    }
                } else { _mostrar_mensaje(Resultado.Titulo, Resultado.Mensaje); }
            }
        });

    } catch (e) {
        _mostrar_mensaje('Informe Técnico', e);
    }
}
function _modal() {
    var tags = '';
    try {
        tags += '<div class="modal fade" id="modal_datos" name="modal_datos" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">';
        tags += '<div class="modal-dialog">';
        tags += '<div class="modal-content">';

        tags += '<div class="modal-header">';
        tags += '<button type="button" class="close cancelar" data-dismiss="modal" aria-label="Close" onclick="_set_close_modal(true);"><i class="fa fa-times"></i></button>';
        tags += '<h4 class="modal-title" id="myModalLabel">';
        tags += '<label id="lbl_titulo"></label>';
        tags += '</h4>';
        tags += '</div>';

        tags += '<div class="modal-body">';

        tags += '<div class="row">' +
            ' <div class="col-md-4">' +
            '            <label class="fuente_lbl_controles">(*) Estatus</label>' +
            '        <select id="cmb_estatus" name="cmb_estatus" class="form-control input-sm" disabled="disabled" data-parsley-required="true" required ></select> ' +
            '        <input type="hidden" id="txt_parametro_id"/>' +
            '       </div> ' +

            ' <div class="col-md-8">' +
            '            <label class="fuente_lbl_controles">(*) Ruta Servicios Web</label>' +
            '        <input type="text" id="txt_ruta_servicios" name="txt_ruta_servicios" class="form-control input-sm" placeholder="(*)Ruta Servicios Web" data-parsley-required="true" maxlength="100" required /> ' +
            '    </div>' +
            '</div>' +
            '<div class="row">' +
            ' <div class="col-md-6">' +
            '            <label class="fuente_lbl_controles">(*) Latitud Ubicacion Empresa</label>' +
            '        <input type="text" id="txt_latitud_empresa" name="txt_latitud_empresa" class="form-control input-sm"  placeholder="(*)Latitud Ubicacion Empresa" data-parsley-required="true" maxlength="100" required /> ' +
            '       </div> ' +

            ' <div class="col-md-6">' +
            '            <label class="fuente_lbl_controles">(*) Longitud Ubicacion Empresa</label>' +
            '        <input type="text" id="txt_longitud_empresa" name="txt_longitud_empresa" class="form-control input-sm"  placeholder="(*)Longitud Ubicacion Empresa" data-parsley-required="true" maxlength="100" required /> ' +
            '    </div>' +
            '</div>' +
            '<div class="row">' +
            ' <div class="col-md-6">' +
            '            <label class="fuente_lbl_controles">(*) Latitud Ubicacion Vigilancia</label>' +
            '        <input type="text" id="txt_latitud_vigilancia" name="txt_latitud_vigilancia" class="form-control input-sm"  placeholder="(*)Latitud Ubicacion Vigilancia" data-parsley-required="true" maxlength="100" required /> ' +
            '       </div> ' +

            ' <div class="col-md-6">' +
            '            <label class="fuente_lbl_controles">(*) Longitud Ubicacion Vigilancia</label>' +
            '        <input type="text" id="txt_longitud_vigilancia" name="txt_longitud_vigilancia" class="form-control input-sm"  placeholder="(*)Longitud Ubicacion Vigilancia" data-parsley-required="true" maxlength="100" required /> ' +
            '    </div>' +
            '</div>' +
               '<div class="row">' +
            ' <div class="col-md-6">' +
            '            <label class="fuente_lbl_controles">(*) Distancia Vigilancia</label>' +
            '        <input type="text" id="txt_distancia_vigilancia" name="txt_distancia_vigilancia" class="form-control input-sm"  placeholder="(*)Distancia Vigilancia" data-parsley-required="true" maxlength="100" required /> ' +
            '       </div> ' +
             ' <div class="col-md-6">' +
            '            <label class="fuente_lbl_controles">(*) Tiempo Peticiones GPS</label>' +
            '        <input type="text" id="txt_tiempo" name="txt_tiempo" class="form-control input-sm"  placeholder="(*)Tiempo Peticiones GPS" data-parsley-required="true" maxlength="100" required /> ' +
            '       </div> ' +
            '</div>' +
             '<div class="row">' +
            ' <div class="col-md-6">' +
            '            <label class="fuente_lbl_controles">(*) Aplicacion ID</label>' +
            '        <input type="text" id="txt_aplicacion_id" name="txt_aplicacion_id" class="form-control input-sm"  placeholder="(*)Aplicacion ID" data-parsley-required="true" maxlength="100" required /> ' +
            '       </div> ' +

            ' <div class="col-md-6">' +
            '            <label class="fuente_lbl_controles">(*) Remitente ID</label>' +
            '        <input type="text" id="txt_remitente_id" name="txt_remitente_id" class="form-control input-sm"  placeholder="(*)Remitente ID" data-parsley-required="true" maxlength="100" required /> ' +
            '    </div>' +
            '</div>' +
             '<div class="row">' +
            ' <div class="col-md-4">' +
            '            <label class="fuente_lbl_controles">(*) Perfil Seguridad</label>' +
            '        <input type="text" id="txt_perfil_seguridad" name="txt_perfil_seguridad" class="form-control input-sm"  placeholder="(*)Perfil Seguridad" data-parsley-required="true" maxlength="100" required /> ' +
            '       </div> ' +

            ' <div class="col-md-8">' +
            '            <label class="fuente_lbl_controles">(*) Api_Token</label>' +
            '        <input type="text" id="txt_api_token" name="txt_api_token" class="form-control input-sm" placeholder="(*)Api Token" data-parsley-required="true" maxlength="100" required /> ' +
            '    </div>' +
            '</div>';

        tags += '</div>';

        tags += '<div class="modal-footer">';
        tags += '<div class="row">';

        tags += '<div class="col-md-7">';
        tags += '<div id="sumary_error" class="alert alert-danger text-left" style="width: 277.78px !important; display:none;">';
        tags += '<label id="lbl_msg_error"/>';
        tags += '</div>';
        tags += '</div>';

        tags += '<div class="col-md-5">';
        tags += '<div class="form-inline">';
        tags += '<button type="submit" class="btn btn-info btn-icon btn-icon-standalone btn-xs" id="btn_guardar_datos" title="Guardar"><i class="fa fa-check"></i><span>Aceptar</span></button>';
        tags += '<button type="button" class="btn btn-danger btn-icon btn-icon-standalone btn-xs cancelar" data-dismiss="modal" id="btn_cancelar" aria-label="Close" onclick="_set_close_modal(true);" title="Cancelar operaci&oacute;n"><i class="fa fa-remove"></i><span>Cancelar</span></button>';
        tags += '</div>';
        tags += '</div>';

        tags += '</div>';
        tags += '</div>';

        tags += '</div>';
        tags += '</div>';
        tags += '</div>';

        $(tags).appendTo('body');

        $('#btn_guardar_datos').bind('click', function (e) {
            e.preventDefault();

            if ($('#txt_parametro_id').val() != null && $('#txt_parametro_id').val() != undefined && $('#txt_parametro_id').val() != '') {
                var _output = _validation('editar');
                if (_output.Estatus) {
                    if (_modificar_parametros()) {
                        _estado_inicial();
                        _set_close_modal(true);
                        jQuery('#modal_datos').modal('hide');
                    }
                } else {
                    _set_close_modal(false);
                }
            } else {
                var _output = _validation('alta');
                if (_output.Estatus) {
                    if (_alta_parametros()) {
                        _estado_inicial();
                        _set_close_modal(true);
                        jQuery('#modal_datos').modal('hide');
                    }
                } else {
                    _set_close_modal(false);
                }
            }
        });
    } catch (e) {
        _mostrar_mensaje('Informe técnico', e);
    }
}
function _set_close_modal(state) {
    closeModal = state;
}
function btn_editar_click(parametros) {
    var row = $(parametros).data('parametros');
    $('#txt_parametro_id').val(row.Parametro_ID);
    $('#txt_ruta_servicios').val(row.Ruta_Servicios_Web);
    $('#txt_latitud_empresa').val(row.Latitud_Ubicacion_Empresa);
    $('#txt_longitud_empresa').val(row.Longitud_Ubicacion_Empresa);
    $('#txt_latitud_vigilancia').val(row.Latitud_Ubicacion_Vigilancia);
    $('#txt_longitud_vigilancia').val(row.Longitud_Ubicacion_Vigilancia);
    $('#txt_distancia_vigilancia').val(row.Distancia_Vigilancia);
    $('#txt_tiempo').val(row.Tiempo_Peticiones_GPS);
    $('#txt_aplicacion_id').val(row.Aplicacion_ID);
    $('#txt_remitente_id').val(row.Remitente_ID);
    $('#txt_api_token').val(row.Api_Token);
    $('#txt_perfil_seguridad').val(row.Perfil_Seguridad);
    $('#cmb_estatus').val(row.Estatus);


    _clear_all_class_error();
    _habilitar_controles('Modificar');
    _launch_modal('<i class="glyphicon glyphicon-edit" style="font-size: 25px;"></i>&nbsp;&nbsp;Actualizar registro');
}
function btn_eliminar_click(parametros) {
    var row = $(parametros).data('parametros');

    bootbox.confirm({
        title: 'Eliminar Registro',
        message: '¿Está seguro de eliminar el registro seleccionado?',
        callback: function (result) {
            if (result) {
                _eliminar_parametro(row.Parametro_ID);
            }
            _estado_inicial();
        }
    });
}
function _set_location_toolbar() {
    $('#toolbar').parent().removeClass("pull-left");
    $('#toolbar').parent().addClass("pull-right");
}
function _set_title_modal(Titulo) {
    $("#lbl_titulo").html(Titulo);
}
function _search() {
    var filtros = null;
    try {
        show_loading_bar({
            pct: 78,
            wait: .5,
            delay: .5,
            finish: function (pct) {
                filtros = new Object();
                filtros.Nombre = $('#txt_busqueda_por_nombre').val() === '' ? '' : $('#txt_busqueda_por_nombre').val();

                var $data = JSON.stringify({ 'jsonObject': JSON.stringify(filtros) });

                jQuery.ajax({
                    type: 'POST',
                    url: 'controllers/Parametros_Movil_Controller.asmx/Consultar_Parametros_Por_Filtros',
                    data: $data,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true,
                    cache: false,
                    success: function (datos) {
                        if (datos !== null) {
                            $('#tbl_parametros_movil').bootstrapTable('load', JSON.parse(datos.d));
                            hide_loading_bar();
                        }
                    }
                });
            }
        });
    } catch (e) {

    }
}
function _validation(opcion) {
    var _output = new Object();

    _output.Estatus = true;
    _output.Mensaje = '';

    if (!$('#txt_ruta_servicios').parsley().isValid()) {
        _add_class_error('#txt_ruta_servicios');
        _output.Estatus = false;
        _output.Mensaje += '&nbsp;<i class="fa fa-angle-double-right"></i>&nbsp;La ruta del servicio web  es un dato requerido.<br />';
    } 

    if (!$('#cmb_estatus').parsley().isValid()) {
        _add_class_error('#cmb_estatus');
        _output.Estatus = false;
        _output.Mensaje += '&nbsp;<i class="fa fa-angle-double-right"></i>&nbsp;El estatus es un dato requerido.<br />';
    } 
    if (!_output.Estatus) _validation_sumary(_output);

    return _output;
}
function _add_class_error(selector) {
    $(selector).addClass('alert-danger');
}
function _remove_class_error(selector) {
    $(selector).removeClass('alert-danger');
}
function _clear_all_class_error() {
    $('#modal_datos input[type=text]').each(function (index, element) {
        _remove_class_error('#' + $(this).attr('id'));
    });
    $('#modal_datos select').each(function () {
        _remove_class_error('#' + $(this).attr('id'));
    });
}
function _validation_sumary(validation) {
    var header_message = '<i class="fa fa-exclamation-triangle fa-2x"></i><span>Observaciones</span><br />';

    if (validation == null) {
        $('#lbl_msg_error').html('');
        $('#sumary_error').css('display', 'none');
    } else {
        $('#lbl_msg_error').html(header_message + validation.Mensaje);
        $('#sumary_error').css('display', 'block');
    }
}
function _launch_modal(title_window) {
    _set_title_modal(title_window);
    jQuery('#modal_datos').modal('show', { backdrop: 'static', keyboard: false });
    
}
function _enter_keypress_modal() {
    var $btn = $('[id$=btn_guardar_datos]').get(0);
    $(window).keypress(function (e) {
        if (e.which === 13 && e.target.type !== 'textarea') {
            if ($btn != undefined && $btn != null) {
                if ($btn.type === 'submit')
                    $btn.click();
                else
                    eval($btn.href);
                return false;
            }
        }
    });
}
function _validate_fields(value, id, field) {
    var Parametros = null;
    var Resultado = null;

    try {
        Parametros = new Object();
        if (id !== null)
            Parametros.Parametro_ID = parseInt(id);

        switch (field) {
            case 'nombre':
                Parametros.Ruta_Servicios_Web = value;
                break;
            default:
        }

        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(Parametros) });

        $.ajax({
            type: 'POST',
            url: 'controllers/Parametros_Movil_Controller.asmx/Consultar_Parametros_Por_Nombre',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function (result) {
                if (result !== null)
                    Resultado = JSON.parse(result.d);
            }
        });
    } catch (e) {
        Resultado = new Object();
        Resultado.Estatus = 'error';
        Resultado.Mensaje = 'No fue posible realizar la validación del ' + field + ' en la base de datos.';
        _mostrar_mensaje('Informe Técnico', e);
    }
    return Resultado;
}
function _load_estatus() {
    var filtros = null;
    try {
        jQuery.ajax({
            type: 'POST',
            url: 'controllers/Parametros_Movil_Controller.asmx/Consultar_Estatus',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            cache: false,
            success: function (datos) {
                if (datos !== null) {
                    var datos_combo = $.parseJSON(datos.d);
                    var select2 = $('#cmb_estatus');
                    $('option', select2).remove();
                    var options = '<option value="">--SELECCIONE--</option>';
                    for (var Indice_Estatus = 0; Indice_Estatus < datos_combo.length; Indice_Estatus++) {
                        options += '<option value="' + datos_combo[Indice_Estatus].Estatus_ID + '">' + datos_combo[Indice_Estatus].Estatus.toUpperCase() + '</option>';
                        if (datos_combo[Indice_Estatus].Estatus.toUpperCase() == 'ACTIVO') {
                            estatusActivo = datos_combo[Indice_Estatus].Estatus_ID;
                        }
                    }
                    select2.append(options);
                }
            }
        });
    } catch (e) {

    }
}
