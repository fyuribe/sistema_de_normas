﻿
var estatusActivo = '';
var $table_horarios = null;
var $table = null;

$(document).on('ready', function () {
    _inicializar_pagina();
    $table_horarios = $('#tbl_lista_horarios');
    _limpiar_controles();
    _set_location_toolbar();
    _eventos_textbox();
    _eventos();

});
function _inicializar_pagina() {
    try {
        _estado_inicial();

        //$('#cmb_iniciador').select2();
        _habilitar_controles('Inicial');
        _crear_tbl_solicitud_citas();
        _crear_tbl_horarios();
        $('#tbl_solicitud_citas').bootstrapTable('refresh', 'controllers/Ope_Solicitud_Citas_Controller.asmx/Consultar_Solicitud_Por_Filtros');
        _consultar_departamentos('#cmb_departamento_visitado');
       // _consultar_departamento1('#cmb_departamento_visitado');
        //_consultar_departamento('#cmb_departamento_visitado')
       // _consultar_empleados1('#cmb_empleado_visitado');
       // _consultar_empleado('#cmb_empleado_visitado');
       // _consultar_datos_empleado('#cmb_empleado_visitado');
        _load_estatus();
        _set_location_toolbar();
    } catch (e) {
        _mostrar_mensaje('Informe Técnico', e);
    }
}
function _habilitar_controles(opcion) {
    $('#div_principal_solicitud_citas').css('display', 'none');
    $('#div_Informacion').css('display', 'none');
    switch (opcion) {
        case 'Inicial':
            $('#div_principal_solicitud_citas').css('display', 'block');
            break;
        case 'Nuevo':
            $('#div_Informacion').css('display', 'block');
            $('#tbl_avisos_coordinacion').bootstrapTable('hideColumn', 'Modificar');
            break;
        case 'Modificar':
            $('#div_Informacion').css('display', 'block');
           // $('#tbl_avisos_coordinacion').bootstrapTable('hideColumn', 'Modificar');
            $('#cmb_estatus').attr("disabled", true);
            break;
        case 'Visualizar':
            $('#tbl_lista_horarios').bootstrapTable('hideColumn', 'eliminar');
            $("#btn_salir").css('display', 'none');
            $('#cmb_empleado_visitado').attr("disabled", true);
            $('#cmb_departamento_visitado').attr("disabled", true);
            $('#cmb_estatus').attr("disabled", true);
            $('#div_Informacion').css('display', 'block');
            $('#nombre_solicitante').attr("disabled", true);
            $('#apellidos_solicitante').attr("disabled", true);
            $('#correo_solicitante').attr("disabled", true);
            $('#telefono_solicitante').attr("disabled", true);
            $('#txt_motivo_cita').attr("disabled", true);
            $("#btn_guardar_solicitud").css('display', 'none');
            $("#btn_regresar").css('display', 'block');
            $('#btn_agregar_horario').attr('disabled',true)

            break;
    }
}
function _eventos() {
    try {
        $('#modal_datos').on('hidden.bs.modal', function () {
            if (!closeModal)
                $(this).modal('show');
        });
        $('#btn_nuevo').on('click', function (e) {
            $('#div_Informacion').css('display', 'block');
            $('#div_principal_solicitud_citas').css('display', 'none');
            $('#cmb_estatus').attr("disabled", true);
            $('#cmb_estatus').val(estatusActivo);
           _limpiar_controles();
        });

        $('#btn_inicio').on('click', function (e) { e.preventDefault(); window.location.href = '../Paginas_Generales/Frm_Apl_Principal.aspx'; });
        $('#btn_busqueda').on('click', function (e) {

            if ($('#txt_busqueda_no_solicitud').val() == ''&& $('#txt_busqueda_solicitante').val() == '' && $('#txt_busqueda_apellidos').val() == '')  {
                _mostrar_mensaje('Validación', "<i class='fa fa-exclamation-circle'style = 'color:#f2041a;' ></ i > &nbsp;Debes proporcionar por lo menos un filtro de búsqueda");
                
            }
            else  

                _search_solicitudes_por_filtros();
            $('#txt_busqueda_no_solicitud').val('');
            $('#txt_busqueda_solicitante').val('');
            $('#txt_busqueda_apellidos').val('');
        });
        $('#btn_salir').on('click', function (e) {
          
                $('#div_Informacion').css('display', 'none');
                $('#div_principal_solicitud_citas').css('display', 'block');
                $('#tbl_solicitud_citas').bootstrapTable('refresh', 'controllers/Ope_Solicitud_Citas_Controller.asmx/Consultar_Solicitud_Por_Filtros');
                _limpiar_controles();
        });
        $('#btn_guardar_solicitud').on('click', function (e) {
            e.preventDefault();
            bootbox.confirm({
                title: 'Guardar Solicitud Cita',
                message: '¿Está seguro de guardar la solicitud?',
                callback: function (result) {
                    if (result) {
                        var output = _validar_datos();
                        if (output.Estatus) {

                            if ($table_horarios.bootstrapTable('getOptions').totalRows < 1) {
                                _mostrar_mensaje('Validación', 'Se debe agregar por lo menos una propuesta de horarios.');
                                return;
                            }

                            if ($('#txt_no_solicitud').val() == '')

                                _guardar_solicitud_cita();

                            else {
                                _modificar_solicitud_cita();
                                _limpiar_controles();
                                $('#div_Informacion').css('display', 'none');
                                $('#div_principal_solicitud_citas').css('display', 'block');                          
                            }
                        } else _mostrar_mensaje('Validación', output.Mensaje);
                    }
                }
            });
        });
        $('#btn_regresar').on('click', function (e) { e.preventDefault(); window.location.href = '../Operacion/Frm_Ope_Solicitud_Citas.aspx'; });

        $('#btn_agregar_horario').on('click', function (e) {
            e.preventDefault();
            //var inicio = $('#h_inicio').val();

            //var fin = $('#h_termino').val();
            //if (inicio > fin) {
            //   _mostrar_mensaje('Información','La hora de inicio debe ser menor a la hora fin')
            //}
            //else
                _agregar_horarios();
          $('#f_cita').val('');
          $('#h_inicio').val('');
          $('#h_termino').val('');           
        });
        $('#modal_datos input[type=text]').each(function (index, element) {
            $(this).on('focus', function () {
                _remove_class_error('#' + $(this).attr('id'));
            });
        });   
        $('#modal_datos select').each(function (index, element) {
            $(this).on('focus', function () {
                _remove_class_error('#' + $(this).attr('id'));
            });
        });

    } catch (e) {
        _mostrar_mensaje('Informe Técnico', e);
    }
}
function _set_location_toolbar() {
    $('#toolbar').parent().removeClass("pull-left");
    $('#toolbar').parent().addClass("pull-right");
}
function _estado_inicial() {
    try {
        $('#dtp_fecha_cita').datetimepicker({
            defaultDate: new Date(),
            viewMode: 'days',
            locale: 'es',
            format: "DD/MM/YYYY"
        });
        $("#dtp_fecha_cita").datetimepicker("useCurrent", true);

        $('#dtp_fecha_busqueda').datetimepicker({
            defaultDate: new Date(),
            viewMode: 'days',
            locale: 'es',
            format: "DD/MM/YYYY"
        });
        $("#dtp_fecha_busqueda").datetimepicker("useCurrent", true);

        $('#dtp_hora_inicio').datetimepicker({
            format: "LT"
            //format: "hh:mm"
        });
        $('#dtp_hora_inicio').datetimepicker("useCurrent", true);

        $('#dtp_hora_fin').datetimepicker({
            format: "LT"
            //format: "hh:mm"
        });
        $("#dtp_hora_fin").datetimepicker("useCurrent", true);

    } catch (ex) {
        _mostrar_mensaje("Informe T&eacute;nico", ex.message)
    }
}

function _eventos_textbox() {
    $('#nombre_solicitante').on('blur', function () {
        $(this).val($(this).val().match(/^[a-z A-z]{2,16}$/) ? $(this).val() : '');
    });
    //$('#apellidos_solicitante').on('blur', function () {
    //    $(this).val($(this).val().match(/^[a-z A-z]{2,16}$/) ? $(this).val() : '');
    //});
    $('#correo_solicitante').on('blur', function () {
        $(this).val($(this).val().match(/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i) ? $(this).val() : '');
    });

    
}
function _limpiar_controles() {
    //$('select').each(function () { $(this).val(''); });
    $('#cmb_departamento_visitado').empty().trigger("change");
    $('#cmb_empleado_visitado').empty().trigger("change");
    $('#nombre_solicitante').val('');
    $('#apellidos_solicitante').val('');
    $('#correo_solicitante').val('');
    $('#telefono_solicitante').val('');
    $('#txt_motivo_cita').val('');
    $('#txt_no_solicitud').val('');
    $('#f_cita').val('');
    $('#h_inicio').val('');
    $('#h_termino').val('');
    $('#tbl_lista_horarios').bootstrapTable('load', []);
    _validation_sumary(null);
}
function _validation_sumary(validation) {
    var header_message = '<i class="fa fa-exclamation-triangle fa-2x"></i><span>Observations</span><br />';
    if (validation == null) {
        $('#lbl_msg_error').html('');
        $('#sumary_error').css('display', 'none');
    } else {
        $('#lbl_msg_error').html(header_message + validation.Mensaje);
        $('#sumary_error').css('display', 'block');
    }
}
function _consultar_departamento1(cmb) {
    try {
        $(cmb).select2({
            language: "es",
            theme: "classic",
            placeholder: 'Buscar Departamento',
            allowClear: true,
            tags: true

        });

    } catch (e) {
        _mostrar_mensaje('Informe Técnico', e);
    }

}
function _consultar_departamento(cmb) {
    try {
        $(cmb).select2({
            language: "es",
            theme: "classic",
            placeholder: 'Buscar Departamento',
            allowClear: true,
            ajax: {
                url: 'controllers/Ope_Solicitud_Citas_Controller.asmx/Consultar_Departamentos',
                cache: "true",
                dataType: 'json',
                type: "POST",
                delay: 250,
                cache: true,
                params: {
                    contentType: 'application/json; charset=utf-8'
                },
                quietMillis: 100,
                results: function (data) {
                    return { results: data };
                },
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page
                    };
                },
                processResults: function (data, page) {
                    return {
                        results: data
                    };
                },
            }
        });

    } catch (e) {
        _mostrar_mensaje('Informe Técnico', e);
    }
}
function _consultar_datos_empleado(cmb) {

    try {
        $(cmb).select2({
            language: "es",
            theme: "classic",
            placeholder: 'Buscar Empleado',
            allowClear: true,
            ajax: {
                url: 'controllers/Ope_Solicitud_Citas_Controller.asmx/Consultar_Empleados',
                cache: "true",
                dataType: 'json',
                type: "POST",
                delay: 250,
                cache: true,
                params: {
                    contentType: 'application/json; charset=utf-8'
                },
                quietMillis: 100,
                results: function (data) {
                    return { results: data };
                },
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page
                    };
                },
                processResults: function (data, page) {
                    return {
                        results: data
                    };
                },
            }
        });

        $('#cmb_empleado_visitado').on("select2:select", function (evt) {
            var id = evt.params.data.id;

            if ($('#cmb_empleado_visitado:selected').val() !== '' || $('#cmb_empleado_visitado :selected').val() !== undefined) {
                var Empleado = new Object();
                Empleado.Empleado_ID = parseInt($('#cmb_empleado_visitado :selected').val());
                //$('#txt_email_contratista').val('');

                var $data = JSON.stringify({ 'jsonObject': JSON.stringify(Empleado) });

                $.ajax({
                    type: 'POST',
                    url: 'controllers/Ope_Solicitud_Citas_Controller.asmx/Consultar_Datos_Empleados',
                    cache: "true",
                    data: $data,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: false,
                    cache: true,
                    tags: true,
                    success: function (datos) {

                        if (datos !== null && datos !== undefined) {
                            datos_empleado = JSON.parse(datos.d);
                            $("#cmb_departamento_visitado").empty().trigger("change");

                            for (i = 0; i <= datos_empleado.length; i++) {
                                $('#cmb_departamento_visitado').val(datos_empleado[i].Departamento_ID).trigger("change");

                                $('#cmb_departamento_visitado').select2({ tags: true, data: [{ id: datos_empleado[i].Departamento_ID, text: datos_empleado[i].Departamento }] });

                            }

                        }

                    }

                });

            }
        });


    } catch (e) {
        _mostrar_mensaje('Informe Técnico', e);
    }



}
function _consultar_departamentos(cmb) {

    try {
        $(cmb).select2({
            language: "es",
            theme: "classic",
            placeholder: 'Buscar Departamento',
            allowClear: true,
            ajax: {
                url: 'controllers/Ope_Solicitud_Citas_Controller.asmx/Consultar_Departamentos',
                cache: "true",
                dataType: 'json',
                type: "POST",
                delay: 250,
                cache: true,
                params: {
                    contentType: 'application/json; charset=utf-8'
                },
                quietMillis: 100,
                results: function (data) {
                    return { results: data };
                },
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page
                    };
                },
                processResults: function (data, page) {
                    return {
                        results: data
                    };
                },
            }
        });

        $('#cmb_departamento_visitado').on("select2:select", function (evt) {
            var id = evt.params.data.id;

            if ($('#cmb_departamento_visitado:selected').val() !== '' || $('#cmb_departamento_visitado :selected').val() !== undefined) {
                var Departamento = new Object();
                Departamento.Departamento_ID = parseInt($('#cmb_departamento_visitado :selected').val());
                //$('#txt_email_contratista').val('');

                var $data = JSON.stringify({ 'jsonObject': JSON.stringify(Departamento) });

                $.ajax({
                    type: 'POST',
                    url: 'controllers/Ope_Solicitud_Citas_Controller.asmx/Consultar_Datos_Departamentos',
                    cache: "true",
                    data: $data,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: false,
                    cache: true,
                    tags: true,
                    success: function (data) {
                        if (data !== null) {
                            var datos_combo = $.parseJSON(data.d);
                            var select2 = $('#cmb_empleado_visitado');
                            $('option', select2).remove();
                            var options = '<option value="">--SELECCIONE--</option>';
                            for (var Indice_Estatus = 0; Indice_Estatus < datos_combo.length; Indice_Estatus++) {
                                options += '<option value="' + datos_combo[Indice_Estatus].Empleado_ID + '">' + datos_combo[Indice_Estatus].Empleado + '</option>';
                              
                            }
                            select2.append(options);
                        }
                    }

                });

            }
        });


    } catch (e) {
        _mostrar_mensaje('Informe Técnico', e);
    }



}
function _consultar_empleados1(cmb) {
    try {
        $(cmb).select2({
            language: "es",
            theme: "classic",
            placeholder: 'Buscar Empleado',
            allowClear: true,
            tags: true

        });

    } catch (e) {
        _mostrar_mensaje('Informe Técnico', e);
    }

}
function _consultar_empleado(cmb) {
    try {
        $(cmb).select2({
            language: "es",
            theme: "classic",
            placeholder: 'Buscar empleado',
            allowClear: true,
            ajax: {
                url: 'controllers/Ope_Solicitud_Citas_Controller.asmx/Consultar_Empleados',
                cache: "true",
                dataType: 'json',
                type: "POST",
                delay: 250,
                cache: true,
                params: {
                    contentType: 'application/json; charset=utf-8'
                },
                quietMillis: 100,
                results: function (data) {
                    return { results: data };
                },
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page
                    };
                },
                processResults: function (data, page) {
                    return {
                        results: data
                    };
                },
            }
        });

    } catch (e) {
        _mostrar_mensaje('Informe Técnico', e);
    }
}

function _guardar_solicitud_cita() {
    var Solicitud = null;
    try {
        Solicitud = new Object();
        Solicitud.Empleado_ID = $('#cmb_empleado_visitado').val() == '' ? 0 : parseInt($('#cmb_empleado_visitado').val());
        if (isNaN(Solicitud.Empleado_ID)) {
            Solicitud.Empleado_ID = 0;
            Solicitud.Empleado_Visitado = $('#cmb_empleado_visitado').val();
        }
        Solicitud.Departamento_ID = $('#cmb_departamento_visitado').val() == '' ? 0 : parseInt($('#cmb_departamento_visitado').val());
        Solicitud.Estatus_ID = $('#cmb_estatus').val() == '' ? 0 : parseInt($('#cmb_estatus').val());
        Solicitud.Nombre_Solicitante = $('#nombre_solicitante').val();
        Solicitud.Apellidos = $('#apellidos_solicitante').val();
        Solicitud.Correo = $('#correo_solicitante').val();
        Solicitud.Telefono = $('#telefono_solicitante').val();
        Solicitud.Motivo_Cita = $('#txt_motivo_cita').val();
       
        Solicitud.Datos_Detalles_Horarios = JSON.stringify($('#tbl_lista_horarios').bootstrapTable('getData'));
       

        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(Solicitud) });

        $.ajax({
            type: 'POST',
            url: 'controllers/Ope_Solicitud_Citas_Controller.asmx/Alta',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function ($result) {
                var Resultado = JSON.parse($result.d);

                if (Resultado != null && Resultado != undefined && Resultado != '') {
                    if (Resultado.Estatus == 'success') {
                        _mostrar_mensaje(Resultado.Titulo, Resultado.Mensaje);
                        _inicializar_pagina();
                        _limpiar_controles();
                    } else if (Resultado.Estatus == 'error') {
                        _mostrar_mensaje(Resultado.Titulo, Resultado.Mensaje);
                    }
                } else { _mostrar_mensaje(Resultado.Titulo, Resultado.Mensaje); }
            }
        });


    } catch (ex) {
        _mostrar_mensaje("Informe T&eacute;cnico", ex);
    }
}
function _crear_tbl_solicitud_citas() {
    try {
        $('#tbl_solicitud_citas').bootstrapTable('destroy');
        $('#tbl_solicitud_citas').bootstrapTable({
            cache: false,
            striped: true,
            pagination: true,
            pageSize: 10,
            pageList: [10, 25, 50, 100, 200],
            smartDysplay: false,
            search: true,
            showColumns: false,
            showRefresh: false,
            minimumCountColumns: 2,
            clickToSelect: false,

            columns: [

                { field: 'No_Solicitud', title: 'No. Solicitud', width: '10%', align: 'left', valign: 'bottom', sortable: true, clickToSelect: false },
                { field: 'Nombre_Solicitante', title: 'Nombre Solicitante', width: '15%', align: 'left', valign: 'bottom', sortable: true, clickToSelect: false },
                { field: 'Empleado_Visitado', title: 'Empleado Visitado', width: '15%', align: 'left', valign: 'bottom', sortable: true, clickToSelect: false },
                { field: 'Departamento', title: 'Departamento', width: '15%', align: 'left', valign: 'bottom', sortable: true, clickToSelect: false },
                 //{
                 //    field: 'Fecha_Cita', title: 'Fecha cita',
                 //    width: '10%', align: 'center', sortable: true, formatter: function (value,
                 //    row) { return parsefecha(value); }
                 //},
                {
                    field: 'Estatus', title: 'Estatus', width: '10%', align: 'left', valign: '', halign: 'left', sortable: true, clickToSelect: false, formatter: function (value, row) {

                        var color = '';
                        switch (value) {
                            case 'CANCELADO':
                                color = "black";
                                break;
                            case 'ABIERTO':
                                color = "yellow";
                                break;
                            case 'PROCESO':
                                color = 'rgba(85, 171, 237, 1)';
                                break;
                            case 'VENCIDO':
                                color = "orange";
                                break;
                            case 'RECHAZADO':
                                color = "red";
                                break;
                            case 'AUTORIZADO':
                                color = "green";
                                break;
                            case 'VALIDADO':
                                color = "orange";
                                break;
                            case 'FINALIZADO':
                                //color = "rgba(221, 24, 24, 1)";
                                color = "green";
                                break;
                            default:
                                color = 'rgba(85, 171, 237, 1)';
                        }

                        return '<i class="fa fa-circle" style="color: ' + color + '"></i>&nbsp;' + value;
                    }
                },
                {
                    field: 'Modificar', title: 'Modificar', align: 'center', width: '3%',
                    formatter: function (value, row) {
                        return '<div>' +
                                    '<a class="remove ml10 edit _editar_cambio" id="remove_' + row.No_Solicitud + '" href="javascript:void(0)" data-solicitud_cita=\'' + JSON.stringify(row) +
                                    '\' onclick="btn_editar_click(this);"><i class="glyphicon glyphicon-duplicate" title=""></i></a>' +
                               '</div>';
                    }
                },
                {
                    field: 'CANCELED', title: 'Cancelar', align: 'center', width: '3%',
                    formatter: function (value, row) {
                        return '<div>' +
                                    '<a class="remove ml10 edit _editar_cancelar" id="remove_' + row.No_Solicitud + '" href="javascript:void(0)" data-solicitud_cita=\'' + JSON.stringify(row) +
                                    '\' onclick="btn_cancelar_click(this);"><i class="glyphicon glyphicon-remove" title=""></i></a>' +
                               '</div>';
                    }
                },
                {
                    field: 'Visualizar', title: 'Visualizar', align: 'center', width: '2%',
                    formatter: function (value, row) {
                        return '<div>' +
                                    '<a class="remove ml10 edit _ver_orden_cambio" id="remove_' + row.No_Solicitud + '" href="javascript:void(0)" data-solicitud_cita=\'' + JSON.stringify(row) + '\' onclick="btn_visualizar_click(this);"><i class="glyphicon glyphicon-eye-open" title=""></i></a>' +
                                '</div>';
                    }

                }

            ]
        });
    } catch (e) {

    }
}
function _search() {
    var filtros = null;
    try {
        show_loading_bar({
            pct: 78,
            wait: .5,
            delay: .5,
            finish: function (pct) {
                filtros = new Object();
                filtros.Nombre_Solicitante = $('#txt_busqueda_por_nombre').val() === '' ? '' : $('#txt_busqueda_por_nombre').val();

                var $data = JSON.stringify({ 'jsonObject': JSON.stringify(filtros) });

                jQuery.ajax({
                    type: 'POST',
                    url: 'controllers/Ope_Solicitud_Citas_Controller.asmx/Consultar_Solicitud_Por_Filtros',
                    data: $data,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true,
                    cache: false,
                    success: function (datos) {
                        if (datos !== null) {
                            $('#tbl_solicitud_citas').bootstrapTable('load', JSON.parse(datos.d));
                            hide_loading_bar();
                        }
                    }
                });
            }
        });
    } catch (e) {

    }
}
function parseTimeHorario(timeString) {
    var dateTime = timeString.split(" ");
    var timeOnly = dateTime[0];
    var times = timeOnly.split(":");
    var temp =(times[0]) + ":" + times[1] + ":" + times[2];
    return temp;
}
function btn_visualizar_click(renglon) {
    var row = $(renglon).data('solicitud_cita');

    $('#autorizacion_empleado').css('display', 'block');
   
    _crear_tbl_horarios_modificar();
    _consultar_horarios(row.No_Solicitud);
    _habilitar_controles('Visualizar');
    $('#visualizar').css('display', 'block');
    $('#titulo').css('display', 'none');

    $("input[name=cb_autorizar]").each(function () {
        $(this).attr("disabled", true)
    });
    var Solicitud = null;

    try {

        Solicitud = new Object();
        Solicitud.No_Solicitud = row.No_Solicitud;

        if (row.Departamento_ID != null)
            Solicitud.Departamento_ID = row.Departamento_ID;
        if (row.Empleado_ID != null)
            Solicitud.Empleado_ID = row.Empleado_ID;

        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(Solicitud) });
        $.ajax({
            type: 'POST',
            url: 'controllers/Ope_Autorizacion_Solicitud_Controller.asmx/Consultar_Una_Solicitud',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function ($result) {
                var Resultado = JSON.parse($result.d);
                if (Resultado != null && Resultado != undefined && Resultado != '') {

                    $("#txt_no_solicitud").val(row.No_Solicitud);
                    // $('#txt_numero_parte').val(Resultado[0].Numero_Parte);

                    $("#cmb_estatus")[0].innerHTML = "";
                    $("#cmb_estatus").select2({ data: [{ id: row.Estatus_ID, text: row.Estatus }] });
                    $("#cmb_estatus").val(row.Estatus_ID).trigger("change");

                    $("#cmb_departamento_visitado")[0].innerHTML = "";
                    $("#cmb_departamento_visitado").select2({ data: [{ id: row.Departamento_ID, text: row.Departamento }] });
                    $("#cmb_departamento_visitado").val(row.Departamento_ID).trigger("change");

                    $("#cmb_empleado_visitado")[0].innerHTML = "";
                    $("#cmb_empleado_visitado").select2({ data: [{ id: row.Empleado_ID, text: row.Empleado_Visitado }] });
                    $("#cmb_empleado_visitado").val(row.Empleado_ID).trigger("change");




                    $('#nombre_solicitante').val(Resultado[0].Nombre_Solicitante);
                    $('#apellidos_solicitante').val(Resultado[0].Apellidos);
                    $('#correo_solicitante').val(Resultado[0].Correo);

                    $('#telefono_solicitante').val(Resultado[0].Telefono);
                    $('#txt_motivo_cita').val(Resultado[0].Motivo_Cita);

                    if (row.Estatus == "CANCELADO" || row.Estatus == "RECHAZADO") {
                        $("#lbl_observaciones").css('display', 'block');
                        $("#lbl_observaciones").html(Resultado[0].Observaciones);
                    }
                    else {
                        $("#lbl_observaciones").css('display', 'none');
                    }
                    _consultar_departamentos('#cmb_departamento_visitado');
                    _consultar_empleados1("#cmb_empleado_visitado");
                    _consultar_empleado("#cmb_empleado_visitado");

                } else { _mostrar_mensaje(Resultado.Titulo, Resultado.Mensaje); }
            }
        });
    } catch (e) {
        _mostrar_mensaje('Technical Report', e);
    }
}

function _modificar_solicitud_cita() {
    var Solicitud = null;
    try {
        Solicitud = new Object();
        Solicitud.No_Solicitud = parseInt($('#txt_no_solicitud').val())
        Solicitud.Empleado_ID = $('#cmb_empleado_visitado').val() == '' ? 0 : parseInt($('#cmb_empleado_visitado').val());
        if (isNaN(Solicitud.Empleado_ID)) {
            Solicitud.Empleado_ID = 0;
            Solicitud.Empleado_Visitado = $('#cmb_empleado_visitado').val();
        }
        Solicitud.Departamento_ID = $('#cmb_departamento_visitado').val() == '' ? 0 : parseInt($('#cmb_departamento_visitado').val());
        Solicitud.Estatus_ID = $('#cmb_estatus').val() == '' ? 0 : parseInt($('#cmb_estatus').val());
        Solicitud.Nombre_Solicitante = $('#nombre_solicitante').val();
        Solicitud.Apellidos = $('#apellidos_solicitante').val();
        Solicitud.Correo = $('#correo_solicitante').val();
        Solicitud.Telefono = $('#telefono_solicitante').val();
        Solicitud.Motivo_Cita = $('#txt_motivo_cita').val();

        Solicitud.Datos_Detalles_Horarios = JSON.stringify($('#tbl_lista_horarios').bootstrapTable('getData'));

        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(Solicitud) });

        $.ajax({
            type: 'POST',
            url: 'controllers/Ope_Solicitud_Citas_Controller.asmx/Actualizar',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function ($result) {
                var Resultado = JSON.parse($result.d);

                if (Resultado != null && Resultado != undefined && Resultado != '') {
                    if (Resultado.Estatus == 'success') {
                        _mostrar_mensaje(Resultado.Titulo, Resultado.Mensaje);
                        _inicializar_pagina();
                        _limpiar_controles();
                    } else if (Resultado.Estatus == 'error') {
                        _mostrar_mensaje(Resultado.Titulo, Resultado.Mensaje);
                    }
                } else { _mostrar_mensaje(Resultado.Titulo, Resultado.Mensaje); }
            }
        });


    } catch (ex) {
        _mostrar_mensaje("Informe T&eacute;cnico", ex);
    }
}

function parseDateIntercambiarDiaMes(dateString) {
    //Intercambia el dia y el mes de los formatos de fecha( DD/MM/YYYY o MM/DD/YYYY )
    var dateTime = dateString.split(" ");
    var dateOnly = dateTime[0];
    var dates = dateOnly.split("/");
    var temp = dates[1] + "/" + dates[0] + "/" + dates[2];
    return temp;
} 

function parsefecha(dateString) {
    var txtfecha = Date.parse(dateString).toString("dd/MM/yyyy")
    return txtfecha;
    
}
function _load_estatus() {
    var filtros = null;
    try {
        jQuery.ajax({
            type: 'POST',
            url: 'controllers/Ope_Solicitud_Citas_Controller.asmx/Consultar_Estatus',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            cache: false,
            success: function (datos) {
                if (datos !== null) {
                    var datos_combo = $.parseJSON(datos.d);
                    var select2 = $('#cmb_estatus');
                    $('option', select2).remove();
                    var options = '<option value="">--SELECCIONE--</option>';
                    for (var Indice_Estatus = 0; Indice_Estatus < datos_combo.length; Indice_Estatus++) {
                        options += '<option value="' + datos_combo[Indice_Estatus].Estatus_ID + '">' + datos_combo[Indice_Estatus].Estatus.toUpperCase() + '</option>';
                        if (datos_combo[Indice_Estatus].Estatus.toUpperCase() == 'ABIERTO') {
                            estatusActivo = datos_combo[Indice_Estatus].Estatus_ID;
                        }
                    }
                    select2.append(options);
                }
            }
        });
    } catch (e) {

    }
}
function parsehora(timeString) {
    if (timeString =="MM/dd/yyyy HH:MM:SS") {
        return timeString;
    }
    else
    var txtHora = Date.parse(timeString).toString("hh:mm tt")
    return txtHora;
}
function _crear_tbl_horarios() {
    try {
        $('#tbl_lista_horarios').bootstrapTable('destroy');
        $('#tbl_lista_horarios').bootstrapTable({
            idField: 'Horario_Solicitud_Detalle_ID',
            uniqueId: 'Horario_Solicitud_Detalle_ID',
            method: 'POST',
            async: false,
            cache: false,
            striped: true,
            pagination: true,
            pageSize: 10,
            pageList: [10, 25, 50, 100, 200],
            smartDysplay: false,
            search: false,
            showColumns: false,
            showRefresh: false,
            minimumCountColumns: 2,
            editable: true,
            showFooter: true,
            columns: [
                { field: 'Horario_Solicitud_Detalle_ID', title: '', align: 'center', valign: 'bottom', sortable: true, visible: false },
                {
                    field: 'Fecha_Cita', title: 'Fecha Cita',
                    width: '30%', align: 'center', sortable: true, formatter: function (value,
                    row) { return parseDateIntercambiarDiaMes(value); }
                },
                {
                    field: 'Hora_Inicio', title: 'Hora inicio', width: '30%', align: 'center', valign: 'bottom', sortable: true, visible: true,
                    //formatter: function (value,
                    //row) { return parsehora(value);}
                },
              {
                  field: 'Hora_Fin', title: 'Hora fin', width: '30%', align: 'center', valign: 'bottom', sortable: true, visible: true
                  //formatter: function (value,
                  //    row) { return parsehora(value);}
              },
                 {
                     field: 'eliminar', title: 'Quitar', align: 'center', valign: 'bottom', visible: true,formatter: function (value, row) {
                         return '<div><a class="remove ml10" id="' + value + '" href="javascript:void(0)"  data-horario=\'' +
                             JSON.stringify(row) + '\' onclick="_quitar_horario(this);"><i class="glyphicon glyphicon-remove"></i></a></div>';
                     }
                 },

            ],
            onLoadSuccess: function (data) {
            }
        });
    }
    catch (e) {
        _mostrar_mensaje('Error Técnico', 'Error al crear la tabla');
    }
}
function _crear_tbl_horarios_modificar() {
    try {
        $('#tbl_lista_horarios').bootstrapTable('destroy');
        $('#tbl_lista_horarios').bootstrapTable({
            idField: 'Horario_Solicitud_Detalle_ID',
            uniqueId: 'Horario_Solicitud_Detalle_ID',
            method: 'POST',
            async: false,
            cache: false,
            striped: true,
            pagination: true,
            pageSize: 10,
            pageList: [10, 25, 50, 100, 200],
            smartDysplay: false,
            search: false,
            showColumns: false,
            showRefresh: false,
            minimumCountColumns: 2,
            editable: true,
            showFooter: true,
            columns: [
                { field: 'Horario_Solicitud_Detalle_ID', title: '', align: 'center', valign: 'bottom', sortable: true, visible: false },
                {
                    field: 'Fecha_Cita', title: 'Fecha Cita',
                    width: '30%', align: 'center', sortable: true, formatter: function (value,
                    row) { return parseDateIntercambiarDiaMes(value); }
                },
                {
                    field: 'Hora_Inicio', title: 'Hora inicio', width: '30%', align: 'center', valign: 'bottom', sortable: true, visible: true,
                    formatter: function (value,
                    row) { return parsehora(value);}
                },
              {
                  field: 'Hora_Fin', title: 'Hora fin', width: '30%', align: 'center', valign: 'bottom', sortable: true, visible: true,
                  formatter: function (value,
                      row) { return parsehora(value);}
              },
                 {
                     field: 'eliminar', title: 'Quitar', align: 'center', valign: 'bottom', visible: true, formatter: function (value, row) {
                         return '<div><a class="remove ml10" id="' + value + '" href="javascript:void(0)"  data-horario=\'' +
                             JSON.stringify(row) + '\' onclick="_quitar_horario(this);"><i class="glyphicon glyphicon-remove"></i></a></div>';
                     }
                 },

            ],
            onLoadSuccess: function (data) {
            }
        });
    }
    catch (e) {
        _mostrar_mensaje('Error Técnico', 'Error al crear la tabla');
    }
}
function _agregar_lista_horarios() {
    try {
        var no_solicitud = $('#txt_no_solicitud').val();

        if (no_solicitud != "") {
            _guardar_horario();
            _consultar_horarios(no_solicitud);
        }
        else
            $('#tbl_lista_horarios').bootstrapTable('insertRow', {
                index: 0,
                row: {
                    Fecha_Cita:parseDateIntercambiarDiaMes($('#f_cita').val()),
                    Hora_Inicio: $('#h_inicio').val(),
                    Hora_Fin:$('#h_termino').val(),
                    No_Solicitud: $('txt_no_solicitud').val()
                }
        });
    } catch (e) {
       _mostrar_mensaje('Informe Técnico', e);
    }
}
function _quitar_horario(horario) {
    var indexrow = null;
    var row = $(horario).data('horario');
  
    try {
            var no_solicitud = $('#txt_no_solicitud').val();

            if (no_solicitud != "") {
                _eliminar_horario(row.Horario_Solicitud_Detalle_ID)
                _consultar_horarios(no_solicitud);
            }
            else {
                $('#tbl_lista_horarios').bootstrapTable('remove', {
                    field: 'Hora_Inicio',
                    field: 'Fecha_Cita',
                    values: [$.trim(row.Hora_Inicio.toString()), $.trim(row.Fecha_Cita.toString())]
                });
            }

     
    } catch (e) {
        _mostrar_mensaje('Informe Técnico', e);
    }
}
function _agregar_horarios() {
    try {
        var valida_datos_requerido =_validar_datos_requeridos_agregar_horario();
        if (valida_datos_requerido.Estatus) {

            if (!_validar_duplicar_horarios($('#h_inicio').val(),parseDateIntercambiarDiaMes($('#f_cita').val()))) {
                _agregar_lista_horarios();  
            } else {
                _mostrar_mensaje('Información', 'Ya existe un horario con la misma fecha y hora de incio.');
            }
        } else { _mostrar_mensaje('Información', valida_datos_requerido.Mensaje); }

    } catch (e) {
        _mostrar_mensaje('Informe Técnico', e);
    }

}
function _eliminar_horario(horario_id) {
    var horario = null;

    try {
        horario = new Object();
        horario.Horario_Solicitud_Detalle_ID = parseInt(horario_id);

        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(horario) });

        $.ajax({
            type: 'POST',
            url: 'controllers/Ope_Solicitud_Citas_Controller.asmx/Eliminar_Horario',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function (result) {
                var Resultado = JSON.parse(result.d);
                if (Resultado != null && Resultado != undefined && Resultado != '') {
                    if (Resultado.Estatus == 'success') {
                        _search();
                    } else if (Resultado.Estatus == 'error') {
                        _mostrar_mensaje(Resultado.Titulo, Resultado.Mensaje);
                    }
                } else { _mostrar_mensaje(Resultado.Titulo, Resultado.Mensaje); }
            }
        });

    } catch (e) {
        _mostrar_mensaje('Informe Técnico', e);
    }
}
function _guardar_horario() {
    var horario = null;
    try {
        horario = new Object();

        horario.No_Solicitud = parseInt($('#txt_no_solicitud').val());
        horario.Fecha_Cita = parseDateIntercambiarDiaMes($('#f_cita').val());
        horario.Hora_Inicio = $('#h_inicio').val();
        horario.Hora_Fin = $('#h_termino').val();
        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(horario) });

        $.ajax({
            type: 'POST',
            url: 'controllers/Ope_Solicitud_Citas_Controller.asmx/Alta_Horario',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function ($result) {
                var Resultado = JSON.parse($result.d);

                if (Resultado != null && Resultado != undefined && Resultado != '') {
                    if (Resultado.Estatus == 'success') {
                        _mostrar_mensaje(Resultado.Titulo, Resultado.Mensaje);
                    } else if (Resultado.Estatus == 'error') {
                        _mostrar_mensaje(Resultado.Titulo, Resultado.Mensaje);
                    }
                } else { _mostrar_mensaje(Resultado.Titulo, Resultado.Mensaje); }
            }
        });
        // _search_horarios(row.No_Solicitud);

    } catch (ex) {
        _mostrar_mensaje("Informe T&eacute;cnico", ex);
    }
}


function _validar_datos_requeridos_agregar_horario() {
    var _output = new Object();

    try {
        _output.Estatus = true;
        _output.Mensaje = '';

        if ($('#f_cita').val() == '' || $('#f_cita').val() == undefined) {
            _output.Estatus = false;
            _output.Mensaje += '&nbsp;<i class="fa fa-angle-double-right"></i>&nbsp;Seleccione una fecha para la cita.<br />';
        }

        if ($('#h_inicio').val() == '' || $('#h_inicio').val() == undefined) {
            _output.Estatus = false;
            _output.Mensaje += '&nbsp;<i class="fa fa-angle-double-right"></i>&nbsp;Seleccione una hora de inicio para la cita.<br />';
        }
        if ($('#h_termino').val() == '' || $('#h_termino').val() == undefined) {
            _output.Estatus = false;
            _output.Mensaje += '&nbsp;<i class="fa fa-angle-double-right"></i>&nbsp;Seleccione una hora fin para la cita.<br />';
        }
        if ($('#h_inicio').val() == $('#h_termino').val()) {
            _output.Estatus = false;
            _output.Mensaje += '&nbsp;<i class="fa fa-angle-double-right"></i>&nbsp;La hora de inicio de la cita debe ser diferente a la hora fin.<br />';
        }
        //if ($('#h_inicio').val() >= $('#h_termino').val()) {
        //    _output.Estatus = false;
        //    _output.Mensaje += '&nbsp;<i class="fa fa-angle-double-right"></i>&nbsp;La hora de inicio de la cita debe ser menor a la hora fin.<br />';
        //}
        if (_output.Mensaje != "")
            _output.Mensaje = "Favor de proporcionar lo siguiente: <br />" + _output.Mensaje;
    } catch (e) {
        _mostrar_mensaje('Informe técnico', e);
    } finally {
        return _output;
    }
}
function _validar_duplicar_horarios(hora_inicio,fecha_cita) {
    var _horario_agregados = null;
    var _horario_existentes_tabla = false;
    try {
        _horario_agregados = $("#tbl_lista_horarios").bootstrapTable('getData');
        $.each(_horario_agregados, function (index, value) {
            if (value.Hora_Inicio == hora_inicio && value.Fecha_Cita == fecha_cita) {
                _horario_existentes_tabla = true;
            }
               
        });
    } catch (e) {
        _mostrar_mensaje('Informe Técnico', e);
    }
    return _horario_existentes_tabla;
}

function _mostrar_mensaje(Titulo, Mensaje) {
    bootbox.dialog({
        message: Mensaje,
        title: Titulo,
        locale: 'es',
        closeButton: true,
        buttons: [{
            label: 'Cerrar',
            className: 'btn-default',
            callback: function () { }
        }]
    });
}

function _validar_datos() {
    var _output = new Object();

    try {
        _output.Estatus = true;
        _output.Mensaje = '';

       
        if ($('#cmb_estatus :selected').val() == '' || $('#cmb_estatus :selected').val() == undefined) {
            _output.Estatus = false;
            _output.Mensaje += '&nbsp;<i class="fa fa-angle-double-right"></i>&nbsp;El estatus es un dato requerido.<br />';
        }
        if ($('cmb_departamento_visitado :selected').val() == '' || $('#cmb_departamento_visitado :selected').val() == undefined) {
            _output.Estatus = false;
            _output.Mensaje += '&nbsp;<i class="fa fa-angle-double-right"></i>&nbsp;El departamento al que visitas es un campo requerido.<br />';
        }
        
        if ($("#nombre_solicitante").val() == null || $("#nombre_solicitante").val() == "") {
            _output.Estatus = false;
            _output.Mensaje += '&nbsp;<i class="fa fa-angle-double-right"></i>&nbsp;El nombre del solicitante es un campo requerido.<br />';
        }

        if ($('#apellidos_solicitante').val() == '' || $('#apellidos_solicitante').val() == null) {
            _output.Estatus = false;
            _output.Mensaje += '&nbsp;<i class="fa fa-angle-double-right"></i>&nbsp;Los apellidos del solicitante son un campo requerido.<br />';
        }

        if ($('#txt_motivo_cita').val() == '' || $('#txt_motivo_cita').val() == undefined) {
            _output.Estatus = false;
            _output.Mensaje += '&nbsp;<i class="fa fa-angle-double-right"></i>&nbsp;El motivo de la cita es un campo requerido.<br />';
        }

        if ($('#correo_solicitante').val() == '' || $('#correo_solicitante').val() == undefined) {
            _output.Estatus = false;
           _output.Mensaje += '&nbsp;<i class="fa fa-angle-double-right"></i>&nbsp;Debes ingresar tu correo electrónico.<br />';
       }
                
        if ($('#telefono_solicitante').val() == '' || $('#telefono_solicitante').val() == undefined) {
             _output.Estatus = false;
             _output.Mensaje += '&nbsp;<i class="fa fa-angle-double-right"></i>&nbsp;Debes ingresar tu teléfono<br />';
         }
                

        if ($table_horarios.bootstrapTable('getOptions').totalRows == 0) {
            _output.Estatus = false;
            _output.Mensaje += '&nbsp;<i class="fa fa-angle-double-right"></i>&nbsp;No se ha agregado ningún horario para la cita.<br />';
        }

        if (_output.Mensaje != "")
            _output.Mensaje = "Favor de proporcionar lo siguiente: <br />" + _output.Mensaje;
    } catch (e) {
        _mostrar_mensaje('Informe técnico', e);
    } finally {
        return _output;
    }
}

function btn_cancelar_click(renglon) {
    var row = $(renglon).data('solicitud_cita');
  if (row.Estatus != "ABIERTO") {
        _mostrar_mensaje('Validación', "<i class='fa fa-exclamation-circle' style = 'color:#f2041a;' ></ i > &nbsp;No puedes cancelar una solicitud con estatus: " +row.Estatus +", la solicitud debe estar con estatus ABIERTO");
        return;
    }
    bootbox.confirm({
        title: 'Cancelar Cita',
        message: '¿Estas seguro de cancelar la solicitud de la cita?',
        callback: function (result) {
            if (result) {
                bootbox.prompt('Describe la razón de la cancelación.', function (result) {
                    _actualizar_observaciones_solicitud(row.No_Solicitud, result);
                    _cancelar_solicitud_cita(row.No_Solicitud);
                    _search_solicitudes_por_filtros();
                    _mostrar_mensaje("Validación", "<i class='fa fa-check'style = 'color: #00A41E;' ></ i > &nbsp;La cita fue cancelada.");
                });
            }
        }
        //callback: function (result) {
        //    if (result) {
        //        //validar si es usuario
        //        //var usuario = _valida_usuarios();
        //        //if (usuario == true) {
        //        //    _mostrar_mensaje('Validation Report', 'Invalid user: unable to cancel the alert.');
        //        //    return;
        //        //}
        //        _cancelar_solicitud_cita(row.No_Solicitud);
        //        _mostrar_mensaje("Validación", "<i class='fa fa-check'style = 'color: #00A41E;' ></ i > &nbsp; La Solicitud fue cancelada exitosamente.");
        //    }
        //}
    });
}
function _cancelar_solicitud_cita(no_solicitud) {
    var Solicitud = null;
    var isComplete = false;
    try {
        Solicitud = new Object();
        Solicitud.No_Solicitud = parseInt(no_solicitud);

        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(Solicitud) });
        $.ajax({
            type: 'POST',
            url: 'controllers/Ope_Solicitud_Citas_Controller.asmx/Cancelar_Solicitud_Cita',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function ($result) {
                Resultado = JSON.parse($result.d);
                if (Resultado != null && Resultado != undefined && Resultado != '') {
                    if (Resultado.Estatus == 'success') {
                        validacion = true;
                    } else if (Resultado.Estatus == 'error') {
                        _mostrar_mensaje(Resultado.Titulo, Resultado.Mensaje);
                    }
                } else { _mostrar_mensaje("Error", "Unable to get results"); }
            }
        });
        //_search_alertas_rojas_por_filtros();
    } catch (e) {
        _mostrar_mensaje('Technical Report', e);
    }
    return isComplete;
}
function _actualizar_observaciones_solicitud(no_solicitud, result) {
    var solicitud;
    try {
        solicitud = new Object();
        solicitud.No_Solicitud = parseInt(no_solicitud);
        solicitud.Observaciones = result == null ? '' : result;

        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(solicitud) });
        $.ajax({
            type: 'POST',
            url: 'controllers/Ope_Solicitud_Citas_Controller.asmx/Actualizar_Observaciones',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function ($result) {
                Resultado = JSON.parse($result.d);
                if (Resultado != null && Resultado != undefined && Resultado != '') {
                    if (Resultado.Estatus == 'success') {
                        validacion = true;
                    } else if (Resultado.Estatus == 'error') {
                        _mostrar_mensaje(Resultado.Titulo, Resultado.Mensaje);
                    }
                } else { _mostrar_mensaje("Error", "Unable to get results"); }
            }
        });
        // _search_alertas_rojas_por_filtros();
    } catch (e) {
        _mostrar_mensaje('Technical Report', e);
    }
}

function btn_editar_click(renglon) {
    var row = $(renglon).data('solicitud_cita');
    if (row.Estatus != "ABIERTO") {
        _mostrar_mensaje('Validación', "<i class='fa fa-exclamation-circle' style = 'color:#f2041a;' ></ i > &nbsp;No puedes modificar una solicitud con estatus: " + row.Estatus + ", la solicitud debe estar con estatus ABIERTO");
        return;
    }
    $('#titulo').css('display','none');
    $('#titulo_modificar').css('display','block');
    _habilitar_controles('Modificar');
    _crear_tbl_horarios_modificar();
    _consultar_horarios(row.No_Solicitud);


   // var usuario = _validar_usuario();
   // if (usuario)
      //  $('#cmb_estatus').attr({ disabled: false });
   // else
       // $('#cmb_estatus').attr("disabled", true);
    var Solicitud= null;

    try {
      
        Solicitud = new Object();
        Solicitud.No_Solicitud = row.No_Solicitud;

        if (row.Departamento_ID != null)
            Solicitud.Departamento_ID = row.Departamento_ID;
        if (row.Empleado_ID != null)
            Solicitud.Empleado_ID = row.Empleado_ID;

        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(Solicitud) });
        $.ajax({
            type: 'POST',
            url: 'controllers/Ope_Solicitud_Citas_Controller.asmx/Consultar_Una_Solicitud',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function ($result) {
                var Resultado = JSON.parse($result.d);
                if (Resultado != null && Resultado != undefined && Resultado != '') {

                    $("#txt_no_solicitud").val(row.No_Solicitud);
                    // $('#txt_numero_parte').val(Resultado[0].Numero_Parte);

                    $("#cmb_estatus")[0].innerHTML = "";
                    $("#cmb_estatus").select2({ data: [{ id: row.Estatus_ID, text: row.Estatus }] });
                    $("#cmb_estatus").val(row.Estatus_ID).trigger("change");

                    $("#cmb_departamento_visitado")[0].innerHTML = "";
                    $("#cmb_departamento_visitado").select2({ data: [{ id: row.Departamento_ID, text: row.Departamento }] });
                    $("#cmb_departamento_visitado").val(row.Departamento_ID).trigger("change");

                    $("#cmb_empleado_visitado")[0].innerHTML = "";
                    $("#cmb_empleado_visitado").select2({ data: [{ id: row.Empleado_ID, text: row.Empleado_Visitado }] });
                    $("#cmb_empleado_visitado").val(row.Empleado_ID).trigger("change");


              

                    $('#nombre_solicitante').val(Resultado[0].Nombre_Solicitante);
                    $('#apellidos_solicitante').val(Resultado[0].Apellidos);
                    $('#correo_solicitante').val(Resultado[0].Correo);
             
                    $('#telefono_solicitante').val(Resultado[0].Telefono);
                    $('#txt_motivo_cita').val(Resultado[0].Motivo_Cita);
                    ////if (row.Estatus == "CANCELED") {
                    ////    $('#btn_guardar').css('display', 'none');
                    ////    $('#btn_salir').css('display', 'none');
                    ////    $('#btn_regresar').css('display', 'block');
                    ////}
                    ////if (row.Estatus == "CANCELED" || row.Estatus == "REJECTED") {
                    ////    $("#lbl_observaciones").css('display', 'block');
                    ////    $("#lbl_observaciones").html(Resultado[0].Observaciones);
                    ////}
                    ////else {
                    ////    $("#lbl_observaciones").css('display', 'none');
                    ////}

                   // _load_estatus('#cmb_estatus');
                    _consultar_departamentos('#cmb_departamento_visitado');
                    //_consultar_empleados1("#cmb_empleado_visitado");
                   // _consultar_empleado("#cmb_empleado_visitado");

                } else { _mostrar_mensaje(Resultado.Titulo, Resultado.Mensaje); }
            }
        });
    } catch (e) {
        _mostrar_mensaje('Technical Report', e);
    }
}
function _consultar_horarios(no_solicitud) {
    var filtros = null;
    try {

        filtros = new Object();
        filtros.No_Solicitud = parseInt(no_solicitud);
       
        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(filtros) });

        jQuery.ajax({
            type: 'POST',
            url: 'controllers/Ope_Solicitud_Citas_Controller.asmx/Consultar_Horarios',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function (datos) {
                if (datos !== null) {
                    $('#tbl_lista_horarios').bootstrapTable('load', JSON.parse(datos.d));
                }
            }
        });
    } catch (e) {

    }
}
function _solo_numeros(e) {
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = "0123456789";
    especiales = [8];

    tecla_especial = false
    for (var i in especiales) {
        if (key == especiales[i]) {
            tecla_especial = true;
            break;
        }
    }
    if (letras.indexOf(tecla) == -1 && !tecla_especial)
        return false;
}
function _search_solicitudes_por_filtros() {
    var filtros = null;
    try {
        filtros = new Object();

        if ($.trim($('#txt_busqueda_no_solicitud').val()) !== '')
            filtros.No_Solicitud = parseInt($('#txt_busqueda_no_solicitud').val());

        if ($.trim($('#txt_busqueda_solicitante').val()) !== '')
            filtros.Nombre_Solicitante = $('#txt_busqueda_solicitante').val();

        if ($.trim($('#txt_busqueda_apellidos').val()) !== '')
            filtros.Apellidos = $('#txt_busqueda_apellidos').val();

        if ($.trim($('#f_busqueda').val()) !== '')
            filtros.Fecha_Cita = $('#f_busqueda').val();
       // if ($('#cmb_busqueda_por_estatus :selected').val() !== undefined && $('#cmb_busqueda_por_estatus').val() !== '')
          //  filtros.Estatus_ID = parseInt($('#cmb_busqueda_por_estatus :selected').val());



        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(filtros) });
        jQuery.ajax({
            type: 'POST',
            url: 'controllers/Ope_Solicitud_Citas_Controller.asmx/Consultar_Solicitud',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function (datos) {
                if (datos !== null) {
                    $('#tbl_solicitud_citas').bootstrapTable('load', JSON.parse(datos.d));
                    //_agregar_tooltip_tabla();
                    datos.d = (datos.d == undefined || datos.d == null) ? '[]' : datos.d;
                    //if (JSON.parse(datos.d).length > 0)
                    //    $("#ctrl_panel").click();
                }
            }
        });
    } catch (e) {
        _mostrar_mensaje('Technical Report', e);
    }
}
function _validar_horario(no_solicitud) {
    var filtros = null;
    var datos_cb = null;
    try {
        filtros = new Object();
        filtros.No_Solicitud = parseInt(no_solicitud);
        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(filtros) });
        jQuery.ajax({
            type: 'POST',
            url: 'controllers/Ope_Solicitud_Citas.asmx/Validar_Horario',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function (datos) {
                if (datos !== null) {
                    datos_cb = JSON.parse(datos.d);
                }
            }
        });
        return datos_cb.Mensaje;
    } catch (e) {

    }
}
