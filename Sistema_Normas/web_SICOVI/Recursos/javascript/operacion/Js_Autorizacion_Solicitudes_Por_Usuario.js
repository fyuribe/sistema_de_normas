﻿
var estatusActivo = '';
var $table_horarios = null;
var $table = null;

$(document).on('ready', function () {
    _inicializar_pagina();
    $table_horarios = $('#tbl_lista_horarios');
    _limpiar_controles();
    _set_location_toolbar();
    _eventos_textbox();
    _eventos();
});

function _habilitar_controles(opcion) {
    $('#div_principal_solicitud_citas').css('display', 'none');
    $('#div_Informacion').css('display', 'none');
    switch (opcion) {
        case 'Inicial':
            $('#div_principal_solicitud_citas').css('display', 'block');
            break;
        case 'Nuevo':
            $('#div_Informacion').css('display', 'block');
            $('#tbl_avisos_coordinacion').bootstrapTable('hideColumn', 'Modificar');
            break;
        case 'Modificar':
            $('#div_Informacion').css('display', 'block');
            // $('#tbl_avisos_coordinacion').bootstrapTable('hideColumn', 'Modificar');
            $('#cmb_estatus').attr("disabled", true);
            break;
        case 'Autorizar':
           
            //$('#cmb_empleado_visitado').attr("disabled",true);
            $('#cmb_departamento_visitado').attr("disabled",true);
            $('#cmb_estatus').attr("disabled", true);
            $('#div_Informacion').css('display', 'block');
            $('#nombre_solicitante').attr("disabled", true);
            $('#apellidos_solicitante').attr("disabled", true);
            $('#correo_solicitante').attr("disabled", true);
            $('#telefono_solicitante').attr("disabled", true);
            $('#txt_motivo_cita').attr("disabled", true);
          
            break;
        case 'Visualizar':
            $('#cmb_empleado_visitado').attr("disabled",true);
            $('#cmb_departamento_visitado').attr("disabled", true);
            $('#cmb_estatus').attr("disabled", true);
            $('#div_Informacion').css('display', 'block');
            $('#nombre_solicitante').attr("disabled", true);
            $('#apellidos_solicitante').attr("disabled", true);
            $('#correo_solicitante').attr("disabled", true);
            $('#telefono_solicitante').attr("disabled", true);
            $('#txt_motivo_cita').attr("disabled", true);
            $("#btn_confirmar").css('display', 'none');
            $("#btn_rechazar").css('display', 'none');
            $('#btn_reasignar_cita').css('display','none')
            break;
    }
}
function _inicializar_pagina() {
    try {
        _estado_inicial();
        _habilitar_controles('Inicial');
        _crear_tbl_solicitud_departamento();
        _crear_tbl_solicitud_citas();
        _crear_tbl_horarios();
        _consultar_empleado();
        _load_estatus();
        _set_location_toolbar();
        _search_solicitud_por_usuario();
        _search_solicitud_por_departamento();

    } catch (e) {
        _mostrar_mensaje('Informe Técnico', e);
    }
}
function _eventos() {
    try {
        $('#modal_datos').on('hidden.bs.modal', function () {
            if (!closeModal)
                $(this).modal('show');
        });
        $('#btn_rechazar').on('click', function (e) {
            btn_rechazar_cita();
        });
      
        $('#btn_busqueda').on('click', function (e) {
           // _search_solicitudes_por_filtros();
        });
        $('#btn_salir').on('click', function (e) {

            $('#div_Informacion').css('display', 'none');
            $('#div_principal_solicitud_citas').css('display', 'block');
            _limpiar_controles();
        });
        $('#btn_confirmar').on('click', function (e) {
            $('#cmb_empleado_visitado').on('change', function () {
                var no_solicitud = $('#txt_no_solicitud').val();
                _actualizar_horarios(no_solicitud);
                _btn_reasignar_cita();
            });
              
           _btn_autorizar_cita();
        });
        $('#btn_asignar_empleado').on('click', function (e) {
            _btn_asignar_empleado();
        });
      
        $('#btn_regresar').on('click', function (e) { e.preventDefault(); window.location.href = '../Operacion/Frm_Ope_Autorizacion_Solicitud_Citas.aspx'; });
        $('#btn_regresa').on('click', function (e) { e.preventDefault(); window.location.href = '../Operacion/Frm_Ope_Autorizacion_Solicitud_Citas.aspx'; });
        $('#btn_agregar_horario').on('click', function (e) {
            e.preventDefault();
            //_agregar_lista_horarios();
            _agregar_horarios();
            _limpiar_controles();
        });

        $('#modal_datos input[type=text]').each(function (index, element) {
            $(this).on('focus', function () {
                _remove_class_error('#' + $(this).attr('id'));
            });
        });
        $('#modal_datos select').each(function (index, element) {
            $(this).on('focus', function () {
                _remove_class_error('#' + $(this).attr('id'));
            });
        });
        $('#cmb_empleado_visitado').on("select2:select", function (evt) {
            var text = evt.params.data.text;
            _btn_reasignar_cita();

        });
         
    } catch (e) {
        _mostrar_mensaje('Informe Técnico', e);
    }
}
function _set_location_toolbar() {
    $('#toolbar').parent().removeClass("pull-left");
    $('#toolbar').parent().addClass("pull-right");
}
function _estado_inicial() {
    try {
        $('#dtp_fecha_cita').datetimepicker({
            defaultDate: new Date(),
            viewMode: 'days',
            locale: 'es',
            format: "DD/MM/YYYY"
        });
        $("#dtp_fecha_cita").datetimepicker("useCurrent", true);

        $('#dtp_fecha_busqueda').datetimepicker({
            defaultDate: new Date(),
            viewMode: 'days',
            locale: 'es',
            format: "DD/MM/YYYY"
        });
        $("#dtp_fecha_busqueda").datetimepicker("useCurrent", true);

        $('#dtp_hora_inicio').datetimepicker({
            format: "LT"
            //format: "hh:mm"
        });
        $('#dtp_hora_inicio').datetimepicker("useCurrent", true);

        $('#dtp_hora_fin').datetimepicker({
            format: "LT"
            //format: "hh:mm"
        });
        $("#dtp_hora_fin").datetimepicker("useCurrent", true);

    } catch (ex) {
        _mostrar_mensaje("Informe T&eacute;nico", ex.message)
    }
}
function _mostrar_mensaje(Titulo, Mensaje) {
    bootbox.dialog({
        message: Mensaje,
        title: Titulo,
        locale: 'es',
        closeButton: true,
        buttons: [{
            label: 'Cerrar',
            className: 'btn-default',
            callback: function () { }
        }]
    });
}
function _validar_datos() {
    var _output = new Object();

    try {
        _output.Estatus = true;
        _output.Mensaje = '';


        if ($('#cmb_estatus :selected').val() == '' || $('#cmb_estatus :selected').val() == undefined) {
            _output.Estatus = false;
            _output.Mensaje += '&nbsp;<i class="fa fa-angle-double-right"></i>&nbsp;El estatus es un dato requerido.<br />';
        }
        if ($('cmb_departamento_visitado :selected').val() == '' || $('#cmb_departamento_visitado :selected').val() == undefined) {
            _output.Estatus = false;
            _output.Mensaje += '&nbsp;<i class="fa fa-angle-double-right"></i>&nbsp;El departamento al que visitas es un campo requerido.<br />';
        }

        if ($("#nombre_solicitante").val() == null || $("#nombre_solicitante").val() == "") {
            _output.Estatus = false;
            _output.Mensaje += '&nbsp;<i class="fa fa-angle-double-right"></i>&nbsp;El nombre del solicitante es un campo requerido.<br />';
        }

        if ($('#apellidos_solicitante').val() == '' || $('#apellidos_solicitante').val() == undefined) {
            _output.Estatus = false;
            _output.Mensaje += '&nbsp;<i class="fa fa-angle-double-right"></i>&nbsp;Los apellidos del solicitante son un campo requerido.<br />';
        }

        if ($('#txt_motivo_cita').val() == '' || $('#txt_motivo_cita').val() == undefined) {
            _output.Estatus = false;
            _output.Mensaje += '&nbsp;<i class="fa fa-angle-double-right"></i>&nbsp;El motivo de la cita es un campo requerido.<br />';
        }

        if ($('#correo_solicitante').val() == '' || $('#correo_solicitante').val() == undefined) {
            _output.Estatus = false;
            _output.Mensaje += '&nbsp;<i class="fa fa-angle-double-right"></i>&nbsp;Debes ingresar tu correo electrónico.<br />';
        }

        if ($('#telefono_solicitante').val() == '' || $('#telefono_solicitante').val() == undefined) {
            _output.Estatus = false;
            _output.Mensaje += '&nbsp;<i class="fa fa-angle-double-right"></i>&nbsp;Debes ingresar tu teléfono<br />';
        }


        if ($table_horarios.bootstrapTable('getOptions').totalRows == 0) {
            _output.Estatus = false;
            _output.Mensaje += '&nbsp;<i class="fa fa-angle-double-right"></i>&nbsp;No se ha agregado ningún horario para la cita.<br />';
        }

        if (_output.Mensaje != "")
            _output.Mensaje = "Favor de proporcionar lo siguiente: <br />" + _output.Mensaje;
    } catch (e) {
        _mostrar_mensaje('Informe técnico', e);
    } finally {
        return _output;
    }
}
function _eventos_textbox() {
    $('#nombre_solicitante').on('blur', function () {
        $(this).val($(this).val().match(/^[a-z A-z]{2,16}$/) ? $(this).val() : '');
    });
    $('#apellidos_solicitante').on('blur', function () {
        $(this).val($(this).val().match(/^[a-z A-z]{2,16}$/) ? $(this).val() : '');
    });
    $('#correo_solicitante').on('blur', function () {
        $(this).val($(this).val().match(/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i) ? $(this).val() : '');
    });


}
function _limpiar_controles() {
    $('#f_cita').val('');
    $('#h_inicio').val('');
    $('#h_termino').val('');
    _validation_sumary(null);
}
function _validation_sumary(validation) {
    var header_message = '<i class="fa fa-exclamation-triangle fa-2x"></i><span>Observations</span><br />';
    if (validation == null) {
        $('#lbl_msg_error').html('');
        $('#sumary_error').css('display', 'none');
    } else {
        $('#lbl_msg_error').html(header_message + validation.Mensaje);
        $('#sumary_error').css('display', 'block');
    }
}
function _consultar_departamento1(cmb) {
    try {
        $(cmb).select2({
            language: "es",
            theme: "classic",
            placeholder: 'Buscar Departamento',
            allowClear: true,
            tags: true

        });

    } catch (e) {
        _mostrar_mensaje('Informe Técnico', e);
    }

}
function _consultar_departamento(cmb) {
    try {
        $(cmb).select2({
            language: "es",
            theme: "classic",
            placeholder: 'Buscar Departamento',
            allowClear: true,
            ajax: {
                url: 'controllers/Ope_Solicitud_Citas_Controller.asmx/Consultar_Departamentos',
                cache: "true",
                dataType: 'json',
                type: "POST",
                delay: 250,
                cache: true,
                params: {
                    contentType: 'application/json; charset=utf-8'
                },
                quietMillis: 100,
                results: function (data) {
                    return { results: data };
                },
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page
                    };
                },
                processResults: function (data, page) {
                    return {
                        results: data
                    };
                },
            }
        });

    } catch (e) {
        _mostrar_mensaje('Informe Técnico', e);
    }
}
function _consultar_datos_empleado(cmb) {

    try {
        $(cmb).select2({
            language: "es",
            theme: "classic",
            placeholder: 'Buscar Empleado',
            allowClear: true,
            ajax: {
                url: 'controllers/Ope_Solicitud_Citas_Controller.asmx/Consultar_Empleados',
                cache: "true",
                dataType: 'json',
                type: "POST",
                delay: 250,
                cache: true,
                params: {
                    contentType: 'application/json; charset=utf-8'
                },
                quietMillis: 100,
                results: function (data) {
                    return { results: data };
                },
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page
                    };
                },
                processResults: function (data, page) {
                    return {
                        results: data
                    };
                },
            }
        });

        $('#cmb_empleado_visitado').on("select2:select", function (evt) {
            var id = evt.params.data.id;

            if ($('#cmb_empleado_visitado:selected').val() !== '' || $('#cmb_empleado_visitado :selected').val() !== undefined) {
                var Empleado = new Object();
                Empleado.Empleado_ID = parseInt($('#cmb_empleado_visitado :selected').val());
                //$('#txt_email_contratista').val('');

                var $data = JSON.stringify({ 'jsonObject': JSON.stringify(Empleado) });

                $.ajax({
                    type: 'POST',
                    url: 'controllers/Ope_Solicitud_Citas_Controller.asmx/Consultar_Datos_Empleados',
                    cache: "true",
                    data: $data,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: false,
                    cache: true,
                    tags: true,
                    success: function (datos) {

                        if (datos !== null && datos !== undefined) {
                            datos_empleado = JSON.parse(datos.d);
                            $("#cmb_departamento_visitado").empty().trigger("change");

                            for (i = 0; i <= datos_empleado.length; i++) {
                                $('#cmb_departamento_visitado').val(datos_empleado[i].Departamento_ID).trigger("change");

                                $('#cmb_departamento_visitado').select2({ tags: true, data: [{ id: datos_empleado[i].Departamento_ID, text: datos_empleado[i].Departamento }] });

                            }

                        }

                    }

                });

            }
        });


    } catch (e) {
        _mostrar_mensaje('Informe Técnico', e);
    }



}
function _consultar_departamentos(cmb) {

    try {
        $(cmb).select2({
            language: "es",
            theme: "classic",
            placeholder: 'Buscar Departamento',
            allowClear: true,
            ajax: {
                url: 'controllers/Ope_Solicitud_Citas_Controller.asmx/Consultar_Departamentos',
                cache: "true",
                dataType: 'json',
                type: "POST",
                delay: 250,
                cache: true,
                params: {
                    contentType: 'application/json; charset=utf-8'
                },
                quietMillis: 100,
                results: function (data) {
                    return { results: data };
                },
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page
                    };
                },
                processResults: function (data, page) {
                    return {
                        results: data
                    };
                },
            }
        });

        $('#cmb_departamento_visitado').on("select2:select", function (evt) {
            var id = evt.params.data.id;

            if ($('#cmb_departamento_visitado:selected').val() !== '' || $('#cmb_departamento_visitado :selected').val() !== undefined) {
                var Departamento = new Object();
                Departamento.Departamento_ID = parseInt($('#cmb_departamento_visitado :selected').val());
                //$('#txt_email_contratista').val('');

                var $data = JSON.stringify({ 'jsonObject': JSON.stringify(Departamento) });

                $.ajax({
                    type: 'POST',
                    url: 'controllers/Ope_Solicitud_Citas_Controller.asmx/Consultar_Datos_Departamentos',
                    cache: "true",
                    data: $data,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: false,
                    cache: true,
                    tags: true,
                    success: function (datos) {

                        if (datos !== null && datos !== undefined) {
                            datos_empleado = JSON.parse(datos.d);
                            $("#cmb_empleado_visitado").empty().trigger("change");

                            for (i = 0; i <= datos_empleado.length; i++) {
                                $('#cmb_empleado_visitado').val(datos_empleado[i].Empleado_ID).trigger("change");

                                $('#cmb_empleado_visitado').select2({ tags: true, data: [{ id: datos_empleado[i].Empleado_ID, text: datos_empleado[i].Empleado }] });

                            }

                        }

                    }

                });

            }
        });


    } catch (e) {
        _mostrar_mensaje('Informe Técnico', e);
    }



}
function _consultar_empleados1(cmb) {
    try {
        $(cmb).select2({
            language: "es",
            theme: "classic",
            placeholder: 'Buscar Empleado',
            allowClear: true,
            tags: true

        });

    } catch (e) {
        _mostrar_mensaje('Informe Técnico', e);
    }

}
function _consultar_empleado(cmb) {
    try {
        $(cmb).select2({
            language: "es",
            theme: "classic",
            placeholder: 'Buscar empleado',
            allowClear: true,
            ajax: {
                url: 'controllers/Ope_Autorizacion_Solicitud_Controller.asmx/Consultar_Empleados',
                cache: "true",
                dataType: 'json',
                type: "POST",
                delay: 250,
                cache: true,
                params: {
                    contentType: 'application/json; charset=utf-8'
                },
                quietMillis: 100,
                results: function (data) {
                    return { results: data };
                },
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page
                    };
                },
                processResults: function (data, page) {
                    return {
                        results: data
                    };
                },
            }
        });

    } catch (e) {
        _mostrar_mensaje('Informe Técnico', e);
    }
}
function _search_solicitud_por_usuario() {
    var filtros = null;
    try {
        filtros = new Object();

        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(filtros) });
        jQuery.ajax({
            type: 'POST',
            url: 'controllers/Ope_Autorizacion_Solicitud_Controller.asmx/Consultar_Solicitudes_Por_Usuario',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function (datos) {
                if (datos !== null && datos.d !== '') {
                    $('#tbl_solicitud').bootstrapTable('load', JSON.parse(datos.d));
                   // _agregar_tooltip_tabla();
                    datos.d = (datos.d == undefined || datos.d == null) ? '[]' : datos.d;
                    //if (JSON.parse(datos.d).length > 0)
                    //    $("#ctrl_panel").click();
                }
            }
        });
    } catch (e) {
        _mostrar_mensaje('Technical Report', e);
    }
}
function _crear_tbl_solicitud_citas() {
    try {
        $('#tbl_solicitud').bootstrapTable('destroy');
        $('#tbl_solicitud').bootstrapTable({
            cache: false,
            striped: true,
            pagination: true,
            pageSize: 10,
            pageList: [10, 25, 50, 100, 200],
            smartDysplay: false,
            search: true,
            showColumns: false,
            showRefresh: false,
            minimumCountColumns: 2,
            clickToSelect: false,

            columns: [

                { field: 'No_Solicitud', title: 'No. Solicitud', width: '10%', align: 'left', valign: 'bottom', sortable: true, clickToSelect: false },
                { field: 'Empleado_Visitado', title: 'Empleado Visitado', width: '15%', align: 'left', valign: 'bottom', sortable: true, clickToSelect: false },
                { field: 'Nombre_Solicitante', title: 'Nombre Solicitante', width: '15%', align: 'left', valign: 'bottom', sortable: true, clickToSelect: false },
                { field: 'Departamento', title: 'Departamento', width: '15%', align: 'left', valign: 'bottom', sortable: true, clickToSelect: false },
             
                {
                    field: 'Estatus', title: 'Estatus', width: '10%', align: 'left', valign: '', halign: 'left', sortable: true, clickToSelect: false, formatter: function (value, row) {

                        var color = '';
                        switch (value) {
                            case 'CANCELADO':
                                color = "black";
                                break;
                            case 'ABIERTO':
                                color = "yellow";
                                break;
                            case 'PROCESO':
                                color = 'rgba(85, 171, 237, 1)';
                                break;
                            case 'AUTORIZADO':
                                color = 'rgba(85, 255, 51, 1)';
                                break;
                            case 'VENCIDO':
                                color = "orange";
                                break;
                            case 'RECHAZADO':
                                color = "red";
                                break;
                            case 'VALIDADO':
                                color = "orange";
                                break;
                            case 'FINALIZADO':
                                //color = "rgba(221, 24, 24, 1)";
                                color = "green";
                                break;
                            default:
                                color = 'rgba(85, 171, 237, 1)';
                        }

                        return '<i class="fa fa-circle" style="color: ' + color + '"></i>&nbsp;' + value;
                    }
                },
                 {
                     field: 'Autorizar', title: 'Autorizar', align: 'center', width: '2%',
                     formatter: function (value, row) {
                         return '<div>' +
                                   '<a class="remove ml10 edit _editar_cambio" id="remove_' + row.No_Solicitud + '" href="javascript:void(0)" data-solicitud_cita=\'' + JSON.stringify(row) + '\' onclick="btn_editar_click(this);"><i class="glyphicon glyphicon-ok"  title=""></i></a>' +
                                '</div>';
                     }
                 },
                {
                    field: 'Visualizar', title: 'Visualizar', align: 'center', width: '2%',
                    formatter: function (value, row) {
                        return '<div>' +
                                    '<a class="remove ml10 edit _ver_orden_cambio" id="remove_' + row.No_Solicitud + '" href="javascript:void(0)" data-solicitud_cita=\'' + JSON.stringify(row) + '\' onclick="btn_visualizar_click(this);"><i class="glyphicon glyphicon-eye-open" title=""></i></a>' +
                                 '</div>';
                    }

                }

            ]
        });
    } catch (e) {

    }
}
function _search_solicitud_por_departamento() {
    var filtros = null;
    try {
        filtros = new Object();

        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(filtros) });
        jQuery.ajax({
            type: 'POST',
            url: 'controllers/Ope_Autorizacion_Solicitud_Controller.asmx/Consultar_Solicitudes_Por_Departamento',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function (datos) {
                if (datos !== null && datos.d !== '') {
                    $('#tbl_solicitud_departamento').bootstrapTable('load', JSON.parse(datos.d));
                    // _agregar_tooltip_tabla();
                    datos.d = (datos.d == undefined || datos.d == null) ? '[]' : datos.d;
                    //if (JSON.parse(datos.d).length > 0)
                    //    $("#ctrl_panel").click();
                }
            }
        });
    } catch (e) {
        _mostrar_mensaje('Technical Report', e);
    }
}
function _crear_tbl_solicitud_departamento() {
    try {
        $('#tbl_solicitud_departamento').bootstrapTable('destroy');
        $('#tbl_solicitud_departamento').bootstrapTable({
            cache: false,
            striped: true,
            pagination: true,
            pageSize: 10,
            pageList: [10, 25, 50, 100, 200],
            smartDysplay: false,
            search: true,
            showColumns: false,
            showRefresh: false,
            minimumCountColumns: 2,
            clickToSelect: false,

            columns: [

                { field: 'No_Solicitud', title: 'No. Solicitud', width: '10%', align: 'left', valign: 'bottom', sortable: true, clickToSelect: false },
                { field: 'Nombre_Solicitante', title: 'Nombre Solicitante', width: '15%', align: 'left', valign: 'bottom', sortable: true, clickToSelect: false },
                { field: 'Departamento', title: 'Departamento', width: '15%', align: 'left', valign: 'bottom', sortable: true, clickToSelect: false },
                 //{
                 //    field: 'Fecha_Cita', title: 'Fecha cita',
                 //    width: '10%', align: 'center', sortable: true, formatter: function (value,
                 //    row) { return parsefecha(value); }
                 //},
                {
                    field: 'Estatus', title: 'Estatus', width: '10%', align: 'left', valign: '', halign: 'left', sortable: true, clickToSelect: false, formatter: function (value, row) {

                        var color = '';
                        switch (value) {
                            case 'CANCELADO':
                                color = "black";
                                break;
                            case 'ABIERTO':
                                color = "yellow";
                                break;
                            case 'PROCESO':
                                color = 'rgba(85, 171, 237, 1)';
                                break;
                            case 'VENCIDO':
                                color = "orange";
                                break;
                            case 'RECHAZADO':
                                color = "red";
                                break;
                            case 'VALIDADO':
                                color = "orange";
                                break;
                            case 'FINALIZADO':
                                //color = "rgba(221, 24, 24, 1)";
                                color = "green";
                                break;
                            default:
                                color = 'rgba(85, 171, 237, 1)';
                        }

                        return '<i class="fa fa-circle" style="color: ' + color + '"></i>&nbsp;' + value;
                    }
                },

                 {
                     field: 'Autorizar', title: 'Asignar empleado', align: 'center', width: '2%',
                     formatter: function (value, row) {
                         return '<div>' +
                                   '<a class="remove ml10 edit _editar_cambio" id="remove_' + row.No_Solicitud + '" href="javascript:void(0)" data-solicitud_cita=\'' + JSON.stringify(row) + '\' onclick="btn_editar_por_departamento_click(this);"><i class="glyphicon glyphicon-ok"  title=""></i></a>' +
                                '</div>';
                     }
                 },
         
                {
                    field: 'Visualizar', title: 'Visualizar', align: 'center', width: '2%',
                    formatter: function (value, row) {
                        return '<div>' +
                                    '<a class="remove ml10 edit _ver_orden_cambio" id="remove_' + row.No_Solicitud + '" href="javascript:void(0)" data-solicitud_cita=\'' + JSON.stringify(row) + '\' onclick="btn_visualizar_click(this);"><i class="glyphicon glyphicon-eye-open" title=""></i></a>' +
                                 '</div>';
                    }

                }

            ]
        });
    } catch (e) {

    }
}
function _crear_tbl_horarios() {
    try {
        $('#tbl_lista_horarios').bootstrapTable('destroy');
        $('#tbl_lista_horarios').bootstrapTable({
            idField: 'Horario_Solicitud_Detalle_ID',
            uniqueId: 'Horario_Solicitud_Detalle_ID',
            method: 'POST',
            async: false,
            cache: false,
            striped: true,
            pagination: true,
            pageSize: 10,
            pageList: [10, 25, 50, 100, 200],
            smartDysplay: false,
            search: false,
            showColumns: false,
            showRefresh: false,
            minimumCountColumns: 2,
            editable: true,
            showFooter: true,
            columns: [
                { field: 'Horario_Solicitud_Detalle_ID', title: '', align: 'center', valign: 'bottom', sortable: true, visible: false },
                {
                    field: 'Fecha_Cita', title: 'Fecha Cita',
                    width: '30%', align: 'center', sortable: true, formatter: function (value,
                    row) { return parseDateIntercambiarDiaMes(value); }
                },
                {
                    field: 'Hora_Inicio', title: 'Hora inicio', width: '30%', align: 'center', valign: 'bottom', sortable: true, visible: true, formatter: function (value,
                    row) { return parsehora(value); }
                },
              {
                  field: 'Hora_Fin', title: 'Hora fin', width: '30%', align: 'center', valign: 'bottom', sortable: true, visible: true, formatter: function (value,
                      row) { return parsehora(value); }
              },
               {
                 field: 'Aprobado', title: 'Autorizado', width: '10%', align: 'center', valign: '', halign: 'left', sortable: true, clickToSelect: false, formatter: function (value, row) {
                   var color = '';
                   switch (value) {
                     case 'NO':
                       color = "rgba(221, 24, 24, 1)";
                     break;
                     case 'PENDIENTE':
                       color = "yellow";
                     break;
                     case 'SI':
                       color = 'rgba(85, 255, 51, 1)';
                     break;
                     default:
                       color = 'rgba(85, 171, 237, 1)';
                    }
                    return '<i class="fa fa-circle" style="color: ' + color + '"></i>&nbsp;' + value;
                  }
                },
               {
                   field: 'Autorizar', title: 'Autorizar', align: 'center', width: '2%',
                     formatter: function (value, row) {
                        return '<div>' +
                            //'<a class="remove ml10 edit _editar_cambio" id="remove_' + row.No_Solicitud + '" href="javascript:void(0)" data-horario=\'' + JSON.stringify(row) + '\' onclick="btn_autorizar_click(this);"><i class="glyphicon glyphicon-ok"  title=""></i></a>' +
                             '<input type="checkbox" name="cb_autorizar" id="check_' + row.Horario_Solicitud_Detalle_ID + '" href="javascript:void(0)" data-horario=\'' + JSON.stringify(row) + '\' onclick="btn_autorizar_click(this);"/>' +
                             '</div>';
                        }
               },
            ],
            onLoadSuccess: function (data) {
            }
        });
    }
    catch (e) {
        _mostrar_mensaje('Error Técnico', 'Error al crear la tabla');
    }
}
function _agregar_lista_horarios() {
    try {
        $('#tbl_lista_horarios').bootstrapTable('insertRow', {
            index: 0,
            row: {
                Fecha_Cita: parseDateIntercambiarDiaMes($('#f_cita').val()),
                Hora_Inicio: $('#h_inicio').val(),
                Hora_Fin: $('#h_termino').val(),
                Aprobado: "PENDIENTE",
                No_Solicitud: $('txt_no_solicitud').val(),

            }
        });
    } catch (e) {
        _mostrar_mensaje('Informe Técnico', e);
    }
}
function _agregar_horarios() {
    try {
        
        var no_solicitud = $('#txt_no_solicitud').val();
     
        var valida_datos_requerido = _validar_datos_requeridos_agregar_horario();
        var horarios = _validar_horario_rechazado(no_solicitud)
        if (horarios == "False")
            _mostrar_mensaje('Validación', 'Debes rechazar los horarios de la lista para poder agregar una nueva propuesta de horario.');
        else

        if (valida_datos_requerido.Estatus) {

            if (!_validar_duplicar_horarios($('#h_inicio').val(), parseDateIntercambiarDiaMes($('#f_cita').val()))) {

                _agregar_lista_horarios();
                _guardar_horario();
                _consultar_horarios(no_solicitud);

            } else {
                _mostrar_mensaje('Información', 'Ya existe un horario con la misma fecha y hora de incio.');
            }
        } else { _mostrar_mensaje('Información', valida_datos_requerido.Mensaje); }

    } catch (e) {
        _mostrar_mensaje('Informe Técnico', e);
    }

}
function _validar_datos_requeridos_agregar_horario() {
    var _output = new Object();

    try {
       
        _output.Estatus = true;
        _output.Mensaje = '';

        if ($('#f_cita').val() == '' || $('#f_cita').val() == undefined) {
            _output.Estatus = false;
            _output.Mensaje += '&nbsp;<i class="fa fa-angle-double-right"></i>&nbsp;Seleccione una fecha para la cita.<br />';
        }

        if ($('#h_inicio').val() == '' || $('#h_inicio').val() == undefined) {
            _output.Estatus = false;
            _output.Mensaje += '&nbsp;<i class="fa fa-angle-double-right"></i>&nbsp;Seleccione una hora de inicio para la cita.<br />';
        }
        if ($('#h_termino').val() == '' || $('#h_termino').val() == undefined) {
            _output.Estatus = false;
            _output.Mensaje += '&nbsp;<i class="fa fa-angle-double-right"></i>&nbsp;Seleccione una hora fin para la cita.<br />';
        }
        if ($('#h_inicio').val() == $('#h_termino').val()) {
            _output.Estatus = false;
            _output.Mensaje += '&nbsp;<i class="fa fa-angle-double-right"></i>&nbsp;La hora de inicio de la cita debe ser diferente a la hora fin.<br />';
        }

        if (_output.Mensaje != "")
            _output.Mensaje = "Favor de proporcionar lo siguiente: <br />" + _output.Mensaje;
    } catch (e) {
        _mostrar_mensaje('Informe técnico', e);
    } finally {
        return _output;
    }
}
function _validar_duplicar_horarios(hora_inicio, fecha_cita) {
    var _horario_agregados = null;
    var _horario_existentes_tabla = false;
    try {
        _horario_agregados = $("#tbl_lista_horarios").bootstrapTable('getData');
        $.each(_horario_agregados, function (index, value) {
            if (value.Hora_Inicio == hora_inicio && value.Fecha_Cita == fecha_cita) {
                _horario_existentes_tabla = true;
            }

        });
    } catch (e) {
        _mostrar_mensaje('Informe Técnico', e);
    }
    return _horario_existentes_tabla;
}
function _guardar_horario() {
    var horario = null;
    try {
        horario = new Object();

        horario.No_Solicitud = parseInt($('#txt_no_solicitud').val());
        horario.Fecha_Cita = parseDateIntercambiarDiaMes($('#f_cita').val());
        horario.Hora_Inicio = $('#h_inicio').val();
        horario.Hora_Fin = $('#h_termino').val();
        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(horario) });

        $.ajax({
            type: 'POST',
            url: 'controllers/Ope_Autorizacion_Solicitud_Controller.asmx/Alta_Horario',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function ($result) {
                var Resultado = JSON.parse($result.d);

                if (Resultado != null && Resultado != undefined && Resultado != '') {
                    if (Resultado.Estatus == 'success') {
                        _mostrar_mensaje(Resultado.Titulo, Resultado.Mensaje);
                    } else if (Resultado.Estatus == 'error') {
                        _mostrar_mensaje(Resultado.Titulo, Resultado.Mensaje);
                    }
                } else { _mostrar_mensaje(Resultado.Titulo, Resultado.Mensaje); }
            }
        });
        // _search_horarios(row.No_Solicitud);

    } catch (ex) {
        _mostrar_mensaje("Informe T&eacute;cnico", ex);
    }
}
function btn_cancelar_click(renglon) {
    var row = $(renglon).data('solicitud_cita');
    if (row.Estatus.toUpperCase() == "CERRADO") {
        _mostrar_mensaje('Validación', "<i class='fa fa-exclamation-circle' style = 'color:#f2041a;' ></ i > &nbsp;No puedes cancelar una solicitud cerrada");
        return;
    }
    if (row.Estatus.toUpperCase() == "CANCELADO") {
        _mostrar_mensaje('Validación', "<i class='fa fa-exclamation-circle' style = 'color:#f2041a;' ></ i > &nbsp; No puedes cancelar una solicitud que ya esta cancelada.");
        return;
    }
    bootbox.confirm({
        title: 'Cancelar Cita',
        message: '¿Estas seguro de cancelar la solicitud de la cita?',
        callback: function (result) {
            if (result) {
                //validar si es usuario
                //var usuario = _valida_usuarios();
                //if (usuario == true) {
                //    _mostrar_mensaje('Validation Report', 'Invalid user: unable to cancel the alert.');
                //    return;
                //}
                _cancelar_solicitud_cita(row.No_Solicitud);
                _mostrar_mensaje("Validación", "<i class='fa fa-check'style = 'color: #00A41E;' ></ i > &nbsp; La Solicitud fue cancelada exitosamente.");
            }
        }
    });
}
function _cancelar_solicitud_cita(no_solicitud) {
    var Solicitud = null;
    var isComplete = false;
    try {
        Solicitud = new Object();
        Solicitud.No_Solicitud = parseInt(no_solicitud);

        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(Solicitud) });
        $.ajax({
            type: 'POST',
            url: 'controllers/Ope_Solicitud_Citas_Controller.asmx/Cancelar_Solicitud_Cita',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function ($result) {
                Resultado = JSON.parse($result.d);
                if (Resultado != null && Resultado != undefined && Resultado != '') {
                    if (Resultado.Estatus == 'success') {
                        validacion = true;
                    } else if (Resultado.Estatus == 'error') {
                        _mostrar_mensaje(Resultado.Titulo, Resultado.Mensaje);
                    }
                } else { _mostrar_mensaje("Error", "Unable to get results"); }
            }
        });
        //_search_alertas_rojas_por_filtros();
    } catch (e) {
        _mostrar_mensaje('Technical Report', e);
    }
    return isComplete;
}
function btn_autorizar_click(renglon) {
    var row = $(renglon).data('horario');
    var horarios = _validar_horario(row.No_Solicitud);

    var checks = $('input:checkbox[name=cb_autorizar]:checked').length;
    if (checks > 1) {
        _mostrar_mensaje("Validación", "<i class='fa fa-exclamation-circle'style = 'color:#f2041a;' ></ i > &nbsp;No puedes autorizar más de un horario para la cita.");
        $('#check_' + row.Horario_Solicitud_Detalle_ID).attr('checked', false)
        return;

    }

    if ($('#check_' + row.Horario_Solicitud_Detalle_ID).is(':checked')) {
        _autorizar_horario(row.Horario_Solicitud_Detalle_ID)
        _mostrar_mensaje("Validación", "<i class='fa fa-check'style = 'color: #00A41E;' ></ i > &nbsp;Este será el horario asignado para la cita.");
        _consultar_horarios(row.No_Solicitud);
        var horario = _validar_horario(row.No_Solicitud);
        if (horario == "True") {
            $('#btn_agregar_horario').attr({ disabled: true });
        }
      
        }
        else {
            _rechazar_horario(row.Horario_Solicitud_Detalle_ID);
            _mostrar_mensaje("Validación", "<i class='fa fa-check'style = 'color: #00A41E;' ></ i > &nbsp;Este horario ha sido descartado para la cita.");
            _search_horarios(row.No_Solicitud);
            var horario = _validar_horario(row.No_Solicitud);
            if (horario == "False") {
                $('#btn_agregar_horario').attr({ disabled: false });
            }
         }
     
}
function _autorizar_horario(horario) {
    var horarios = null;
    var isComplete = false;
    try {
        horarios = new Object();
        horarios.Horario_Solicitud_Detalle_ID = parseInt(horario);

        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(horarios) });
        $.ajax({
            type: 'POST',
            url: 'controllers/Ope_Autorizacion_Solicitud_Controller.asmx/Autorizar_Horario',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function ($result) {
                Resultado = JSON.parse($result.d);
                if (Resultado != null && Resultado != undefined && Resultado != '') {

                    for (var indice = 0; indice < Resultado.length; indice++) {
                        if (Resultado[indice].Aprobado == "SI") {
                            $('#check_' + Resultado[indice].Horario_Solicitud_Detalle_ID).attr('checked', true);
                        }
                    }
                    if (Resultado.Estatus == 'success') {
                        validacion = true;
                    } else if (Resultado.Estatus == 'error') {
                        _mostrar_mensaje(Resultado.Titulo, Resultado.Mensaje);
                    }
                } else { _mostrar_mensaje("Error", "Unable to get results"); }
            }
        });
    } catch (e) {
        _mostrar_mensaje('Technical Report', e);
    }
    return isComplete;
  
}
function btn_rechazar_click(renglon) {
    var row = $(renglon).data('horario');

    if (row.Aprobado.toUpperCase() == "NO") {
        _mostrar_mensaje('Validación', "<i class='fa fa-exclamation-circle'style = 'color:#f2041a;' ></ i > &nbsp;Este horario ya está rechazado.");
        return;
    }
    //if (row.Aprobado.toUpperCase() == "SI") {
    //    _mostrar_mensaje('Validación', "<i class='fa fa-exclamation-circle' style = 'color:#f2041a;' ></ i > &nbsp; No puedes rechazar un horario que ya fue autorizado.");
    //    return;
    //}

    bootbox.confirm({
        title: 'Rechazar horario',
        message: '¿Estás seguro de rechazar este horario?',
        callback: function (result) {
            if (result) {
                //validar si es usuario
                //var usuario = _valida_usuarios();
                //if (usuario == true) {
                //    _mostrar_mensaje('Validation Report', 'Invalid user: unable to cancel the alert.');
                //    return;
                //}
                _rechazar_horario(row.Horario_Solicitud_Detalle_ID);
                _mostrar_mensaje("Validación", "<i class='fa fa-check'style = 'color: #00A41E;' ></ i > &nbsp;Este horario ha sido descartado para la cita.");
                _search_horarios(row.No_Solicitud);
            }
        }
    });
}
function _rechazar_horario(horario) {
    var horarios = null;
    var isComplete = false;
    try {
        horarios = new Object();
        horarios.Horario_Solicitud_Detalle_ID = parseInt(horario);

        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(horarios) });
        $.ajax({
            type: 'POST',
            url: 'controllers/Ope_Autorizacion_Solicitud_Controller.asmx/Rechazar_Horario',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function ($result) {
                Resultado = JSON.parse($result.d);
                if (Resultado != null && Resultado != undefined && Resultado != '') {
                    for (var indice = 0; indice < Resultado.length; indice++) {
                        if (Resultado[indice].Aprobado == "NO") {
                            $('#check_' + Resultado[indice].Horario_Solicitud_Detalle_ID).attr('checked', f);
                        }
                    }
                    if (Resultado.Estatus == 'success') {
                        validacion = true;
                    } else if (Resultado.Estatus == 'error') {
                        _mostrar_mensaje(Resultado.Titulo, Resultado.Mensaje);
                    }
                } else { _mostrar_mensaje("Error", "Unable to get results"); }
            }
        });
        $('#tbl_lista_horarios').bootstrapTable('refresh', 'controllers/Ope_Autorizacion_Solicitud_Controller.asmx/Consultar_Horarios');
    } catch (e) {
        _mostrar_mensaje('Technical Report', e);
    }
    return isComplete;

}
function _consultar_horarios(no_solicitud) {
    var filtros = null;
    try {

        filtros = new Object();
        filtros.No_Solicitud = parseInt(no_solicitud);

        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(filtros) });

        jQuery.ajax({
            type: 'POST',
            url: 'controllers/Ope_Autorizacion_Solicitud_Controller.asmx/Consultar_Horarios',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function (datos) {
                if (datos !== null) {
                    datos_cb = JSON.parse(datos.d);
                    $('#tbl_lista_horarios').bootstrapTable('load', JSON.parse(datos.d));
                    //$('input[name=ch_equipos]').each(function (index) {
                    for (var indice = 0; indice < datos_cb.length; indice++) {
                        if (datos_cb[indice].Aprobado == "SI") {
                            $('#check_' + datos_cb[indice].Horario_Solicitud_Detalle_ID).attr('checked', true);
                        }
                    }
                }
            }
        });

    } catch (e) {

    }
}
function _search_horarios(no_solicitud) {
    var filtros = null;
    var datos_cb = null;
    try {
        filtros = new Object();
        filtros.No_Solicitud = parseInt(no_solicitud);
        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(filtros) });
        jQuery.ajax({
            type: 'POST',
            url: 'controllers/Ope_Autorizacion_Solicitud_Controller.asmx/Consultar_Horarios_Por_Filtros',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function (datos) {
                if (datos !== null && datos.d !== '') {
                    datos_cb = JSON.parse(datos.d);
                    $('#tbl_lista_horarios').bootstrapTable('load', JSON.parse(datos.d));
                    
                    //$('input[name=ch_equipos]').each(function (index) {
                    for (var indice = 0; indice < datos_cb.length; indice++) {
                        if (datos_cb[indice].Aprobado == "SI") {
                            $('#check_' + datos_cb[indice].Horario_Solicitud_Detalle_ID).attr('checked', true);
                        }
                    }
                    datos.d = (datos.d == undefined || datos.d == null) ? '[]' : datos.d;
                    //if (JSON.parse(datos.d).length > 0)
                    //    $("#ctrl_panel").click();
                }
            }
        });
    } catch (e) {
        _mostrar_mensaje('Technical Report', e);
    }
}
function btn_editar_click(renglon) {
    var row = $(renglon).data('solicitud_cita');

    if (row.Estatus != "ABIERTO") {
        _mostrar_mensaje('Validación', "<i class='fa fa-exclamation-circle'style = 'color:#f2041a;' ></ i > &nbsp;No puedes autorizar una cita con estatus. "+row.Estatus);
        return;
    }
    $('#autorizacion_empleado').css('display', 'block');
    $('#asignar_empleado').css('display', 'none');
    _habilitar_controles('Autorizar');
    _crear_tbl_horarios();
    _consultar_horarios(row.No_Solicitud);
  
    var Solicitud = null;

    try {

        Solicitud = new Object();
        Solicitud.No_Solicitud = row.No_Solicitud;

        if (row.Departamento_ID != null)
            Solicitud.Departamento_ID = row.Departamento_ID;
        if (row.Empleado_ID != null)
            Solicitud.Empleado_ID = row.Empleado_ID;

        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(Solicitud) });
        $.ajax({
            type: 'POST',
            url: 'controllers/Ope_Solicitud_Citas_Controller.asmx/Consultar_Una_Solicitud',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function ($result) {
                var Resultado = JSON.parse($result.d);
                if (Resultado != null && Resultado != undefined && Resultado != '') {

                    $("#txt_no_solicitud").val(row.No_Solicitud);
                    // $('#txt_numero_parte').val(Resultado[0].Numero_Parte);

                    $("#cmb_estatus")[0].innerHTML = "";
                    $("#cmb_estatus").select2({ data: [{ id: row.Estatus_ID, text: row.Estatus }] });
                    $("#cmb_estatus").val(row.Estatus_ID).trigger("change");

                    $("#cmb_departamento_visitado")[0].innerHTML = "";
                    $("#cmb_departamento_visitado").select2({ data: [{ id: row.Departamento_ID, text: row.Departamento }] });
                    $("#cmb_departamento_visitado").val(row.Departamento_ID).trigger("change");

                    $("#cmb_empleado_visitado")[0].innerHTML = "";
                    $("#cmb_empleado_visitado").select2({ data: [{ id: row.Empleado_ID, text: row.Empleado_Visitado }] });
                    $("#cmb_empleado_visitado").val(row.Empleado_ID).trigger("change");




                    $('#nombre_solicitante').val(Resultado[0].Nombre_Solicitante);
                    $('#apellidos_solicitante').val(Resultado[0].Apellidos);
                    $('#correo_solicitante').val(Resultado[0].Correo);

                    $('#telefono_solicitante').val(Resultado[0].Telefono);
                    $('#txt_motivo_cita').val(Resultado[0].Motivo_Cita);
                    ////if (row.Estatus == "CANCELED") {
                    ////    $('#btn_guardar').css('display', 'none');
                    ////    $('#btn_salir').css('display', 'none');
                    ////    $('#btn_regresar').css('display', 'block');
                    ////}
                    ////if (row.Estatus == "CANCELED" || row.Estatus == "REJECTED") {
                    ////    $("#lbl_observaciones").css('display', 'block');
                    ////    $("#lbl_observaciones").html(Resultado[0].Observaciones);
                    ////}
                    ////else {
                    ////    $("#lbl_observaciones").css('display', 'none');
                    ////}


                    // _load_estatus('#cmb_estatus');
                    _consultar_departamentos('#cmb_departamento_visitado');
                    _consultar_empleados1("#cmb_empleado_visitado");
                    _consultar_empleado("#cmb_empleado_visitado");

                } else { _mostrar_mensaje(Resultado.Titulo, Resultado.Mensaje); }
            }
        });
    } catch (e) {
        _mostrar_mensaje('Technical Report', e);
    }
}
function _modificar_solicitud_cita() {
    var Solicitud = null;
    try {
        Solicitud = new Object();
        Solicitud.No_Solicitud = parseInt($('#txt_no_solicitud').val())
        Solicitud.Empleado_ID = $('#cmb_empleado_visitado').val() == '' ? 0 : parseInt($('#cmb_empleado_visitado').val());
        if (isNaN(Solicitud.Empleado_ID)) {
            Solicitud.Empleado_ID = 0;
            Solicitud.Empleado_Visitado = $('#cmb_empleado_visitado').val();
        }
        Solicitud.Departamento_ID = $('#cmb_departamento_visitado').val() == '' ? 0 : parseInt($('#cmb_departamento_visitado').val());
        Solicitud.Estatus_ID = $('#cmb_estatus').val() == '' ? 0 : parseInt($('#cmb_estatus').val());
        Solicitud.Nombre_Solicitante = $('#nombre_solicitante').val();
        Solicitud.Apellidos = $('#apellidos_solicitante').val();
        Solicitud.Correo = $('#correo_solicitante').val();
        Solicitud.Telefono = $('#telefono_solicitante').val();
        Solicitud.Motivo_Cita = $('#txt_motivo_cita').val();

        Solicitud.Datos_Detalles_Horarios = JSON.stringify($('#tbl_lista_horarios').bootstrapTable('getData'));

        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(Solicitud) });

        $.ajax({
            type: 'POST',
            url: 'controllers/Ope_Solicitud_Citas_Controller.asmx/Actualizar',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function ($result) {
                var Resultado = JSON.parse($result.d);

                if (Resultado != null && Resultado != undefined && Resultado != '') {
                    if (Resultado.Estatus == 'success') {
                        _mostrar_mensaje(Resultado.Titulo, Resultado.Mensaje);
                        _inicializar_pagina();
                        _limpiar_controles();
                    } else if (Resultado.Estatus == 'error') {
                        _mostrar_mensaje(Resultado.Titulo, Resultado.Mensaje);
                    }
                } else { _mostrar_mensaje(Resultado.Titulo, Resultado.Mensaje); }
            }
        });


    } catch (ex) {
        _mostrar_mensaje("Informe T&eacute;cnico", ex);
    }
}
function btn_rechazar_cita() {
    var no_solicitud = $('#txt_no_solicitud').val();
    var estatus=$('#cmb_estatus').val();

    if (estatus.toUpperCase() == "28") {
        _mostrar_mensaje('Validación', "<i class='fa fa-exclamation-circle'style = 'color:#f2041a;' ></ i > &nbsp;Esta cita ya fue rechazada.");
        return;
    }
    if (estatus.toUpperCase() == "17") {
        _mostrar_mensaje('Validación', "<i class='fa fa-exclamation-circle'style = 'color:#f2041a;' ></ i > &nbsp;Esta cita esta cancelada .");
        return;
    }
    if (estatus.toUpperCase() == "8") {
        _mostrar_mensaje('Validación', "<i class='fa fa-exclamation-circle'style = 'color:#f2041a;' ></ i > &nbsp;Esta cita ya fue autorizada.");
        return;
    }
    bootbox.confirm({
        title: 'Rechazar cita',
        message: '¿Estás seguro de rechazar esta cita?',
        callback: function (result) {
            if (result) {
                bootbox.prompt('Describe la razón del rechazo.', function (result) {
                    _actualizar_observaciones_solicitud(no_solicitud, result);
                    _rechazar_cita(no_solicitud);
                    _mostrar_mensaje("Validación", "<i class='fa fa-check'style = 'color: #00A41E;' ></ i > &nbsp;La cita fue rechazada.");
                    _habilitar_controles('Inicial');
                    _search_solicitud_por_usuario();
                    _search_solicitud_por_departamento();
                });
            }
            else {
                _search_solicitud_por_usuario();
                _search_solicitud_por_departamento();
            }

        }
    });
}
function _rechazar_cita(solicitud) {
    var horarios = null;
    var isComplete = false;
    try {
        horarios = new Object();
        horarios.No_Solicitud = parseInt(solicitud);

        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(horarios) });
        $.ajax({
            type: 'POST',
            url: 'controllers/Ope_Autorizacion_Solicitud_Controller.asmx/Rechazar_Solicitud',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function ($result) {
                Resultado = JSON.parse($result.d);
                if (Resultado != null && Resultado != undefined && Resultado != '') {
                    if (Resultado.Estatus == 'success') {
                        validacion = true;
                    } else if (Resultado.Estatus == 'error') {
                        _mostrar_mensaje(Resultado.Titulo, Resultado.Mensaje);
                    }
                } else { _mostrar_mensaje("Error", "Unable to get results"); }
            }
        });
     
    } catch (e) {
        _mostrar_mensaje('Technical Report', e);
    }
    return isComplete;

}
function _actualizar_observaciones_solicitud(no_solicitud, result) {
    var solicitud;
    try {
        solicitud = new Object();
        solicitud.No_Solicitud = parseInt(no_solicitud);
        solicitud.Observaciones = result == null ? '' : result;

        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(solicitud) });
        $.ajax({
            type: 'POST',
            url: 'controllers/Ope_Autorizacion_Solicitud_Controller.asmx/Actualizar_Observaciones',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function ($result) {
                Resultado = JSON.parse($result.d);
                if (Resultado != null && Resultado != undefined && Resultado != '') {
                    if (Resultado.Estatus == 'success') {
                        validacion = true;
                    } else if (Resultado.Estatus == 'error') {
                        _mostrar_mensaje(Resultado.Titulo, Resultado.Mensaje);
                    }
                } else { _mostrar_mensaje("Error", "Unable to get results"); }
            }
        });
       // _search_alertas_rojas_por_filtros();
    } catch (e) {
        _mostrar_mensaje('Technical Report', e);
    }
}
function _btn_autorizar_cita() {

    var no_solicitud = $('#txt_no_solicitud').val();
    bootbox.confirm({
        title: 'Autorizar cita',
        message: '¿Estás seguro de autorizar esta cita?',
        callback: function (result) {
            if (result) {
                var horarios = _validar_horario(no_solicitud)
                if (horarios == "False")
                    _mostrar_mensaje('Validación', "<i class='fa fa-exclamation-circle'style = 'color:#f2041a;' ></ i > &nbsp;Debes autorizar un horario para la cita.");

                else if ($('#cmb_empleado_visitado :selected').val() == '' || $('#cmb_empleado_visitado :selected').val() == undefined) {
                    _mostrar_mensaje('Validación', "<i class='fa fa-exclamation-circle'style = 'color:#f2041a;' ></ i > &nbsp;Debes asignar un empleado para atender la cita.");
                }
                else {
                    _autorizar_cita(no_solicitud);
                    _mostrar_mensaje("Validación", "<i class='fa fa-check'style = 'color: #00A41E;' ></ i > &nbsp;La cita ha sido autorizada exitosamente.");
                    _habilitar_controles('Inicial');
                    _search_solicitud_por_usuario();
                    _search_solicitud_por_departamento();
                }           
            }
        }
    });
}
function _autorizar_cita(solicitud) {
    var Solicitud = null;
    var isComplete = false;

    try {
        Solicitud= new Object();
        Solicitud.No_Solicitud = parseInt(solicitud);
        Solicitud.Empleado_ID = $('#cmb_empleado_visitado').val() == '' ? 0 : parseInt($('#cmb_empleado_visitado').val());
        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(Solicitud) });
        $.ajax({
            type: 'POST',
            url: 'controllers/Ope_Autorizacion_Solicitud_Controller.asmx/Autorizar_Solicitud',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function ($result) {
                Resultado = JSON.parse($result.d);
                if (Resultado != null && Resultado != undefined && Resultado != '') {
                    if (Resultado.Estatus == 'success') {
                        validacion = true;
                    } else if (Resultado.Estatus == 'error') {
                        _mostrar_mensaje(Resultado.Titulo, Resultado.Mensaje);
                    }
                } else { _mostrar_mensaje("Error", "Unable to get results"); }
            }
        });

    } catch (e) {
        _mostrar_mensaje('Technical Report', e);
    }
    return isComplete;

}
function _btn_asignar_empleado() {

    var no_solicitud = $('#txt_no_solicitud').val();
    bootbox.confirm({
        title: 'Asignar empleado',
        message: '¿Estás seguro de asignar un empleado para la cita?',
        callback: function (result) {
            if (result) {
         
                if ($('#cmb_empleado_visitado :selected').val() == '' || $('#cmb_empleado_visitado :selected').val() == undefined) {
                    _mostrar_mensaje('Validación', 'Debes asignar al empleado que atenderá la cita.');
                }
                else {
                    _asignar_empleado(no_solicitud);
                    _mostrar_mensaje("Validación", "<i class='fa fa-check'style = 'color: #00A41E;' ></ i > &nbsp;La cita fue asignada al empleado que seleccionaste.");
                    _habilitar_controles('Inicial');
                    _search_solicitud_por_usuario();
                    _search_solicitud_por_departamento();
                }
            }
        }
    });
}
function _btn_reasignar_cita() {

    var no_solicitud = $('#txt_no_solicitud').val();
    bootbox.confirm({
        title: 'Reasignar cita',
        message: '¿Estás seguro de reasignar la cita al empleado seleccionado?',
        callback: function (result) {
            if (result) {

                if ($('#cmb_empleado_visitado :selected').val() == '' || $('#cmb_empleado_visitado :selected').val() == undefined) {
                    _mostrar_mensaje('Validación', 'Debes asignar al empleado del departamento que atenderá la cita .');
                }
                else {
                    _asignar_empleado(no_solicitud);
                    _mostrar_mensaje("Validación", "<i class='fa fa-check'style = 'color: #00A41E;' ></ i > &nbsp;La cita fue asignada al empleado que seleccionaste.");
                    _habilitar_controles('Inicial');
                    _search_solicitud_por_usuario();
                    _search_solicitud_por_departamento();
                }
            }
        }
    });
}
function _asignar_empleado(solicitud) {
    var Solicitud = null;
    var isComplete = false;

    try {
        Solicitud = new Object();
        Solicitud.No_Solicitud = parseInt(solicitud);
        Solicitud.Empleado_ID = $('#cmb_empleado_visitado').val() == '' ? 0 : parseInt($('#cmb_empleado_visitado').val());

        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(Solicitud) });
        $.ajax({
            type: 'POST',
            url: 'controllers/Ope_Autorizacion_Solicitud_Controller.asmx/Asignar_Empleado_Cita',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function ($result) {
                Resultado = JSON.parse($result.d);
                if (Resultado != null && Resultado != undefined && Resultado != '') {
                    if (Resultado.Estatus == 'success') {
                        validacion = true;
                    } else if (Resultado.Estatus == 'error') {
                        _mostrar_mensaje(Resultado.Titulo, Resultado.Mensaje);
                    }
                } else { _mostrar_mensaje("Error", "Unable to get results"); }
            }
        });

    } catch (e) {
        _mostrar_mensaje('Technical Report', e);
    }
    return isComplete;

}
function btn_editar_por_departamento_click(renglon) {
    var row = $(renglon).data('solicitud_cita');
    if (row.Estatus == "CERRADO" || row.Estatus == "FINALIZADO" || row.Estatus == "CANCELADO") {
        _mostrar_mensaje('Validación', "<i class='fa fa-exclamation-circle'style = 'color:#f2041a;' ></ i > &nbsp;No puedes asignar una solicitud con estatus: " +row.Estatus);
        return;
    }
    $('#btnrechazar').css('display', 'none')

    _crear_tbl_horarios();
    $('#btn_agregar_horario').attr('disabled',true)
    $('#asignar_empleado').css('display', 'block');
    $('#autorizacion_empleado').css('display', 'none');
    $('#tbl_lista_horarios').bootstrapTable('hideColumn', 'Autorizar');
    $('#tbl_lista_horarios').bootstrapTable('hideColumn', 'Rechazar');
    _habilitar_controles('Autorizar');
  
    _consultar_horarios(row.No_Solicitud);
    var Solicitud = null;

    try {

        Solicitud = new Object();
        Solicitud.No_Solicitud = row.No_Solicitud;

        if (row.Departamento_ID != null)
            Solicitud.Departamento_ID = row.Departamento_ID;
        if (row.Empleado_ID != null)
            Solicitud.Empleado_ID = row.Empleado_ID;

        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(Solicitud) });
        $.ajax({
            type: 'POST',
            url: 'controllers/Ope_Solicitud_Citas_Controller.asmx/Consultar_Una_Solicitud',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function ($result) {
                var Resultado = JSON.parse($result.d);
                if (Resultado != null && Resultado != undefined && Resultado != '') {

                    $("#txt_no_solicitud").val(row.No_Solicitud);
                    // $('#txt_numero_parte').val(Resultado[0].Numero_Parte);

                    $("#cmb_estatus")[0].innerHTML = "";
                    $("#cmb_estatus").select2({ data: [{ id: row.Estatus_ID, text: row.Estatus }] });
                    $("#cmb_estatus").val(row.Estatus_ID).trigger("change");

                    $("#cmb_departamento_visitado")[0].innerHTML = "";
                    $("#cmb_departamento_visitado").select2({ data: [{ id: row.Departamento_ID, text: row.Departamento }] });
                    $("#cmb_departamento_visitado").val(row.Departamento_ID).trigger("change");

                    $("#cmb_empleado_visitado")[0].innerHTML = "";
                    $("#cmb_empleado_visitado").select2({ data: [{ id: row.Empleado_ID, text: row.Empleado_Visitado }] });
                    $("#cmb_empleado_visitado").val(row.Empleado_ID).trigger("change");




                    $('#nombre_solicitante').val(Resultado[0].Nombre_Solicitante);
                    $('#apellidos_solicitante').val(Resultado[0].Apellidos);
                    $('#correo_solicitante').val(Resultado[0].Correo);

                    $('#telefono_solicitante').val(Resultado[0].Telefono);
                    $('#txt_motivo_cita').val(Resultado[0].Motivo_Cita);
                    ////if (row.Estatus == "CANCELED") {
                    ////    $('#btn_guardar').css('display', 'none');
                    ////    $('#btn_salir').css('display', 'none');
                    ////    $('#btn_regresar').css('display', 'block');
                    ////}
                    ////if (row.Estatus == "CANCELED" || row.Estatus == "REJECTED") {
                    ////    $("#lbl_observaciones").css('display', 'block');
                    ////    $("#lbl_observaciones").html(Resultado[0].Observaciones);
                    ////}
                    ////else {
                    ////    $("#lbl_observaciones").css('display', 'none');
                    ////}


                    // _load_estatus('#cmb_estatus');
                    _consultar_departamentos('#cmb_departamento_visitado');
                    _consultar_empleados1("#cmb_empleado_visitado");
                    _consultar_empleado("#cmb_empleado_visitado");

                } else { _mostrar_mensaje(Resultado.Titulo, Resultado.Mensaje); }
            }
        });
    } catch (e) {
        _mostrar_mensaje('Technical Report', e);
    }
}
function _validar_horario(no_solicitud) {
    var filtros = null;
    var datos_cb = null;
    try {
        filtros = new Object();
        filtros.No_Solicitud = parseInt(no_solicitud);
        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(filtros) });
        jQuery.ajax({
            type: 'POST',
            url: 'controllers/Ope_Autorizacion_Solicitud_Controller.asmx/Validar_Horario',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function (datos) {
                if (datos !== null) {
                    datos_cb = JSON.parse(datos.d);
                }
            }
        });
        return datos_cb.Mensaje;
    } catch (e) {

    }
}
function _validar_horario_rechazado(no_solicitud) {
    var filtros = null;
    var datos_cb = null;
    try {
        filtros = new Object();
        filtros.No_Solicitud = parseInt(no_solicitud);
        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(filtros) });
        jQuery.ajax({
            type: 'POST',
            url: 'controllers/Ope_Autorizacion_Solicitud_Controller.asmx/Validar_Horario_Rechazado',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function (datos) {
                if (datos !== null) {
                    datos_cb = JSON.parse(datos.d);
                }
            }
        });
        return datos_cb.Mensaje;
    } catch (e) {

    }
}
function _actualizar_horarios(no_solicitud) {
    var filtros = null;
    var datos_cb = null;
    try {
        filtros = new Object();
        filtros.No_Solicitud = parseInt(no_solicitud);
        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(filtros) });
        jQuery.ajax({
            type: 'POST',
            url: 'controllers/Ope_Autorizacion_Solicitud_Controller.asmx/Actualizar_Horario',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function (datos) {
                if (datos !== null) {
                    datos_cb = JSON.parse(datos.d);
                }
            }
        });
        return datos_cb.Mensaje;
    } catch (e) {

    }
}
function btn_visualizar_click(renglon) {
    var row = $(renglon).data('solicitud_cita');

    $('#autorizacion_empleado').css('display', 'block');
    _habilitar_controles('Visualizar');
    _crear_tbl_horarios();
    _consultar_horarios(row.No_Solicitud);

    $('#visualizar').css('display', 'block');
    $('#titulo').css('display', 'none');

    $("input[name=cb_autorizar]").each(function () {
     $(this).attr("disabled", true)      
    });
    var Solicitud = null;

    try {

        Solicitud = new Object();
        Solicitud.No_Solicitud = row.No_Solicitud;

        if (row.Departamento_ID != null)
            Solicitud.Departamento_ID = row.Departamento_ID;
        if (row.Empleado_ID != null)
            Solicitud.Empleado_ID = row.Empleado_ID;

        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(Solicitud) });
        $.ajax({
            type: 'POST',
            url: 'controllers/Ope_Autorizacion_Solicitud_Controller.asmx/Consultar_Una_Solicitud',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function ($result) {
                var Resultado = JSON.parse($result.d);
                if (Resultado != null && Resultado != undefined && Resultado != '') {

                    $("#txt_no_solicitud").val(row.No_Solicitud);
                    // $('#txt_numero_parte').val(Resultado[0].Numero_Parte);

                    $("#cmb_estatus")[0].innerHTML = "";
                    $("#cmb_estatus").select2({ data: [{ id: row.Estatus_ID, text: row.Estatus }] });
                    $("#cmb_estatus").val(row.Estatus_ID).trigger("change");

                    $("#cmb_departamento_visitado")[0].innerHTML = "";
                    $("#cmb_departamento_visitado").select2({ data: [{ id: row.Departamento_ID, text: row.Departamento }] });
                    $("#cmb_departamento_visitado").val(row.Departamento_ID).trigger("change");

                    $("#cmb_empleado_visitado")[0].innerHTML = "";
                    $("#cmb_empleado_visitado").select2({ data: [{ id: row.Empleado_ID, text: row.Empleado_Visitado }] });
                    $("#cmb_empleado_visitado").val(row.Empleado_ID).trigger("change");




                    $('#nombre_solicitante').val(Resultado[0].Nombre_Solicitante);
                    $('#apellidos_solicitante').val(Resultado[0].Apellidos);
                    $('#correo_solicitante').val(Resultado[0].Correo);

                    $('#telefono_solicitante').val(Resultado[0].Telefono);
                    $('#txt_motivo_cita').val(Resultado[0].Motivo_Cita);
                    
                    if (row.Estatus == "CANCELADO" || row.Estatus == "RECHAZADO") {
                        $("#lbl_observaciones").css('display', 'block');
                        $("#lbl_observaciones").html(Resultado[0].Observaciones);
                    }
                    else {
                        $("#lbl_observaciones").css('display', 'none');
                    }
                    _consultar_departamentos('#cmb_departamento_visitado');
                    _consultar_empleados1("#cmb_empleado_visitado");
                    _consultar_empleado("#cmb_empleado_visitado");

                } else { _mostrar_mensaje(Resultado.Titulo, Resultado.Mensaje); }
            }
        });
    } catch (e) {
        _mostrar_mensaje('Technical Report', e);
    }
}
function _solo_numeros(e) {
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = "0123456789";
    especiales = [8];

    tecla_especial = false
    for (var i in especiales) {
        if (key == especiales[i]) {
            tecla_especial = true;
            break;
        }
    }
    if (letras.indexOf(tecla) == -1 && !tecla_especial)
        return false;
}
function _search_solicitudes_por_filtros() {
    var filtros = null;
    try {
        filtros = new Object();

        if ($.trim($('#txt_busqueda_no_solicitud').val()) !== '')
            filtros.No_Solicitud = parseInt($('#txt_busqueda_no_solicitud').val());

        if ($.trim($('#txt_busqueda_solicitante').val()) !== '')
            filtros.Nombre_Solicitante = $('#txt_busqueda_solicitante').val();

        if ($.trim($('#txt_busqueda_apellidos').val()) !== '')
            filtros.Apellidos = $('#txt_busqueda_apellidos').val();

        if ($.trim($('#f_busqueda').val()) !== '')
            filtros.Fecha_Cita = $('#f_busqueda').val();

        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(filtros) });
        jQuery.ajax({
            type: 'POST',
            url: 'controllers/Ope_Solicitud_Citas_Controller.asmx/Consultar_Solicitud',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function (datos) {
                if (datos !== null) {
                    $('#tbl_solicitud').bootstrapTable('load', JSON.parse(datos.d));
                    //_agregar_tooltip_tabla();
                    datos.d = (datos.d == undefined || datos.d == null) ? '[]' : datos.d;
                    //if (JSON.parse(datos.d).length > 0)
                    //    $("#ctrl_panel").click();
                }
            }
        });
    } catch (e) {
        _mostrar_mensaje('Technical Report', e);
    }
}
function parseTimeHorario(timeString) {
    var dateTime = timeString.split(" ");
    var timeOnly = dateTime[0];
    var times = timeOnly.split(":");
    var temp = (times[0]) + ":" + times[1] + ":" + times[2];
    return temp;
}
function parseDateIntercambiarDiaMes(dateString) {
    //Intercambia el dia y el mes de los formatos de fecha( DD/MM/YYYY o MM/DD/YYYY )
    var dateTime = dateString.split(" ");
    var dateOnly = dateTime[0];
    var dates = dateOnly.split("/");
    var temp = dates[1] + "/" + dates[0] + "/" + dates[2];
    return temp;
}
function parsehora(timeString) {
    var txtHora = Date.parse(timeString).toString("hh:mm tt")
    return txtHora
    ;
}
function parsefecha(dateString) {
    var txtfecha = Date.parse(dateString).toString("dd/MM/yyyy")
    return txtfecha;

}
function _load_estatus() {
    var filtros = null;
    try {
        jQuery.ajax({
            type: 'POST',
            url: 'controllers/Ope_Solicitud_Citas_Controller.asmx/Consultar_Estatus',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            cache: false,
            success: function (datos) {
                if (datos !== null) {
                    var datos_combo = $.parseJSON(datos.d);
                    var select2 = $('#cmb_estatus');
                    $('option', select2).remove();
                    var options = '<option value="">--SELECCIONE--</option>';
                    for (var Indice_Estatus = 0; Indice_Estatus < datos_combo.length; Indice_Estatus++) {
                        options += '<option value="' + datos_combo[Indice_Estatus].Estatus_ID + '">' + datos_combo[Indice_Estatus].Estatus.toUpperCase() + '</option>';
                        if (datos_combo[Indice_Estatus].Estatus.toUpperCase() == 'ABIERTO') {
                            estatusActivo = datos_combo[Indice_Estatus].Estatus_ID;
                        }
                    }
                    select2.append(options);
                }
            }
        });
    } catch (e) {

    }
}
function combo() {

    $('#cmb_empleado_visitado').on('change', function () {

        alert("El checkbox con valor " + $(this).val() + " ha sido seleccionado");
       
    });
}