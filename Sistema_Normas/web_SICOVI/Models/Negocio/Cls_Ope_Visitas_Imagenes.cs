﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web_cambios_procesos.Models.Negocio
{
    public class Cls_Ope_Visitas_Imagenes
    {
        public int Id { get; set; }
        public int No_Solicitud { get; set; }
        public string Ruta_Interna { get; set; }
        public string Ruta_Externa { get; set; }

    }
}