﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web_cambios_procesos.Models.Negocio
{
    public class Cls_Cat_Requerimientos_Legales_Negocio
    {
    public int Requerimiento_Legal_ID { get; set; }
    public int  Norma_ID { get; set; }
    public string  Requerimiento { get; set; }
    public string  Estatus { get; set; }
    public string  Vigencia { get; set; }
    public string  Descripcion { get; set; }
    public int?  Prioridad { get; set; }
    public int?  Vigencia_Dias { get; set; }
    public decimal?  Costo_Aproximado { get; set; }
    public string  Usuario_Creo { get; set; }
    public DateTime  Fecha_Creo { get; set; }
    public string  Usuario_Modifico { get; set; }
    public DateTime  Fecha_Modifico { get; set; }
    public DateTime?  Fecha_Actualizacion { get; set; }
    public DateTime? Fecha_Entrada_Vigor { get; set; }
     
    public string Norma { get; set; }
    }
}