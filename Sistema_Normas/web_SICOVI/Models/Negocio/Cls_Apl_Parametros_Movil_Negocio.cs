﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web_cambios_procesos.Models.Negocio
{
    public class Cls_Apl_Parametros_Movil_Negocio
    {
        public int Parametro_ID { get; set; }
        public int ?Estatus_ID { get; set; }
        public int ?Distancia_Vigilancia { get; set; }
        public int? Tiempo_Peticiones_GPS { get; set; }
        public string Ruta_Servicios_Web { get; set; }
        public double Latitud_Ubicacion_Empresa { get; set; }
        public double Longitud_Ubicacion_Empresa { get; set; }
        public double Latitud_Ubicacion_Vigilancia { get; set; }
        public double Longitud_Ubicacion_Vigilancia { get; set; }
        public string Aplicacion_ID { get; set; }
        public string Remitente_ID { get; set; }
        public string Api_Token { get; set; }
        public string Perfil_Seguridad { get; set; }
        public string Estatus { get; set; }
    }
}