﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web_cambios_procesos.Models.Negocio
{
    public class Cls_Cat_Nivel_Competencia_Negocio
    {
        public int Nivel_Competencia_ID { get; set; }
        public string Estatus { set; get; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string Usuario_Creo { set; get; }
        public string Fecha_Creo { set; get; }
        public string Usuario_Modifico { set; get; }
        public string Fecha_Modifico { set; get; }
    }
}