﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web_cambios_procesos.Models.Negocio
{
    public class Cls_Ope_Solicitud_Citas_Negocio
    {
        public int No_Solicitud { get; set; }
        public int? Empleado_ID { get; set; }
        public int Estatus_ID { get; set; }
        public int? Departamento_ID { get; set; }
        public string Nombre_Solicitante { get; set; }
        public string Apellidos { get; set; }
        public string Correo { get; set; }
        public string Telefono { get; set; }
        public string Fecha_Cita { get; set; }
        public string Hora_Inicio { get; set; }
        public string Hora_Fin { get; set; }
        public string Motivo_Cita { get; set; }
        public string Usuario_Creo { get; set; }
        public DateTime Fecha_Creo { get; set; }
        public string Usuario_Modifico { get; set; }
        public DateTime Fecha_Modifico { get; set; }
        public string Empleado_Visitado { get; set; }
        public string Departamento { get; set; }
        public string Estatus { get; set; }
        public string Datos_Detalles_Horarios { get; set; }
        public string Aprobado { get; set; }
        public string Observaciones { get; set; }
        public decimal? Latitud_Visitado { get; set; }
        public decimal? Longitud_Visitado { get; set; }
    }
}