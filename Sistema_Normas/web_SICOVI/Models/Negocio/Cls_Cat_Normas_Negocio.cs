﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web_cambios_procesos.Models.Negocio
{
    public class Cls_Cat_Normas_Negocio
    {
     public int Norma_ID { get; set; }
     public string Nombre { get; set; }
    public string Estatus { get; set; }
    public string Descripcion { get; set; }
    public string Usuario_Creo { get; set; }
    public DateTime Fecha_Creo { get; set; }
    public string Usuario_Modifico { get; set; }
    public DateTime Fecha_Modifico { get; set; }
    public int? Nivel_Competencia_ID { get; set; }
    public int? Aspecto_Normativo_ID { get; set; }
    public string Nivel_Competencia { get; set; }
    public string Aspecto_Normativo { get; set; }
    }
}